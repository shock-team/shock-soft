# Shock Soft

## About
The following project consists of a web application with the purpose of aiding in the repair process of a business which dedicates to, among various other activities, repair electronic devices such as computers, printers etc. This program will be presented as the project for the assignature "Desarrollo de Aplicaciones Cliente-Servidor" from UTN FRCU in 2021.

## Scope
The scope of this solution includes the functionalities necessary to create, update, delete and list the available repair jobs and clients, as well as other related entities.
The main focus of this program is to aid in the updating process of a repair, allowing the user to include different multimedia files and scan products to add them in a quicker way.
Another important functionality provided by the program is the ability to integrate it with a Telegram bot, which can respond to the clients' requests to know the current status of the repair.
