import { Router } from "@angular/router";
import { EditRepairTab } from "./edit-repair-tab";
import { ChangeDetectorRef } from "@angular/core";
import mappings from './edit-repair-tabs-list-mappings.json'

export abstract class EditRepairTabList extends EditRepairTab{

    deletionHandler?: (item: any) => void;

    constructor(tabType: string, router: Router, changeDetectorRef: ChangeDetectorRef){
        super(tabType,router,changeDetectorRef);
    }

    isEmpty(): boolean {
        const listProperty = this.editRepairTabsService.tabrepair[mappings[this.tabType]];
        return (listProperty && listProperty?.length == 0);
    }

    getDeletionHandler() {
        if(this.canEdit()){
            return this.deletionHandler;
        }
        else{
            return null;
        }
    }
}