import { AfterContentChecked, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { EditRepairTabsService } from 'src/app/services/edit-repair-tabs.service';

export abstract class EditRepairTab implements AfterContentChecked{

    editRepairTabsService: EditRepairTabsService;
    tabType: string;

    constructor(tabType: string, protected router: Router, protected changeDetectorRef: ChangeDetectorRef) {
        this.editRepairTabsService = EditRepairTabsService.getInstance(EditRepairTabsService.getRepairId(this.router.url));
        this.tabType = tabType;
    }

    isLoading(): boolean {
        return this.editRepairTabsService.isLoading('Repair') || this.editRepairTabsService.isLoading(this.tabType);
    }

    ngAfterContentChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    canEdit(): boolean{
        return EditRepairTabsService.getCanEdit(this.router.url);
    }
}