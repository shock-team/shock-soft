import { ModalService } from "../../services/modal.service";
import { SettingsService } from "../../services/settings.service";
import { ServiceDelete } from "../service/service-delete";
import { EntityListPageController } from "./entity-list-page-controller";

export abstract class EntityListPageControllerModal extends EntityListPageController {
    
    editionHandler: (entity: any) => void = (entity: any) => {
        this.modalService.CreateAndEditModal(this.template,
            (entity: any, pHandler: () => void) => this.baseService.update(entity,() => {
                this.refreshPages(this.listHeaders.CurrentPage);
                pHandler();
            }),
            (pHandler: (entity: any) => void) => this.baseService.getById(entity.id,pHandler));   
    }

    deletionHandler: (entity: any) => void = (entity: any) => {
        let page = this.listHeaders.CurrentPage;
        if((this.listHeaders.TotalCount % this.listHeaders.PageSize == 1) && (this.listHeaders.CurrentPage > 1)) {
            page--;
        }
        this.modalService.DeleteModal(this.template,entity,
        (pHandler: () => void) => this.baseService.delete(entity.id, () => {
            this.refreshPages(page);
            pHandler()}));
    }

    constructor(filtersSize: number, template: string, protected baseService: ServiceDelete<any,any,any>, settingsService: SettingsService, protected modalService: ModalService) {
        super(filtersSize,template,baseService,settingsService);
    }

    onClickedNew() {
        this.modalService.CreateAndEditModal(this.template,
            (entity: any, pHandler: () => void) => this.baseService.add(entity,() => {
                this.refreshPages(this.listHeaders.CurrentPage);
                pHandler();
              }));
    }
}