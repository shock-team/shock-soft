import { ViewWillEnter, ViewWillLeave } from "@ionic/angular";
import { Subscription } from "rxjs";
import { ListHeaders } from "../../entities/ListHeaders";
import { SettingsService } from "../../services/settings.service";
import { Service } from "../service/service";

export abstract class EntityListPageController implements ViewWillEnter, ViewWillLeave {
    
    public data: any[] = [];
    public subscriptions: Subscription[] = [];
    public template: string;
    public listHeaders: ListHeaders = new ListHeaders();
    public filters: string[] = [];
    
    public refreshHandler: (page: number) => void = (page: number) => this.refreshPages(page);
    
    constructor(filtersSize: number, template: string, protected baseService: Service<any>, protected settingsService: SettingsService) {
        this.template = template;
        for (let index = 0; index < filtersSize; index++) {
            this.filters.push('');
        }
    }

    refreshPages(page: number) {
        this.subscriptions = [];      
        this.listHeaders = null;
        let subs = this.baseService.getAll(this.settingsService.getByName('pageSize'),page.toString(),this.filters,
        (entity: any, listHeaders: ListHeaders) => {
            this.setData(entity,listHeaders);
            subs.unsubscribe();
        });
        this.subscriptions.push(subs);
    }

    setData(data: any[], listHeaders: ListHeaders) {
        this.listHeaders = listHeaders;
        this.data = data;
    }

    search(value: string, filterNumber: number){
        this.filters[filterNumber] = value.substring(0,200);
        this.refreshPages(1);
    }

    isEmpty(): boolean {
        return (this.listHeaders?.TotalCount == 0);
    }

    loadingFailed(): boolean {
        return !this.listHeaders;
    }
    
    isLoading(): boolean {
        return this.subscriptions.findIndex( subscription => !subscription.closed ) != -1;
    }

    ionViewWillEnter(): void {
        this.refreshPages(this.listHeaders.CurrentPage);
    }

    ionViewWillLeave(): void {
        this.subscriptions.forEach( subscription => subscription.unsubscribe() );
    }
    
    abstract onClickedNew()
    
    onClickedRefresh() {
        this.refreshPages(this.listHeaders?.CurrentPage ?? 1);
    }

    getListTemplate(): string {
        return this.template + 'List';
    }
}