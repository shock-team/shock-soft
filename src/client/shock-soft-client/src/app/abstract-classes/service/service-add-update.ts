import { Subscription } from "rxjs";
import { ApiService } from "../../services/api.service";
import { Service } from "./service";

export abstract class ServiceAddUpdate<T,TAddDto,TEditDto> extends Service<T> {

    constructor(apiURL: string, apiService: ApiService) {
        super(apiURL,apiService);
    }

    public add(entity: TAddDto, succcessHandler: () => void): Subscription {
        return this.apiService.add<T>(this.apiURL,entity,succcessHandler);
    }

    public update(entity: TEditDto, succcessHandler: (responseData?: any) => void): Subscription {
        return this.apiService.update<T>(this.apiURL+'/'+entity['id'],entity, succcessHandler);
    }
}