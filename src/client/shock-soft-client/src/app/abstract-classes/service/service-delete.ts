import { Subscription } from "rxjs";
import { ApiService } from "../../services/api.service";
import { ServiceAddUpdate } from "./service-add-update";

export abstract class ServiceDelete<T,TAddDto,TEditDto> extends ServiceAddUpdate<T,TAddDto,TEditDto> {

    constructor(apiURL: string, apiService: ApiService) {
        super(apiURL,apiService);
    }

    public delete(id: string, succcessHandler: () => void): Subscription {
        return this.apiService.delete<T>(this.apiURL+'/'+id, succcessHandler);
    }
}