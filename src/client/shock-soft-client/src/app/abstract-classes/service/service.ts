import { Subscription } from "rxjs";
import { Filter } from "../../entities/Filter";
import { ListHeaders } from "../../entities/ListHeaders";
import { ApiService } from "../../services/api.service";

export abstract class Service<T>{

    protected apiURL: string;

    constructor(apiUrl: string, protected apiService: ApiService) {
        this.apiURL = apiUrl;
    }

    public getById(id: string, setHandler: (entity: T) => void): Subscription {
        return this.apiService.get<T>(this.apiURL+'/'+id,(entity) => setHandler(entity));
    }

    public getAll(pageSize: string, pageNumber: string, filterStrings: string[], setHandler:
        (entityArray: T[], listHeaders: ListHeaders) => void): Subscription {
        const filters = this.generateFilters(filterStrings);
        return this.apiService.getAll<T>(this.apiURL+'?',filters,pageNumber,pageSize,setHandler);
    }

    protected generateFilters(filterStrings: string[]): Filter[] {
        const filters: Filter[] = [];

        if(filterStrings[0].length > 0){
            filters.push({
            parameter: 'Name',
            value: filterStrings[0]
            });
        };

        return filters;
    }
}