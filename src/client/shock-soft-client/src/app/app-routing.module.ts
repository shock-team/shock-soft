import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { canActivate, redirectUnauthorizedTo } from '@angular/fire/auth-guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'menu',
    pathMatch: 'full',
  },
  {
    path: 'menu',
    loadChildren: () =>
      import('./views/main-menu/main-menu.module').then(
        (m) => m.MainMenuPageModule
      ),
    ...canActivate(() => redirectUnauthorizedTo(['/login'])),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./views/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'registrarse',
    loadChildren: () =>
      import('./views/register/register.module').then(
        (m) => m.RegisterPageModule
      ),
  },
  {
    path: 'clientes',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./views/client-list/client-list.module').then(
            (m) => m.ClientListPageModule
          ),
      },
      {
        path: 'nuevo',
        loadChildren: () =>
          import('./views/create-client/create-client.module').then(
            (m) => m.CreateClientPageModule
          ),
      },
      {
        path: ':clientId',
        loadChildren: () =>
          import('./views/create-client/create-client.module').then(
            (m) => m.CreateClientPageModule
          ),
      },
      {
        path: 'editar/:clientId',
        loadChildren: () =>
          import('./views/create-client/create-client.module').then(
            (m) => m.CreateClientPageModule
          ),
      },
    ],
    ...canActivate(() => redirectUnauthorizedTo(['/login'])),
  },
  {
    path: 'reparaciones',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./views/repair-list/repair-list.module').then(
            (m) => m.RepairListPageModule
          ),
      },
      {
        path: 'nueva',
        loadChildren: () =>
          import('./views/create-repair/create-repair.module').then(
            (m) => m.CreateRepairPageModule
          ),
      },
      {
        path: ':repairId',
        loadChildren: () =>
          import('./views/edit-repair/edit-repair.module').then(
            (m) => m.EditRepairPageModule
          ),
      },
      {
        path: 'editar/:repairId',
        loadChildren: () =>
          import('./views/edit-repair/edit-repair.module').then(
            (m) => m.EditRepairPageModule
          ),
      },
    ],
    ...canActivate(() => redirectUnauthorizedTo(['/login'])),
  },
  {
    path: 'productos',
    loadChildren: () =>
      import('./views/product-list/product-list.module').then(
        (m) => m.ProductListPageModule
      ),
    ...canActivate(() => redirectUnauthorizedTo(['/login'])),
  },
  {
    path: 'objetos-incluidos',
    loadChildren: () =>
      import('./views/included-item-list/included-item-list.module').then(
        (m) => m.IncludedItemListPageModule
      ),
    ...canActivate(() => redirectUnauthorizedTo(['/login'])),
  },
  {
    path: 'tipos-de-dispositivo',
    loadChildren: () =>
      import('./views/device-type-list/device-type-list.module').then(
        (m) => m.DeviceTypeListPageModule
      ),
    ...canActivate(() => redirectUnauthorizedTo(['/login'])),
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
