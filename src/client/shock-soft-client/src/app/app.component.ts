import { Component, Injector } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  static injector: Injector;
  constructor(private injector: Injector) {
    AppComponent.injector = injector;
  }
}
