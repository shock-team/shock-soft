import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DisplayEditFieldQuantityComponent } from './display-edit-field-quantity.component';

describe('DisplayEditFieldQuantityComponent', () => {
  let component: DisplayEditFieldQuantityComponent;
  let fixture: ComponentFixture<DisplayEditFieldQuantityComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayEditFieldQuantityComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DisplayEditFieldQuantityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
