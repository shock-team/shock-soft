import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { RepairLine } from 'src/app/entities/RepairLine';
import { NumberUtils } from '../../utils/number-utils';


@Component({
  selector: 'app-display-edit-field-quantity',
  templateUrl: './display-edit-field-quantity.component.html',
  styleUrls: ['./display-edit-field-quantity.component.scss'],
})
export class DisplayEditFieldQuantityComponent implements OnInit, OnChanges {

  @Input() repairLine: RepairLine;
  @Input() maxValue: number;
  @Input() minValue: number;
  @Input() disabled: boolean;

  quantity: number;
  inputId: string;

  constructor() { }

  ngOnInit() {}

  onChangedValue(value: number) {
    this.repairLine.quantity = NumberUtils.getInputValue(value,this.minValue,this.maxValue);
    (document.getElementById(this.inputId) as HTMLInputElement).value = this.repairLine.quantity.toString();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.repairLine?.firstChange){
      this.quantity = this.repairLine.quantity;
      this.inputId = 'input-' + this.repairLine.id;
    }
  }
}
