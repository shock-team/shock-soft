import { AfterContentChecked, Component, Input, OnInit } from '@angular/core';
import { ChipData } from 'src/app/entities/ChipData';
import displayFieldChipTemplates from 'src/app/components/display-field-chip/templates.json';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-display-field-chip',
  templateUrl: './display-field-chip.component.html',
  styleUrls: ['./display-field-chip.component.scss'],
})
export class DisplayFieldChipComponent implements OnInit, AfterContentChecked {
  
  @Input() state: number;
  @Input() template: string;
  @Input() clickHandler: (state: any) => void;
  @Input() disabled: boolean;

  currentTemplate: ChipData[] = [];
  
  constructor(private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.currentTemplate = displayFieldChipTemplates[this.template.toString()];
  }

  ngAfterContentChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  getEntityColor(): string {
    return this.getTemplateEntity().color;
  }

  getEntityIcon(): string {
    return this.getTemplateEntity().icon;
  }

  getEntityString(): string {
    return this.getTemplateEntity().displayValue;
  }

  getTemplateEntity(): ChipData {
    return this.currentTemplate.find( x => x.value == this.state);
  }

  onClickedChip() {
    this.clickHandler(this.state);
  }

}
