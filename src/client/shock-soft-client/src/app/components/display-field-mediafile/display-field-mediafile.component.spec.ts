import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DisplayFieldMediafileComponent } from './display-field-mediafile.component';

describe('DisplayFieldMediafileComponent', () => {
  let component: DisplayFieldMediafileComponent;
  let fixture: ComponentFixture<DisplayFieldMediafileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayFieldMediafileComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DisplayFieldMediafileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
