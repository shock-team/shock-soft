import { AfterContentChecked, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-display-field-mediafile',
  templateUrl: './display-field-mediafile.component.html',
  styleUrls: ['./display-field-mediafile.component.scss'],
})
export class DisplayFieldMediafileComponent implements OnInit, OnDestroy {

  @Input() retrieveFileHandler: (handler: (fileData: Blob) => void) => Subscription;
  @Input() base64Image: string;

  fileurl: string = '';
  file;
  subscription: Subscription;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit() {
    if (this.base64Image == null || this.base64Image == ''){
      this.subscription = this.retrieveFileHandler((data: Blob) => this.file = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(data)));
    }
    else {
      this.file = this.base64Image;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  isLoading(): boolean {
    return this.subscription && !this.subscription?.closed;
  }

  notFound(): boolean {
    return this.file == null
  }
}
