import { Component, Input, OnInit } from '@angular/core';
import errorMessageTemplates from '../../components/error-message/templates.json';

@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss'],
})
export class ErrorMessageComponent implements OnInit {

  @Input() template: string;

  message: string;

  constructor() { }

  ngOnInit() {
    this.message = errorMessageTemplates[this.template];
  }

}
