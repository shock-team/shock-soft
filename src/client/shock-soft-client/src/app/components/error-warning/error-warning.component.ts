import { Component, Input, OnInit } from '@angular/core';
import errorWarningTemplates from './templates.json';

@Component({
  selector: 'app-error-warning',
  templateUrl: './error-warning.component.html',
  styleUrls: ['./error-warning.component.scss'],
})
export class ErrorWarningComponent implements OnInit {

  @Input() template: string;

  message: string;
  
  constructor() { }

  ngOnInit() {
    this.message = errorWarningTemplates[this.template];
  }

}
