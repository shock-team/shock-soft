import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { DisplayField } from 'src/app/entities/DisplayField';

import listCardTemplates from 'src/app/components/list-card/templates.json';

@Component({
  selector: 'app-list-card',
  templateUrl: './list-card.component.html',
  styleUrls: ['./list-card.component.scss'],
})
export class ListCardComponent implements OnInit {

  @Input() template: string;
  @Input() entity: any;
  @Input() editionHandler: (entity: any) => void;
  @Input() deletionHandler: (entity: any) => void; 
  @Input() detailsHandler: (entityId: string) => void;
  @Input() viewHandler: (entity: any) => void;
  @ViewChild('content', { read: ElementRef, static: true }) content: ElementRef;

  canEdit: boolean;
  canDelete: boolean;
  canSeeDetails: boolean;
  canView: boolean;

  displayFields: DisplayField[];

  indexes: number[];

  constructor() { }

  ngOnInit() {
    this.displayFields = listCardTemplates[this.template];
    
    this.initializeIndexes();
    
    this.canEdit = (this.editionHandler != undefined);
    this.canDelete = (this.deletionHandler != undefined);
    this.canSeeDetails = (this.detailsHandler != undefined);
    this.canView = (this.viewHandler != undefined);
  }

  initializeIndexes() {
    this.indexes = [];
    let index = 0;

    if(this.content.nativeElement.innerHTML.length == 0){
      index = 1;
    }

    for(index; index < this.displayFields.length; index += 2) {
      this.indexes.push(index);
    }
  }

  onClickedEdit() {
    this.editionHandler(this.entity);
  }

  onClickedDelete() {
    this.deletionHandler(this.entity);
  }

  onClickedDetails() {
    const entityId = this.entity['id'];
    this.detailsHandler(entityId);
  }

  onClickedView() {
    this.viewHandler(this.entity);
  }

  getEntityValue(fieldIndex: number): string {
    
    if(fieldIndex < this.displayFields.length) {
      const type: string = this.displayFields[fieldIndex].field.substr(this.displayFields[fieldIndex].field.length-4,this.displayFields[fieldIndex].field.length).toLowerCase();
      let currentEntity: any = eval('this.entity?.'+this.displayFields[fieldIndex].field);
      if (type == 'date'){
        let date: string = currentEntity.toString();
        if(!date.includes('Z')){
          date = date + 'Z';
        }
        currentEntity = new Date(date).toLocaleString();
      };
      
      return currentEntity;
    }
    return '';
  }

  getFieldName(fieldIndex: number): string {
    if(fieldIndex < this.displayFields.length) {
      const field = this.displayFields[fieldIndex].displayName;
      if(field != undefined) {
        return field;
      }
    }
    return '';
  }
}
