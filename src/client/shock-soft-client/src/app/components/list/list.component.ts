import { Component, Input, OnInit } from '@angular/core';
import emptyListMessageTemplates from './empty-list-messages.json'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {

  @Input() template: string;
  @Input() isLoading: boolean;
  @Input() loadingFailed: boolean;
  @Input() isEmpty: boolean;

  private emptyListMessage: string;
  
  constructor() { }

  ngOnInit() {
    this.emptyListMessage = emptyListMessageTemplates[this.template];
  }

  getListTemplate(): string {
    return this.template + 'List';
  }
}
