import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ButtonData } from '../../entities/ButtonData';
import { CityService } from '../../services/city.service';
import { LoadingController, MenuController } from '@ionic/angular';
import { Toast } from 'src/app/utils/toast';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  buttons: ButtonData[] = [];
  
  constructor(private cityService: CityService, private router: Router, private menuController: MenuController, private authService: AuthenticationService) { }

  ngOnInit() {
    this.initializeButtons();
  }

  async onClickedCities(){
    const controller = new LoadingController();
    const loading = await controller.create({
      message: 'Inicializando localidades. Por favor espere...',
      spinner: 'crescent'
    });
    await loading.present();
    this.cityService.seedCities(() => {
      Toast.createToast('Localidades inicializadas exitosamente');
      loading.dismiss();
    },
    () => loading.dismiss());
  }

  onLogoutClicked() {
    this.authService.logOut();
  }

  onClickedButton(action: () => void) {
    this.menuController.close();
    action();
  }
  
  initializeButtons() {
    const primaryColor = "primary";
    this.buttons = [
      {
        name: "Reparaciones",
        icon: "build",
        color: "b01",
        action: () => this.router.navigate(['./reparaciones'])
      },
      {
        name: "Clientes",
        icon: "people",
        color: "b02",
        action: () => this.router.navigate(['./clientes'])
      },
      {
        name: "Productos",
        icon: "desktop-outline",
        color: "b03",
        action: () => this.router.navigate(['./productos'])
      },
      {
        name: "Tipos de Dispositivo",
        icon: "print",
        color: "b04",
        action: () => this.router.navigate(['./tipos-de-dispositivo'])
      },
      {
        name: "Objetos Incluidos",
        icon: "battery-charging",
        color: "b05",
        action: () => this.router.navigate(['./objetos-incluidos'])
      },
      {
        name: "Inicializar Localidades",
        icon: "business-outline",
        color: "b06",
        action: () => this.onClickedCities()
      },
      {
        name: "Logout",
        icon: "log-out-outline",
        color: "b07",
        action: () => this.onLogoutClicked()
      }
    ];
  }

}
