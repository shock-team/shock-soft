import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ListHeaders } from 'src/app/entities/ListHeaders';
import { NumberUtils } from '../../utils/number-utils';

@Component({
  selector: 'app-page-selector',
  templateUrl: './page-selector.component.html',
  styleUrls: ['./page-selector.component.scss'],
})
export class PageSelectorComponent implements OnInit, OnChanges {

  @Input() refreshHandler: (page: number) => void;
  @Input() listHeaders: ListHeaders;

  isFirstPageLabelShown: boolean;
  isPreviousPagesLabelShown: boolean;
  isNextPagesLabelShown: boolean;
  isLastPageLabelShown: boolean;

  previousPagesLabel: string;
  nextPagesLabel: string;

  currentPage: number = 1;
  
  constructor() {}

  ngOnInit() {
    this.refreshLabels();
  }

  ngOnChanges(change: SimpleChanges) {
    if(change.listHeaders.currentValue) {
      this.listHeaders = change.listHeaders.currentValue;
      this.currentPage = this.listHeaders.CurrentPage;
      this.refreshLabels();
    }
  }

  onChangedPage(newPage: number) {
   if(newPage != this.currentPage) {
      this.currentPage = NumberUtils.getInputValue(newPage,1,this.listHeaders.TotalPages)
      this.refreshHandler(this.currentPage);
    }
  }

  refreshLabels() {
    this.isFirstPageLabelShown = (this.listHeaders?.CurrentPage > 1);
    this.isPreviousPagesLabelShown = (this.listHeaders?.CurrentPage > 2);
    this.isNextPagesLabelShown = (this.listHeaders?.CurrentPage < (this.listHeaders?.TotalPages - 1));
    this.isLastPageLabelShown = (this.listHeaders?.CurrentPage < this.listHeaders?.TotalPages);
  
    if(this.isPreviousPagesLabelShown) {
      if(this.listHeaders?.CurrentPage == 3) {
        this.previousPagesLabel = '2';
      }
      else {
        this.previousPagesLabel = '...';
      }
    }
    
    if(this.isNextPagesLabelShown) {
      if(this.listHeaders?.CurrentPage == (this.listHeaders?.TotalPages - 2)) {
        this.nextPagesLabel = (this.listHeaders.TotalPages - 1).toString();
      }
      else {
        this.nextPagesLabel = '...';
      }
    }
  }

  validateChar(event) {
    const digitPattern = /[0-9]/;
    let input = String.fromCharCode(event.charCode);

    if(!digitPattern.test(input)) {
      event.preventDefault();
    }
  }

}
