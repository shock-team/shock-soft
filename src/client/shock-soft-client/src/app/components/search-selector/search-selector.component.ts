import { AfterViewInit, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { DisplayField } from 'src/app/entities/DisplayField';
import { SettingsService } from 'src/app/services/settings.service';
import searchSelectorTemplates from 'src/app/components/search-selector/templates.json';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search-selector',
  templateUrl: './search-selector.component.html',
  styleUrls: ['./search-selector.component.scss'],
})
export class SearchSelectorComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

  @Input() template: string;
  @Input() refreshHandler: (pageSize: string, pageNumber: string, params: string[],
    retrievalHandler: (array: any[]) => void) => Subscription;
  @Input() selectHandler: (entity: any) => void;
  @Input() inputsDisabled: boolean;
  @Input() subscription: Subscription;
  @Input() selectedEntity: any;
  @Input() selectId: string;

  displayFields: DisplayField[];
  entities: any[] = [];
  entitiesText: string[] = [];

  selectorEvent: UIEvent;

  selector: any;

  constructor(private settingsService: SettingsService, private changeDetectorRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.displayFields = searchSelectorTemplates[this.template];
    this.initializeText();
    if(!this.subscription) {
      if(this.selectedEntity){
        this.setInputs(this.selectedEntity);
      }
      else{
        this.refreshEntities();
      }
    }
  }

  ngAfterViewInit(): void {
    this.selectorEvent = new UIEvent('UIEvent');
    this.selector = document.getElementById(this.selectId);
    Object.defineProperty(this.selectorEvent, 'target', {value: this.selector, enumerable: true});
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.selectedEntity){   
      this.selectedEntity = changes.selectedEntity?.currentValue;
      if(!changes.selectedEntity?.firstChange){
        this.setInputs(this.selectedEntity);
      }
    }
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  onChangedSelect(event) {
    this.setInputs(event.detail.value);
    if(event.detail.value) {
      this.selectedEntity = event.detail.value;
      this.selectHandler(this.selectedEntity);
    }
  }

  onChangedText(event,index: number) {
    this.entitiesText[index] = event.detail.value;
    if(!this.selectedEntity) {
      this.refreshEntities();
    }
  }

  getDisplayValues(entity: any): string {
    let displayValue = '';
    this.displayFields.forEach(field => displayValue += this.getEntityValue(field.field,entity) + ' ')
    return displayValue;
  }

  refreshEntities() {
    this.subscription = this.refreshHandler(this.settingsService.getByName('selectorSize'),'1',this.entitiesText,
    (data) => {
      this.entities = data;
      this.changeDetectorRef.detectChanges();
      if(this.entitiesText.findIndex(x => x != '') != -1 && !this.inputsDisabled){
        this.selector.open(this.selectorEvent);
      }
    });
  }

  onClickedRemove() {
    this.selectedEntity = null;
    this.selectHandler(this.selectedEntity);
    this.setInputs();
  }

  setInputs(entity?: any) {
    if(entity) {
      for (let index = 0; index < this.displayFields?.length; index++) {
        this.entitiesText[index] = this.getEntityValue(this.displayFields[index].field,entity);
      }
    }
    else {
      for (let index = 0; index < this.displayFields?.length; index++) {
          this.entitiesText[index] = '';
      }
    }
  }

  getEntityValue(field: string, entity: any): string {
    return eval('entity.'+field);
  }

  initializeText() {
    this.displayFields.forEach(field => {
      this.entitiesText.push('');
    });
  }

  isLoading(): boolean {
    return this.subscription && !this.subscription?.closed;
  }

  isEmpty(): boolean {
    return !this.isLoading() && (this.entities.length == 0) && !this.selectedEntity;
  }
}
