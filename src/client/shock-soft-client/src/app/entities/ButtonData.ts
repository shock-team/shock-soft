export interface ButtonData {
    name: string;
    icon: string;
    color: string;
    action: () => void;
}