export interface ChipData {
    value: number;
    displayValue: string;
    color: string;
    icon: string;
}