import { City } from './City';

export class Client {
  id?: string;
  name: string = '';
  surname: string = '';
  phoneNumber: string = '';
  email: string = '';
  address: string = '';
  cityId: string;
  city: City;
}

export interface ClientDto {
  id?: string;
  name?: string;
  surname?: string;
  phoneNumber?: string;
  email?: string;
  address?: string;
  cityId?: string;
}
