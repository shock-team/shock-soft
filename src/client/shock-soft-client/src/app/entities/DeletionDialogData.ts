export interface DeletionDialogData{
    header: string,
    message: string,
    successMessage: string,
    descriptionFields: string[]
}