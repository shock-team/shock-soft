export class EntitiesList {
  hasNextPage: boolean;
  hasPreviousPage: boolean;
  entities: any[];
}
