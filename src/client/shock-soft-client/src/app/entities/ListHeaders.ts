export class ListHeaders {
    TotalCount: number = 0;
    PageSize: number = 0;
    CurrentPage: number = 1;
    TotalPages: number = 0;
    HasPrevious: boolean = false;
    HasNext: boolean = false;
}