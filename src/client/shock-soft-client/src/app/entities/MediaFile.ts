export interface MediaFile{
  id?: string;
  name: string;
  base64Data: string;
  repairId: string;
}

export interface MediaFileAddDto {
  name?: string;
  base64Data?: string;
  repairId?: string;
}
