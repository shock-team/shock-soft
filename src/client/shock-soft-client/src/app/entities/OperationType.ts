export enum OperationType {
    "Create",
    "Read",
    "Update"
}