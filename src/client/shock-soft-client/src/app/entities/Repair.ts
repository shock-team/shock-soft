import { Client } from './Client';
import { DeviceType } from './DeviceType';
import { IncludedItem } from './IncludedItem';
import { MediaFile } from './MediaFile';
import { RepairLine } from './RepairLine';
import { RepairState } from './RepairState';
import { StateChange } from './RepairStateLine';

export class Repair {
  id: string = '';
  problem: string = '';
  solution: string = '';
  laborPrice: number = 0;
  devicePassword: string = '';
  deviceTypeId?: string = '';
  deviceType: DeviceType = new DeviceType();
  clientId?: string = '';
  client: Client = new Client();
  creationDate?: Date;
  state: RepairState = RepairState.Activa;
  stateChanges: StateChange[] = [];
  includedItems: IncludedItem[] = [];
  repairLines: RepairLine[] = [];
  mediaFiles: MediaFile[] = [];

  public getTotal(): number{
    var total: number = 0;
      this.repairLines.forEach((repairLine) => {
        total = total + (repairLine.price*repairLine.quantity);
      });
      total = total+this.laborPrice;
      return total;
  }
}

export interface RepairDto {
  id?: string;
  problem?: string;
  solution?: string;
  laborPrice?: number;
  devicePassword?: string;
  deviceTypeId?: string;
  deviceType?: DeviceType;
  clientId?: string;
  client?: Client;
  creationDate?: Date;
  state?: RepairState;
  includedItems?: IncludedItem[];
  repairLines?: RepairLine[];
  mediaFiles?: MediaFile[];
}

export interface RepairAddDto {
  problem?: string;
  devicePassword?: string;
  deviceTypeId?: string;
  clientId?: string;
  creationDate?: Date;
  includedItems: IncludedItem[];
}
