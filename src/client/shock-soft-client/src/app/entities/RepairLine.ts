import { Product } from './Product';

export interface RepairLine {
  id?: string;
  quantity: number;
  price: number;
  productId: string;
  product: Product;
}
