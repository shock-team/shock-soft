export enum RepairState{
  "En espera",
  "Activa",
  "Pendiente",
  "Reparada",
  "Paga",
  "Cancelada",
  "Irreparable"
}
