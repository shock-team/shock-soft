import { RepairState } from './RepairState';

export interface StateChange {
  id: string;
  state: RepairState;
  date: Date;
}