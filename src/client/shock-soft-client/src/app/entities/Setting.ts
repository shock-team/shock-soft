export interface Setting {
  id: string;
  name: string;
  value: number;
}
