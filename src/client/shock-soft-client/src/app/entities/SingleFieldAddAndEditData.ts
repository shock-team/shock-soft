export interface SingleFieldAddAndEditData {
    className: string,
    newHeader: string,
    newSuccessMessage: string,
    editHeader: string,
    editSuccessMessage: string,
    fieldName: string,
    label: string,
    placeholder: string
}