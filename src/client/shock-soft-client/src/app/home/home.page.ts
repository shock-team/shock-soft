import { Component } from '@angular/core';
import { VideoCapturePlus, VideoCapturePlusOptions, MediaFile } from '@ionic-native/video-capture-plus/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  foto;

  private videoOptions: VideoCapturePlusOptions = {
    limit: 1,
    highquality: true,
    portraitOverlay: 'assets/img/camera/overlay/portrait.png',
    landscapeOverlay: 'assets/img/camera/overlay/landscape.png'
  };

  private cameraOptions: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  constructor(private videoCapturePlus: VideoCapturePlus,
    private camera: Camera) {}

  onClickedVideo(){
    this.videoCapturePlus.captureVideo(this.videoOptions).then((mediafile: MediaFile[]) => {
      console.log(mediafile);});
  }

  onClickedFoto(){
    this.camera.getPicture(this.cameraOptions).then((imageData) => {
      this.foto = 'data:image/jpeg;base64,' + imageData;
    }
    );
  }
}

