import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthenticationService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return this.authService.userToken$.pipe(
      tap((token) => (this.authService.newUserToken = token)),
      switchMap((token) => {
        const headers = new HttpHeaders().set(
          'Authorization',
          'Bearer ' + token
        );

        req = req.clone({
          headers,
        });

        return next.handle(req);
      })
    );
  }
}
