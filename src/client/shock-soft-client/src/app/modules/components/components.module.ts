import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListCardComponent } from '../../components/list-card/list-card.component';
import { PageSelectorComponent } from '../../components/page-selector/page-selector.component';
import { DisplayFieldChipComponent } from '../../components/display-field-chip/display-field-chip.component';
import { DisplayFieldMediafileComponent } from 'src/app/components/display-field-mediafile/display-field-mediafile.component';
import { DisplayEditFieldQuantityComponent } from 'src/app/components/display-edit-field-quantity/display-edit-field-quantity.component';
import { SearchSelectorComponent } from '../../components/search-selector/search-selector.component';
import { ErrorMessageComponent } from '../../components/error-message/error-message.component';
import { ErrorWarningComponent } from '../../components/error-warning/error-warning.component'
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ClientListPageRoutingModule } from '../../views/client-list/client-list-routing.module';
import { ListComponent } from '../../components/list/list.component';

@NgModule({
  declarations: [ListCardComponent,PageSelectorComponent,DisplayFieldChipComponent,SearchSelectorComponent,ErrorMessageComponent,ErrorWarningComponent,ListComponent,DisplayFieldMediafileComponent,DisplayEditFieldQuantityComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClientListPageRoutingModule
  ],
  exports: [ListCardComponent,PageSelectorComponent,DisplayFieldChipComponent,SearchSelectorComponent,ErrorMessageComponent,ErrorWarningComponent,ListComponent,DisplayFieldMediafileComponent,DisplayEditFieldQuantityComponent]
})
export class ComponentsModule { }
