import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../components/components.module';
import { SingleFieldCreateEditPageModule } from '../../views/single-field-create-edit/single-field-create-edit.module';
import { DeletionDialogPageModule } from '../../views/deletion-dialog/deletion-dialog.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ComponentsModule,
    SingleFieldCreateEditPageModule,
    DeletionDialogPageModule
  ]
})
export class ModalsModule { }
