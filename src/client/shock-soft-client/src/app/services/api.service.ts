import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Toast } from '../utils/toast';
import { Filter } from '../entities/Filter';
import { ListHeaders } from '../entities/ListHeaders';
import { Subscription } from 'rxjs';
import { ErrorMessageResolver } from '../utils/error-message-resolver';
import applicationSettings from '../application-settings.json'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseURL;

  constructor(private httpClient: HttpClient) {
    const host = applicationSettings.find(x => x.name = 'serverHost').value;
    this.baseURL = 'http://'+host+'/api/';
  }

  get<T>(urlAndParams: string, pHandler: (item: T) => void): Subscription{
    const subscription = this.httpClient.get<T>(this.baseURL+urlAndParams)
    .subscribe((data: any) => {
      pHandler(data as T);
      subscription.unsubscribe();
    },
    (error) => {
      Toast.createToast(ErrorMessageResolver.GetErrorMessage(error),true);
    });
    return subscription;
  }

  getAll<T>(endpointURL: string, filters: Filter[], pageNumber: string, pageSize: string,
    pHandler: (array: T[], listHeaders: ListHeaders) => void,
    pErrorHandler?: () => void): Subscription{
    const pageNumberParameter = 'PageNumber='+pageNumber+'&';
    const pageSizeParameter = 'PageSize='+pageSize;
    const array: T[] = [];

    filters.forEach(filter => {
      endpointURL += filter.parameter + '=' + filter.value + '&';
    });

    const subscription = this.httpClient.get(this.baseURL+endpointURL+pageNumberParameter+pageSizeParameter, {observe: 'response'})
    .subscribe((data: HttpResponse<T[]>) => {
      const headers = JSON.parse(data.headers.get('Pagination'));
      (data.body as T[]).forEach(x => {
        array.push(x);
      });
      {
        pHandler(array,headers);
        subscription.unsubscribe();
      };
    },(error) => {
      if(pErrorHandler != null){
        pErrorHandler();
      }
      Toast.createToast(ErrorMessageResolver.GetErrorMessage(error),true);
    });
    return subscription;  
  }

  getEverything<T>(endpointURL: string, pRepairId: string, pHandler: (array: T[]) => void, pErrorHandler?: () => void): Subscription{
    const array: T[] = [];
    const repairIdParameter = 'repairId='+pRepairId;
    const subscription = this.httpClient.get(this.baseURL+endpointURL+repairIdParameter, {observe: 'response'})
    .subscribe((data: HttpResponse<T[]>) => {
      (data.body as T[]).forEach(x => {
        array.push(x);
      });
      {
        pHandler(array);
        subscription.unsubscribe();
      };
    },(error) => {
      if(pErrorHandler != null){
        pErrorHandler();
      }
      Toast.createToast(ErrorMessageResolver.GetErrorMessage(error),true);
    });
    return subscription; 
  }

  add<T>(url: string, data: any, pHandler: () => void): Subscription{
    const options = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      })
    };
    const subscription = this.httpClient.post(this.baseURL+url,data,options).subscribe((response: HttpResponse<T>) => {
      if(response != null){
        pHandler();
        subscription.unsubscribe();
      }
    },
    (error) => Toast.createToast(ErrorMessageResolver.GetErrorMessage(error),true));
    return subscription;
  }

  update<T>(url: string, data: any, pHandler: (responseData?: any) => void): Subscription{
    const options = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      })
    };
    const subscription = this.httpClient.put(this.baseURL+url,data,options).subscribe((response) => {
      pHandler(response);
      subscription.unsubscribe();
    },
    (error) => Toast.createToast(ErrorMessageResolver.GetErrorMessage(error),true));
    return subscription;
  }

  delete<T>(url: string, pHandler: () => void): Subscription{
    const subscription = this.httpClient.delete(this.baseURL+url).subscribe((response: HttpResponse<T>) => {
      pHandler();
      subscription.unsubscribe();
    },
    (error) => Toast.createToast(ErrorMessageResolver.GetErrorMessage(error),true));
    return subscription;
  }

  seed<T>(url: string, pSuccessHandler: () => void, pErrorHandler: () => void): Subscription{
    const subscription = this.httpClient.post(this.baseURL+url,{observe: 'response'}).subscribe((response: HttpResponse<T>) => {
      pSuccessHandler();
      subscription.unsubscribe();
    },
    (error) => {
      Toast.createToast(ErrorMessageResolver.GetErrorMessage(error),true);
      pErrorHandler();
    });
    return subscription;
  }

  download<T>(urlAndParams: string, pHandler: (item: T) => void): Subscription{
    const subscription = this.httpClient.get(this.baseURL+urlAndParams,{responseType: 'blob'})
    .subscribe((data: any) => {
      pHandler(data as T);
      subscription.unsubscribe();
    },
    (error) => {
      Toast.createToast(ErrorMessageResolver.GetErrorMessage(error),true);
    });
    return subscription;
  }
}
