import { Injectable } from '@angular/core';
import {
  Auth,
  authState,
  createUserWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithEmailAndPassword,
  signInWithPopup,
  signOut,
  signInWithCredential,
} from '@angular/fire/auth';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { User } from '../entities/User';
import { traceUntilFirst } from '@angular/fire/performance';
import { Toast } from '../utils/toast';
import {GoogleAuth} from '@codetrix-studio/capacitor-google-auth';
import { ErrorMessageResolver } from '../utils/error-message-resolver';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public newUserToken = '';

  public userToken$ = authState(this.auth).pipe(
    traceUntilFirst('auth'),
    filter((user) => !!user),
    switchMap((user) => from(user.getIdToken()))
  );

  constructor(private auth: Auth, private router: Router) {
    GoogleAuth.initialize({
      clientId: '428548358536-6iieerp9go8komams95v9mh6tge9ipb6.apps.googleusercontent.com',
      scopes: ['profile', 'email'],
      grantOfflineAccess: true,
    });
  }

  public login(user: User) {
    signInWithEmailAndPassword(this.auth, user.email, user.password)
      .then(() => this.router.navigate(['/']))
      .catch((error) => Toast.createToast(ErrorMessageResolver.GetErrorMessage(error), true));
  }

  public loginWithGoogle() {
    if(window['Capacitor']['platform'] == 'web'){
      signInWithPopup(this.auth, new GoogleAuthProvider())
      .then(() => this.router.navigate(['/']))
      .catch((error) => Toast.createToast(ErrorMessageResolver.GetErrorMessage(error), true));
    }
    else {
      GoogleAuth.signIn().then((login) => {
        signInWithCredential(this.auth,GoogleAuthProvider.credential(null,login.authentication.accessToken))
        .then(() => this.router.navigate(['/']))
        .catch((error) => Toast.createToast(ErrorMessageResolver.GetErrorMessage(error), true));
      });
    }
  }

  public register(user: User) {
    createUserWithEmailAndPassword(this.auth, user.email, user.password)
      .then(() => {
        Toast.createToast('Usuario registrado correctamente!', false);
        this.router.navigate(['/']);
      })
      .catch((error) => Toast.createToast(ErrorMessageResolver.GetErrorMessage(error), true));
  }

  public logOut() {
    signOut(this.auth);
    this.router.navigate(['/login']);
  }
}
