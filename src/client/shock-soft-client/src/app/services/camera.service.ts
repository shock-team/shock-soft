import { Injectable } from '@angular/core';
import { Camera, CameraSource, CameraResultType, ImageOptions } from '@capacitor/camera';
import { Platform } from '@ionic/angular';
import { ErrorMessageResolver } from '../utils/error-message-resolver';
import { Toast } from '../utils/toast';

@Injectable({
  providedIn: 'root'
})
export class CameraService {

  private imageOptions: ImageOptions = {
    quality: 100,
    resultType: CameraResultType.DataUrl,
    allowEditing: false
  }

  constructor(private platform: Platform) {}

  takePhoto(setFileHandler: (base64: string) => void){
    this.imageOptions.source = CameraSource.Camera;
    this.getPicture(setFileHandler);
  }

  uploadPhoto(setFileHandler: (base64: string) => void){
    this.imageOptions.source = CameraSource.Photos;
    this.getPicture(setFileHandler);
  }

   private getPicture(setFileHandler: (base64: string) => void){
    Camera.getPhoto(this.imageOptions).then((imageData) => {
      setFileHandler(imageData.dataUrl);
    },
    (error) => {
      Toast.createToast(ErrorMessageResolver.GetErrorMessage(error),true);
    });
  }

  public setPreviewSize(base64: string, totalWidthScale: number, totalHeightScale: number, setSizeHandler: (width: number, height: number) => void){
    setSizeHandler(0,0);
    let previewImage = new Image();
    const MAX_WIDTH = totalWidthScale * this.platform.width();
    const MAX_HEIGHT = totalHeightScale * this.platform.height();
    previewImage.src = base64;
    previewImage.decode().then(() => {
      if((previewImage.width > MAX_WIDTH) || (previewImage.height > MAX_HEIGHT)){
        const targetScale = Math.max(previewImage.width / MAX_WIDTH, previewImage.height / MAX_HEIGHT);
        setSizeHandler(Math.round(previewImage.width / targetScale),Math.round(previewImage.height / targetScale));
      }
      else{
        setSizeHandler(previewImage.height,previewImage.height);
      }
    })
  }
}
