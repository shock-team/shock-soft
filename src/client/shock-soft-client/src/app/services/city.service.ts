import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { Service } from '../abstract-classes/service/service';
import { City } from '../entities/City';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class CityService extends Service<City> {

  constructor(apiService: ApiService) {
    super('City', apiService);
  }

  public seedCities(pSuccessHandler: () => void, pErrorHandler: () => void): Subscription{
    return this.apiService.seed(this.apiURL+'/Seed', pSuccessHandler, pErrorHandler);
  }
}
