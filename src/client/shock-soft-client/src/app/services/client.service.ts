import { Injectable } from '@angular/core';
import { ServiceDelete } from '../abstract-classes/service/service-delete';
import { Client, ClientDto } from '../entities/Client';
import { Filter } from '../entities/Filter';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService extends ServiceDelete<Client,ClientDto,ClientDto> {

  constructor(apiService: ApiService) {
    super('Client', apiService);
  }

  protected generateFilters(filterStrings: string[]): Filter[] {
    const filters: Filter[] = [];

    if(filterStrings[0].length > 0){
      filters.push({
        parameter: 'Name',
        value: filterStrings[0]
      });
    };

    if(filterStrings[1].length > 0){
      filters.push({
        parameter: 'Surname',
        value: filterStrings[1]
      });
    };

    return filters;
  }
}
