import { Injectable } from '@angular/core';
import { ServiceDelete } from '../abstract-classes/service/service-delete';
import { DeviceType } from '../entities/DeviceType';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class DeviceTypeService extends ServiceDelete<DeviceType,DeviceType,DeviceType> {

  constructor(apiService: ApiService) {
    super('DeviceType', apiService);
  }
}
