import { TestBed } from '@angular/core/testing';

import { EditRepairTabsService } from './edit-repair-tabs.service';

describe('EditRepairTabsService', () => {
  let service: EditRepairTabsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditRepairTabsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
