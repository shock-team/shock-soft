import { Injectable } from '@angular/core';
import { Repair } from '../entities/Repair';
import { MediaFile, MediaFileAddDto } from '../entities/MediaFile';
import { Subscription } from 'rxjs';
import { RepairService } from './repair.service'
import { DeviceTypeService } from './device-type.service';
import { ClientService } from './client.service'
import { DeviceType } from '../entities/DeviceType';
import { Client } from '../entities/Client';
import { ProductService } from './product.service'
import { AppComponent } from '../app.component';
import { ListHeaders } from '../entities/ListHeaders';
import { MediafileService } from './mediafile.service';
import { IncludedItemService } from './included-item.service';
import { StateChange } from '../entities/RepairStateLine';
import { NumberUtils } from '../utils/number-utils';
import { RepairState } from '../entities/RepairState';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class EditRepairTabsService {

  private static instance: EditRepairTabsService;

  tabrepair: Repair = new Repair();
  addedMediaFiles: MediaFile[] = [];
  deletedMediaFileIds: string[] = [];
  selectState : RepairState = null;

  repairService: RepairService;

  deviceTypeService: DeviceTypeService;

  clientService: ClientService; 
  
  productService: ProductService;

  mediafileService: MediafileService;

  includedItemService: IncludedItemService;
  
  subscriptions: Map<string,Subscription[]> = new Map<string,Subscription[]>();

  listHeaders: Map<string,ListHeaders> = new Map<string,ListHeaders>();

  form: FormGroup = new FormGroup({
    problem: new FormControl(this.tabrepair?.problem, Validators.required),
    solution: new FormControl(this.tabrepair?.solution, Validators.required),
    clientId: new FormControl(this.tabrepair?.clientId, Validators.required),
    password: new FormControl(this.tabrepair?.devicePassword),
    deviceType: new FormControl(this.tabrepair?.deviceTypeId, Validators.required),
    laborPrice: new FormControl(this.tabrepair?.laborPrice)
  });

  constructor(repairId: string) {
    if(this.tabrepair.id.length == 0){
      this.repairService = AppComponent.injector.get(RepairService);
      this.deviceTypeService = AppComponent.injector.get(DeviceTypeService);
      this.clientService = AppComponent.injector.get(ClientService);
      this.productService = AppComponent.injector.get(ProductService);
      this.mediafileService = AppComponent.injector.get(MediafileService);
      this.includedItemService = AppComponent.injector.get(IncludedItemService);
      
      this.setSubscriptions();

      this.listHeaders.set('RepairLines',new ListHeaders());

      this.subscriptions.get('Repair')?.push(this.repairService?.getById(repairId, (repair: Repair) => {
        this.tabrepair = repair;
        this.initializeForm();
        this.selectState = repair.state;
        this.tabrepair.mediaFiles = [];
        this.tabrepair.stateChanges.sort((state1: StateChange, state2: StateChange) => NumberUtils.getComparisonValue(state1.date,state2.date));
        this.initializeClient();
        this.initializeDeviceType();
      }))
    }
  }

  static getCanEdit(url: string): boolean{
    return url.indexOf('editar/') != -1;
  }

  static getRepairId(url: string): string{
    let start: string = '';
    if(this.getCanEdit(url)){
      start = 'editar/';
    }
    else{
      start = 'reparaciones/';
    }
    return url.split(start)[1].split('/')[0];
  }

  static getInstance(repairId: string): EditRepairTabsService {
    if(!this.instance || (this.instance.tabrepair?.id != repairId && this.instance.tabrepair?.id)){
      this.instance = new EditRepairTabsService(repairId)
    }
    return this.instance;
  }

  initializeRepairLines(){
    this.tabrepair.repairLines.forEach( (line) => {
      this.subscriptions.get('Product')?.push(this.productService?.getById(line.productId, (product) => {
        line.product = product;
      }));
    });
  }

  initializeMediaFiles(){
    this.subscriptions.get('MediaFile')?.push(this.mediafileService.getMediafileByRepairId(this.tabrepair.id, (files: MediaFile[]) => this.tabrepair.mediaFiles = files.sort((a,b) => a.name.localeCompare(b.name))));
  }

  initializeClient(){
    this.subscriptions.get('Client')?.push(this.clientService.getById(this.tabrepair.clientId, (client: Client) => this.tabrepair.client = client));
  }

  initializeDeviceType(){
    this.subscriptions.get('DeviceType')?.push(this.deviceTypeService.getById(this.tabrepair.deviceTypeId, (deviceType: DeviceType) => this.tabrepair.deviceType = deviceType));
  }

  getAddedMediaFiles(){
    return this.addedMediaFiles;
  }

  getActiveSubscription(subscriptionKey: string): Subscription {
    return this.subscriptions.get(subscriptionKey)?.find( x => !x?.closed );
  }

  isLoading(subscriptionKey: string): boolean {
    return this.getActiveSubscription(subscriptionKey) != null;
  }
  
  getClientService(): ClientService{
    return this.clientService;
  }

  getDeviceTypeService(): DeviceTypeService{
    return this.deviceTypeService;
  }

  getProductService(): ProductService{
    return this.productService;
  }

  getMediaFileService(): MediafileService{
    return this.mediafileService;
  }

  getIncludedItemService(): IncludedItemService{
    return this.includedItemService;
  }

  getRepairService(): RepairService{
    return this.repairService;
  }

  updateRepair( successHandler: () => void){
    const repairDto = {
      id: this.tabrepair.id,
      problem: this.tabrepair.problem,
      solution: this.tabrepair.solution,
      laborPrice: this.tabrepair.laborPrice,
      devicePassword: this.tabrepair.devicePassword,
      creationDate: this.tabrepair.creationDate,
      clientId: this.tabrepair.clientId,
      deviceTypeId: this.tabrepair.deviceTypeId,
      state: this.selectState,
      includedItems: this.tabrepair.includedItems,
      repairLines: this.tabrepair.repairLines,
      mediaFiles: this.tabrepair.mediaFiles
    };
    this.subscriptions.get('Repair')?.push(this.repairService.update(repairDto, (responseData: Repair) => {

      responseData.stateChanges.sort((a,b) => b.date.toString().localeCompare(a.date.toString()));
      responseData.mediaFiles = this.tabrepair.mediaFiles;
      responseData.repairLines = this.tabrepair.repairLines;
      responseData.client = this.tabrepair.client;
      responseData.deviceType = this.tabrepair.deviceType;
      this.tabrepair = responseData;
      this.initializeForm();
      successHandler();
    }));
    if(this.addedMediaFiles.length > 0){
      this.uploadMediaFiles();
    }
    if(this.deletedMediaFileIds.length > 0){
      this.deleteMediaFiles();
    }
  }

  deleteMediaFiles(){
    this.deletedMediaFileIds.forEach(fileId => this.getActiveSubscription('Repair').add(this.mediafileService.delete(fileId, () => {})));
  }

  uploadMediaFiles(){
    this.addedMediaFiles.forEach(mediaFile => {
      const mediaFileAddDto: MediaFileAddDto = {
        name: mediaFile.name,
        base64Data: mediaFile.base64Data,
        repairId: this.tabrepair.id
      };
      this.getActiveSubscription('Repair').add(this.mediafileService.add(mediaFileAddDto, () => {}));
    })
  }

  setSubscriptions(){
    this.subscriptions.set('Repair',[]);
    this.subscriptions.set('Client',[]);
    this.subscriptions.set('DeviceType',[]);
    this.subscriptions.set('Product',[]);
    this.subscriptions.set('MediaFile',[]);
    this.subscriptions.set('IncludedItem',[]);
  }

  initializeForm(){
    this.form.controls.problem.setValue(this.tabrepair?.problem);
    this.form.controls.solution.setValue(this.tabrepair?.solution);
    this.form.controls.clientId.setValue(this.tabrepair?.clientId);
    this.form.controls.password.setValue(this.tabrepair?.devicePassword);
    this.form.controls.deviceType.setValue(this.tabrepair?.deviceTypeId);
    this.form.controls.laborPrice.setValue(this.tabrepair?.laborPrice);
  }
}
