import { TestBed } from '@angular/core/testing';

import { IncludedItemService } from './included-item.service';

describe('IncludedItemService', () => {
  let service: IncludedItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IncludedItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
