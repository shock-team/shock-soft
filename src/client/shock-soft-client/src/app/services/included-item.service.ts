import { Injectable } from '@angular/core';
import { ServiceDelete } from '../abstract-classes/service/service-delete';
import { ApiService } from './api.service';
import { IncludedItem } from '../entities/IncludedItem'

@Injectable({
  providedIn: 'root'
})
export class IncludedItemService extends ServiceDelete<IncludedItem,IncludedItem,IncludedItem> {

  constructor(apiService: ApiService) {
    super('IncludedItem', apiService);
  }
}
