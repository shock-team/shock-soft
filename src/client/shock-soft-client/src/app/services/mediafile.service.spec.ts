import { TestBed } from '@angular/core/testing';

import { MediafileService } from './mediafile.service';

describe('MediafileService', () => {
  let service: MediafileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MediafileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
