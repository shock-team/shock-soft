import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { MediaFile } from 'src/app/entities/MediaFile';
import { MediaFileAddDto } from 'src/app/entities/MediaFile';
import { ApiService } from './api.service';
import { ServiceDelete } from '../abstract-classes/service/service-delete';

@Injectable({
  providedIn: 'root'
})
export class MediafileService extends ServiceDelete<MediaFile,MediaFileAddDto,MediaFile> {

  constructor(apiService: ApiService) {
    super('MediaFile', apiService);
  }

  getMediafileByRepairId(pRepairId: string, pHandler: (mediaFiles: MediaFile[]) => void): Subscription{
    return this.apiService.getEverything<MediaFile>(this.apiURL+'?',pRepairId,(mediaFiles) => pHandler(mediaFiles));
  }

  downloadMediafile(pId: string, pHandler: (mediaFile: Blob) => void): Subscription{
    return this.apiService.download<Blob>(this.apiURL+'/'+pId+'/download',(mediaFile) => pHandler(mediaFile));
  } 
}
