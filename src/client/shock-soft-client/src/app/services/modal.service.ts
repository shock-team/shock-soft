import { Component, ComponentRef, Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DeletionDialogPage } from '../views/deletion-dialog/deletion-dialog.page';
import { SingleFieldCreateEditPage } from '../views/single-field-create-edit/single-field-create-edit.page';
import { MediaFileDisplayPage } from '../views/media-file-display/media-file-display.page';
import { MediaFile } from '../entities/MediaFile';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private modalController: ModalController) { }

  async CreateAndEditModal(template: string, submitHandler: (entity: any, pHandler: () => void) => Subscription,
  retrievalHandler?: (pHandler: (entity: any) => void) => Subscription){
    const modal = await this.modalController.create({
      component: SingleFieldCreateEditPage,
      componentProps: {
        template: template,
        submitHandler: submitHandler,
        retrievalHandler: retrievalHandler
      },
      cssClass: 'single-field-modal modal-align'
    });

    await modal.present();
  }

  async DeleteModal(template: string, entity: any, deletionHandler: (pHandler: () => void) => Subscription){
    const modal = await this.modalController.create({
      component: DeletionDialogPage,
      componentProps: {
        template: template,
        entity: entity,
        deletionHandler: deletionHandler
      },
      cssClass: 'deletion-modal modal-align'
    });

    await modal.present();
  }

  async ViewMediaFileModal(entity: MediaFile){
    const modal = await this.modalController.create({
      component: MediaFileDisplayPage,
      componentProps: {
        entity: entity
      },
      cssClass: 'media-file-modal modal-align'
      
    }); 

    await modal.present();
  }
}
