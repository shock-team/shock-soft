import { Injectable } from '@angular/core';
import { ServiceDelete } from '../abstract-classes/service/service-delete';
import { Product } from '../entities/Product';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends ServiceDelete<Product,Product,Product> {

  constructor(apiService: ApiService) {
    super('Product', apiService);
  }
}
 