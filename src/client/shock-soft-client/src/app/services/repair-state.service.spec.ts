import { TestBed } from '@angular/core/testing';

import { RepairStateService } from './repair-state.service';

describe('RepairStateService', () => {
  let service: RepairStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RepairStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
