import { Injectable } from '@angular/core';
import { ServiceDelete } from '../abstract-classes/service/service-delete';
import { StateChange } from '../entities/RepairStateLine';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class RepairStateService extends ServiceDelete<StateChange,StateChange,StateChange> {

  constructor(apiService: ApiService) { 
    super('StateChange', apiService);
  }
}
