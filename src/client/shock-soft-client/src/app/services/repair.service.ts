import { Injectable } from '@angular/core';
import { Repair, RepairDto, RepairAddDto } from '../entities/Repair';
import { ApiService } from './api.service';
import { Filter } from '../entities/Filter';
import { ServiceAddUpdate } from '../abstract-classes/service/service-add-update';

@Injectable({
  providedIn: 'root'
})
export class RepairService extends ServiceAddUpdate<Repair,RepairAddDto,RepairDto> {

  constructor(apiService: ApiService) {
    super('Repair', apiService);
  }

  protected generateFilters(filterStrings: string[]): Filter[] {
    const filters: Filter[] = [];

    if(filterStrings[0].length > 0){
      filters.push({
        parameter: 'State',
        value: filterStrings[0]
      });
    };

    if(filterStrings[1].length > 0){
      filters.push({
        parameter: 'ClientName',
        value: filterStrings[1]
      });
    };

    if(filterStrings[2].length > 0){
      filters.push({
        parameter: 'ClientSurname',
        value: filterStrings[2]
      });
    }
    return filters;
  }
}
