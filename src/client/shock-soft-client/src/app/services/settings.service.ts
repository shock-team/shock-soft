import { Injectable } from '@angular/core';
import { Setting } from '../entities/Setting';
import { ApiService } from './api.service';
import applicationSettings from '../application-settings.json'

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private apiURL;

  constructor(private apiService: ApiService) {
    this.apiURL = 'Settings';
  }

  getAll(pHandler: () => void, pErrorHandler?: () => void){
    this.apiService.getAll<Setting>(this.apiURL,[],'1','50',pHandler,pErrorHandler);
  }

  getByName(pName: string): string{
    return applicationSettings.find(x => x.name == pName).value;
  }
}
