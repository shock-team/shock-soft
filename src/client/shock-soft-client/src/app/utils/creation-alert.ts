import { AlertController } from '@ionic/angular';
import { Toast } from './toast';

export class CreationAlert{

  static async createAlert(pDisplayedString: string, pAcceptHandler: (pName: string, pId?: string) => void, pDefaultName?: string){
    const alertController = new AlertController();
    const alert = await alertController.create({
      header: pDisplayedString,
      inputs: [
        {
          name: 'name',
          type: 'text',
          label: 'Nombre: ',
          placeholder: 'Nombre',
          value: pDefaultName,
          max: 65000,
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass:'icon-color',
          handler: () => {}
        },
        {
          text: 'Aceptar',
          cssClass:'icon-color',
          handler: (alertData) => {
            if(alertData.name !== '') {
              pAcceptHandler(alertData.name);
              Toast.createToast('Operación realizada correctamente', false);
            }
            else{
              Toast.createToast('Debe ingresar un Nombre', true);
              this.createAlert(pDisplayedString, (pName, pId?) => pAcceptHandler(pName,pId), pDefaultName);
            }
          }
        }
      ]
    });
    alert.present();
  }
}
