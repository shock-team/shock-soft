import { AlertController } from '@ionic/angular';
import { Toast } from './toast';

export class DeletionAlert{

  static async createAlert(pEntity: string, pAcceptHandler: () => void){
    const alertController = new AlertController();
    const alert = await alertController.create({
      header: 'Borrar ' + pEntity,
      message: '¿Está seguro de que desea eliminar ' + pEntity + '?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass:'icon-color',
          handler: () => {}
        },
        {
          text: 'Aceptar',
          cssClass:'icon-color',
          handler: () => {
            pAcceptHandler();
            Toast.createToast('La operación ha sido realizada correctamente');
          }
        }
      ]
    });
    alert.present();
  }
}
