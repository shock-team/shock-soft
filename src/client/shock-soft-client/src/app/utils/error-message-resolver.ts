import { HttpErrorResponse } from "@angular/common/http";
import errorMessages from './error-messages.json'

export class ErrorMessageResolver {

    static GetErrorMessage(error: any): string { 
        if(error?.status >= 0){
            let errorType = 'Unknown';
            switch(error.status){
                case 0: {
                    errorType = 'No response';
                    break;
                }
                case 400: {
                    errorType = this.GetBadRequestError(error.url);
                    break;
                }
                case 404: {
                    errorType = this.GetNotFoundError(error.url);
                    break;
                }
                case 500: {
                    errorType = this.GetServerError(error);
                    break;
                }
                default: {
                    break;
                }
            }
            return errorMessages[errorType];
        }
        else {
            return errorMessages[error.message ?? error] ?? error;
        }
    }

    static GetServerError(error: HttpErrorResponse): string {
        let serverError = 'Unknown';
        if(error.error.includes('SQL')){
            serverError = this.GetEntity(error.url) + ' ' + this.GetConstraint(error.error);
        }
        return serverError;
    }

    static GetBadRequestError(url: string): string {
        return this.GetEntity(url) + ' INVALID ID';
    }

    static GetNotFoundError(url: string): string {
        return this.GetEntity(url) + ' NOT FOUND';
    }

    private static GetEntity(url: string): string {
        return url.split('api/')[1].split('/')[0];;
    }

    private static GetConstraint(error: string): string {
        return error.split(": '")[1].split(' constraint')[0];
    }
}