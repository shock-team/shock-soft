import { DeviceType } from "../entities/DeviceType";
import { IncludedItem } from "../entities/IncludedItem";
import { Product } from "../entities/Product";

//The purpose of this class is to create an instance of a class from a string
//corresponding to its name
export class InstanceCreator {

    //Creates an instance of a class based on its name
    static CreateInstance(className: string): any {
        let newInstance: any;
        switch(className){
            case 'DeviceType': newInstance = new DeviceType();
            case 'IncludedItem': newInstance = new IncludedItem()
            case 'Product': newInstance = new Product();
            default: null;
        }
        return newInstance;
    }
}