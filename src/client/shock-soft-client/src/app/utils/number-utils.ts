export class NumberUtils{

    public static getInputValue(currentValue: number, minValue: number, maxValue: number): number {
        const MAX_LENGTH = 100;
        const newValue = Number.parseFloat(currentValue?.toString().slice(0,MAX_LENGTH));
        if(newValue >= minValue){
            return Math.min(newValue,maxValue)
        }
        else {
            return minValue;
        }
    }

    public static getComparisonValue(value1: any, value2: any): number {
        return 0 - (value1 > value2 ? 1 : -1);
    }
}