import { ToastController } from '@ionic/angular';

export class Toast{

  static async createToast(pText: string, isError: boolean = false){
    if(pText != '$$'){
      const toastController = new ToastController();
      let toastColor;
      let toastHeader;
      if(isError){
        toastColor = 'danger';
        toastHeader = 'Error';
      }
      else{
        toastColor = 'success';
        toastHeader = 'Éxito';
      }
      const toast = await toastController.create({
        header: toastHeader,
        message: pText,
        duration: 1500,
        translucent: true,
        position: 'top',
        color: toastColor
      });
      toast.present();
    }
  }
}
