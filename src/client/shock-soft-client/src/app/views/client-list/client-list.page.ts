import { Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'src/app/entities/Client';
import { ClientService } from 'src/app/services/client.service';
import { SettingsService } from 'src/app/services/settings.service';
import { ModalService } from '../../services/modal.service';
import { EntityListPageControllerModal } from '../../abstract-classes/entity-controller/entity-list-page-controller-modal';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.page.html',
  styleUrls: ['./client-list.page.scss'],
})
export class ClientListPage extends EntityListPageControllerModal implements OnInit {

  editionHandler: (client: Client) => void = (client: Client) => this.router.navigate(['clientes/editar/',client.id]);
  detailsHandler: (clientId: string) => void = (clientId: string) => this.router.navigate(['clientes/',clientId]);
  
  constructor(clientService: ClientService, settingService: SettingsService, private router: Router,modalService: ModalService,private authService: AuthenticationService) {
    super(2,'Client',clientService,settingService,modalService);
  }

  ngOnInit() {}
  
  onClickedNew() {
    this.router.navigateByUrl('clientes/nuevo');
  }

  onLogoutClicked() {
    this.authService.logOut();
  }
}
