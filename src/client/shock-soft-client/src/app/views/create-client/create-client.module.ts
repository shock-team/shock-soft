import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { CreateClientPageRoutingModule } from './create-client-routing.module';

import { CreateClientPage } from './create-client.page';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ComponentsModule } from '../../modules/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    CreateClientPageRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentsModule
  ],
  declarations: [CreateClientPage]
})
export class CreateClientPageModule {}
