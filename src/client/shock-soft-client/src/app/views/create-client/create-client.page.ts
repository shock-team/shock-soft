import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms'
import { ClientService } from 'src/app/services/client.service';
import { CityService } from 'src/app/services/city.service';
import { City } from 'src/app/entities/City';
import { Client } from 'src/app/entities/Client';
import { Toast } from 'src/app/utils/toast';
import { SettingsService } from 'src/app/services/settings.service';
import { OperationType } from '../../entities/OperationType';
import { Subscription } from 'rxjs';
import { ChangeDetectorRef, AfterContentChecked} from '@angular/core';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.page.html',
  styleUrls: ['./create-client.page.scss'],
})
export class CreateClientPage implements OnInit, AfterContentChecked {

  client: Client = new Client();

  pageName: string;

  operationType: OperationType;

  cities: City[];

  initialSubscription: Subscription;

  form = new FormGroup({
    name: new FormControl(this.client.name, Validators.required),
    surname: new FormControl(this.client.surname, Validators.required),
    phoneNumber: new FormControl(this.client.phoneNumber),
    cityId: new FormControl(this.client.cityId, Validators.required),
    address: new FormControl(this.client.address),
    email: new FormControl(this.client.email, Validators.email)
  });

  refreshHandler: (pageNumber: string, pageSize: string, params: string[],
    pHandler: (array: any[]) => void) => Subscription;

  selectHandler: (city: City) => void;

  initialCityHandler: (pHandler: (city: City) => void) => void;

  clientSubscription: Subscription;

  constructor(private clientService: ClientService, private router: Router, private activatedRoute: ActivatedRoute,
              private cityService: CityService, private settingService: SettingsService, private changeDetectorRef: ChangeDetectorRef) { }
  
    ngOnInit() {
    this.refreshHandler = (pageNumber: string, pageSize: string, params: string[], pHandler: (array: any[]) => void) => this.cityService.getAll(pageNumber,pageSize,params, pHandler);
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if(paramMap.has('clientId')) {
        const id = paramMap.get('clientId');
        this.clientSubscription = this.clientService.getById(id, (client: Client) => {
          this.setClient(client);
          if(client.cityId != '00000000-0000-0000-0000-000000000000') {
            this.form.controls.cityId.setValue(this.client.cityId);
            this.initialSubscription = this.cityService.getById(this.client.cityId, (city: City) => this.client.city = city);
          }
        });
        if(this.router.url.toString().includes('editar')){
          this.pageName = 'Editar Cliente';
          this.operationType = OperationType.Update;
        }
        else{
          this.pageName = 'Detalles del Cliente';
          this.operationType = OperationType.Read;
        }
      }
    else{
      this.pageName = 'Nuevo Cliente';
      this.operationType = OperationType.Create;
      this.client = new Client();
    }});
    this.selectHandler = (city) => { 
      this.client.city = city;
      this.client.cityId = city?.id ?? null;
      this.form.controls.cityId.setValue(this.client.cityId);
    }
  }

  ngAfterContentChecked() { this.changeDetectorRef.detectChanges(); }

  async onClickedAccept(){
    const clientDto = {
      id: this.client.id,
      name: this.client.name,
      surname: this.client.surname,
      phoneNumber: this.client.phoneNumber,
      email: this.client.email,
      address: this.client.address,
      cityId: this.client.cityId
    }
    if(this.operationType == OperationType.Update){
      this.clientSubscription = this.clientService.update(clientDto, () => this.successHandler('Cliente editado exitosamente'));
    }
    else{
      this.clientSubscription = this.clientService.add(clientDto, () => this.successHandler('Cliente creado exitosamente'));
    }
  }

  successHandler(successMessage: string){
    Toast.createToast(successMessage);
    this.router.navigate(['/clientes']);
  }

  setClient(client: Client){
    this.client = client;
  }

  isDetails() {
    return this.operationType == OperationType.Read;
  }

  isLoading(): boolean {
    return this.clientSubscription && !this.clientSubscription?.closed;
  }

  notFound(): boolean {
    return (this.operationType != OperationType.Create) && !this.client.id
  }
}
