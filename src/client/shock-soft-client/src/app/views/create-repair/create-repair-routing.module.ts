import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateRepairPage } from './create-repair.page';

const routes: Routes = [
  {
    path: '',
    component: CreateRepairPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateRepairPageRoutingModule {}
