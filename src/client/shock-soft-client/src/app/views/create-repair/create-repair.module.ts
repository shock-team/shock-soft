import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { IonicModule } from '@ionic/angular';

import { CreateRepairPageRoutingModule } from './create-repair-routing.module';

import { CreateRepairPage } from './create-repair.page';

import { ComponentsModule } from '../../modules/components/components.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateRepairPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [CreateRepairPage]
})
export class CreateRepairPageModule {}
