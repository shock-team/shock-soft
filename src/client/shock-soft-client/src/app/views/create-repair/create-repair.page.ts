import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/app/services/client.service';
import { DeviceTypeService } from 'src/app/services/device-type.service';
import { IncludedItemService } from 'src/app/services/included-item.service';
import { RepairService } from 'src/app/services/repair.service';
import { Router } from '@angular/router';
import { Toast } from 'src/app/utils/toast';
import { Client } from 'src/app/entities/Client';
import { SettingsService } from 'src/app/services/settings.service';
import { Repair, RepairAddDto } from '../../entities/Repair';
import { IncludedItem } from '../../entities/IncludedItem';
import { DeviceType } from '../../entities/DeviceType';
import { FormControl, FormGroup, Validators, ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms'
import { Subscription } from 'rxjs';
import { ChangeDetectorRef, AfterContentChecked} from '@angular/core';


@Component({
  selector: 'app-create-repair',
  templateUrl: './create-repair.page.html',
  styleUrls: ['./create-repair.page.scss'],
})
export class CreateRepairPage implements OnInit {

  repair: Repair = new Repair(); 

  clients: Client[];

  includedItems: IncludedItem[];

  deviceTypes: DeviceType[];

  selectedSegment: string = '0';

  form = new FormGroup({
    deviceTypeId: new FormControl(this.repair.deviceTypeId, Validators.required),
    clientId: new FormControl(this.repair.clientId, Validators.required),
    creationDate: new FormControl(this.repair.creationDate, Validators.required),
    password: new FormControl(this.repair.devicePassword),
    problem: new FormControl(this.repair.problem, Validators.required),
  });

  refreshClientHandler: (pageNumber: string, pageSize: string, params: string[],
    pHandler: (array: any[]) => void) => Subscription;
  refreshDeviceTypeHandler: (pageNumber: string, pageSize: string, params: string[],
    pHandler: (array: any[]) => void) => Subscription;

  selectClientHandler: (client: Client) => void;
  selectDeviceTypeHandler: (deviceType: DeviceType) => void;

  initialClientHandler: (pHandler: (client: Client) => void) => void;
  initialDeviceTypeHandler: (pHandler: (deviceType: DeviceType) => void) => void;

  repairSubscription: Subscription;

  constructor(private repairService: RepairService, private clientService: ClientService,
              private includedItemService: IncludedItemService, private deviceTypeService: DeviceTypeService,
              private router: Router, private settingService: SettingsService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.refreshClientHandler = (pageNumber: string, pageSize: string, params: string[], pHandler: (array: any[]) => void) => this.clientService.getAll(pageNumber,pageSize,params, pHandler);
    this.refreshDeviceTypeHandler = (pageNumber: string, pageSize: string, params: string[], pHandler: (array: any[]) => void) => this.deviceTypeService.getAll(pageNumber,pageSize,params, pHandler);
    
    this.repair = new Repair();
    
    this.includedItemService.getAll('10','1',[''], (array) => this.includedItems = array);
    
    this.selectClientHandler = (client) => { 
      this.repair.client = client;
      this.repair.clientId = client?.id ?? null;
      this.form.controls.clientId.setValue(this.repair.clientId);
    };
    this.selectDeviceTypeHandler = (deviceType) => { 
      this.repair.deviceType = deviceType;
      this.repair.deviceTypeId = deviceType?.id ?? null;
      this.form.controls.deviceTypeId.setValue(this.repair.deviceTypeId);
    }
    
  }

  ngAfterContentChecked() { this.changeDetectorRef.detectChanges(); }

  onChangedDate(event){
    this.repair.creationDate = event.detail.value;
    this.form.controls.creationDate.setValue(this.repair.creationDate);
  }

  onCheckedItem(item: IncludedItem,event){
    if(!event.detail.checked){
      const itemIndex = this.repair.includedItems.findIndex((includedItem: IncludedItem) => (includedItem.id == item.id));
      this.repair.includedItems.splice(itemIndex,1);
    }
    else{
      this.repair.includedItems.push(item);
    }
  }

  isSelected(pItem: any): boolean {
    const index = this.repair.includedItems.findIndex((includedItem: IncludedItem) => (includedItem.id == pItem.id));
    return index >= 0;
  }

  onChangedSegment(event){
    this.selectedSegment = event.detail.value;
  }

  isGeneralDataSelected(): boolean{
    return this.selectedSegment === '0';
  }

  async onClickedAccept(){
    await this.repairService.add(
      {
        problem: this.repair.problem,
        deviceTypeId: this.repair.deviceTypeId,
        clientId: this.repair.clientId,
        creationDate: this.repair.creationDate, 
        devicePassword: this.repair.devicePassword,
        includedItems: this.repair.includedItems
      },
      () => this.successHandler('Reparación creada exitosamente'));
  }

  successHandler(successMessage: string){
    Toast.createToast(successMessage);
    this.router.navigate(['/reparaciones']);
  }

  isLoading(): boolean {
    return this.repairSubscription && !this.repairSubscription?.closed;
  }
}
