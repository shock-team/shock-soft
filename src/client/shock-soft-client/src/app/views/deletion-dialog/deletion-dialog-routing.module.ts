import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeletionDialogPage } from './deletion-dialog.page';

const routes: Routes = [
  {
    path: '',
    component: DeletionDialogPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeletionDialogPageRoutingModule {}
