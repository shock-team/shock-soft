import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeletionDialogPageRoutingModule } from './deletion-dialog-routing.module';

import { DeletionDialogPage } from './deletion-dialog.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeletionDialogPageRoutingModule
  ],
  declarations: [DeletionDialogPage]
})
export class DeletionDialogPageModule {}
