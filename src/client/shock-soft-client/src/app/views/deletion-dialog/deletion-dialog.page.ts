import { AfterContentChecked, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DeletionDialogData } from '../../entities/DeletionDialogData';
import { Toast } from '../../utils/toast';
import deletionDialogTemplates from './templates.json'

@Component({
  selector: 'app-deletion-dialog',
  templateUrl: './deletion-dialog.page.html',
  styleUrls: ['./deletion-dialog.page.scss'],
})
export class DeletionDialogPage implements OnInit, OnDestroy, AfterContentChecked {

  @Input() template: string;
  @Input() entity: any;
  @Input() deletionHandler: (pHandler: () => void) => Subscription;

  dialogData: DeletionDialogData;

  subscription: Subscription;

  constructor(private modalController: ModalController, private changeDetectorRef: ChangeDetectorRef) { }

  ngAfterContentChecked() { this.changeDetectorRef.detectChanges(); }
  
  ngOnInit() {
    this.dialogData = deletionDialogTemplates[this.template];
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  onClickedNo(){
    this.modalController.dismiss();
  }

  onClickedYes(){
    this.subscription = this.deletionHandler(() => {
      Toast.createToast(this.dialogData.successMessage);
    })
    this.modalController.dismiss();
  }

  isLoading(){
    return this.subscription && (!this.subscription?.closed);
  }

  getDescription(): string{
    let description: string = '';
    this.dialogData.descriptionFields.forEach( field => description += this.entity[field] + ' ');
    return description.trim()+'?';
  }

}
