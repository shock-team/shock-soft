import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeviceTypeListPage } from './device-type-list.page';

const routes: Routes = [
  {
    path: '',
    component: DeviceTypeListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceTypeListPageRoutingModule {}
