import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeviceTypeListPageRoutingModule } from './device-type-list-routing.module';

import { DeviceTypeListPage } from './device-type-list.page';
import { ComponentsModule } from '../../modules/components/components.module';
import { ModalsModule } from '../../modules/modals/modals.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeviceTypeListPageRoutingModule,
    ComponentsModule,
    ModalsModule
  ],
  declarations: [DeviceTypeListPage]
})
export class DeviceTypeListPageModule {}
