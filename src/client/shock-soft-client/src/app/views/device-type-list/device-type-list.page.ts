import { Component, OnInit } from '@angular/core';
import { DeviceTypeService } from 'src/app/services/device-type.service';
import { SettingsService } from 'src/app/services/settings.service';
import { EntityListPageControllerModal } from '../../abstract-classes/entity-controller/entity-list-page-controller-modal';
import { ModalService } from '../../services/modal.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-device-type-list',
  templateUrl: './device-type-list.page.html',
  styleUrls: ['./device-type-list.page.scss'],
})
export class DeviceTypeListPage extends EntityListPageControllerModal implements OnInit {

  constructor(deviceTypeService: DeviceTypeService, settingsService: SettingsService, modalService: ModalService,private authService: AuthenticationService) {
    super(1,'DeviceType',deviceTypeService,settingsService,modalService);
  }

  ngOnInit() {}

  onLogoutClicked() {
    this.authService.logOut();
  }
}
