import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AttachedFilesPage } from './attached-files.page';

const routes: Routes = [
  {
    path: '',
    component: AttachedFilesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttachedFilesPageRoutingModule {}
