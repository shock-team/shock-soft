import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { IonicModule } from '@ionic/angular';

import { AttachedFilesPageRoutingModule } from './attached-files-routing.module';

import { AttachedFilesPage } from './attached-files.page';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ComponentsModule } from '../../../modules/components/components.module';

import { MediaFileDisplayPageModule } from '../../media-file-display/media-file-display.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttachedFilesPageRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentsModule,
    MediaFileDisplayPageModule
  ],
  declarations: [AttachedFilesPage]
})
export class AttachedFilesPageModule {}
