import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MediaFile } from 'src/app/entities/MediaFile';
import { OperationType } from '../../../entities/OperationType';
import { Subscription } from 'rxjs';
import { ModalService } from '../../../services/modal.service';

import { CameraService } from '../../../services/camera.service';
import { EditRepairTabList } from '../../../abstract-classes/edit-repair-tab/edit-repair-tab-list';

@Component({
  selector: 'app-attached-files',
  templateUrl: './attached-files.page.html',
  styleUrls: ['./attached-files.page.scss'],
})
export class AttachedFilesPage extends EditRepairTabList implements OnInit {

  file : MediaFile = null;
  dropdown: boolean = false;
  fileName = '';

  previewWidth: number = 0;
  previewHeight: number = 0;

  operationType: OperationType;

  repairSubscription: Subscription;
  cardSubscriptions: Subscription[];

  mediaFileHandler: (fileId: string, successHandler: (fileData: Blob) => void) => Subscription = (fileId,successHandler) => {
    return this.editRepairTabsService?.mediafileService.downloadMediafile(fileId,successHandler);
  };

  viewHandler:(item: any) => void = (item: any) => {
    this.modalService.ViewMediaFileModal(item);
  }

  setFileHandler: (base64: string) => void;
  
  constructor(router: Router, changeDetectorRef: ChangeDetectorRef, private cameraService: CameraService, private modalService: ModalService) {
    super('MediaFile',router,changeDetectorRef);
  }

  ngOnInit() {
    if(this.editRepairTabsService.isLoading('Repair')){
      this.editRepairTabsService.getActiveSubscription('Repair').add(() => this.editRepairTabsService.initializeMediaFiles())
    }

    else{
      if(!this.editRepairTabsService.tabrepair?.mediaFiles || this.editRepairTabsService.tabrepair?.mediaFiles?.length == 0){
        this.editRepairTabsService.initializeMediaFiles();
      }  
    }
    
    this.setFileHandler = (base64: string) => {
      this.file = {
        name: '',
        base64Data: base64,
        repairId: this.editRepairTabsService.tabrepair?.id
      };
      this.cameraService.setPreviewSize(base64,0.4,0.4,(width: number, height: number) => {
        this.previewWidth = width;
        this.previewHeight = height;
      })
    }

    this.deletionHandler = (file: MediaFile) => {
      const indexToBeDeleted = this.editRepairTabsService.tabrepair?.mediaFiles.findIndex(mediaFile => mediaFile.id == file.id);
      this.editRepairTabsService.deletedMediaFileIds.push(file.id);
      this.editRepairTabsService.tabrepair?.mediaFiles.splice(indexToBeDeleted,1);
    }
  }

  onClickedTakePhoto(){
    this.cameraService.takePhoto(this.setFileHandler);
  }

  onClickedUploadPhoto(){
    this.cameraService.uploadPhoto(this.setFileHandler);
  }

  canUpload(): boolean{
    return this.file?.base64Data && (this.fileName?.length > 0)
  }

  onClickedAddPhoto(){
    this.file.name = this.fileName;
    this.editRepairTabsService.addedMediaFiles.push(this.file);
    this.editRepairTabsService.tabrepair.mediaFiles.push(this.file);
    this.fileName = '';
    this.file = null;
  }

  showPreview():boolean {
    return this.file && (this.previewHeight > 0) && (this.previewWidth > 0);
  }

}
