import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { GeneralInformationPageRoutingModule } from './general-information-routing.module';
import { GeneralInformationPage } from './general-information.page';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ComponentsModule } from '../../../modules/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeneralInformationPageRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentsModule
  ],
  declarations: [GeneralInformationPage]
})
export class GeneralInformationPageModule {}
