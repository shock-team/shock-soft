import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'src/app/entities/Client';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DeviceType } from 'src/app/entities/DeviceType';
import { Subscription } from 'rxjs';
import { NumberUtils } from '../../../utils/number-utils';
import { EditRepairTab } from '../../../abstract-classes/edit-repair-tab/edit-repair-tab';

@Component({
  selector: 'app-general-information',
  templateUrl: './general-information.page.html',
  styleUrls: ['./general-information.page.scss'],
})
export class GeneralInformationPage extends EditRepairTab implements OnInit {

  total: number;

  clients;
  deviceTypes;

  minLaborPrice: number = 0;
  maxLaborPrice: number = 99999999;

  selectedClientId: string;
  selectedClientText: string;
  selectedClient: Client;

  selectedDeviceTypeId: string;
  selectedDeviceTypeText: string;

  refreshClientHandler: (pageNumber: string, pageSize: string, params: string[],
    pHandler: (array: any[]) => void) => Subscription;
  refreshDeviceTypeHandler: (pageNumber: string, pageSize: string, params: string[],
    pHandler: (array: any[]) => void) => Subscription;

  selectClientHandler: (client: Client) => void;
  selectDeviceTypeHandler: (deviceType: DeviceType) => void;

  initialClientHandler: (pHandler: (client: Client) => void) => void;
  initialDeviceTypeHandler: (pHandler: (deviceType: DeviceType) => void) => void;

  constructor(router: Router, changeDetectorRef: ChangeDetectorRef) {
    super('Repair',router,changeDetectorRef)
  }

  ngOnInit() {

    if(!this.editRepairTabsService.isLoading('Repair')){
      if(!this.editRepairTabsService?.tabrepair?.client && !this.editRepairTabsService.isLoading('Client')){
        this.editRepairTabsService.initializeClient();
      }
      if(!this.editRepairTabsService?.tabrepair?.deviceType && !this.editRepairTabsService.isLoading('DeviceType')){
        this.editRepairTabsService.initializeDeviceType();
      }
    }
    
    this.refreshClientHandler = (pageNumber: string, pageSize: string, params: string[], pHandler: (array: any[]) => void) => this.editRepairTabsService.getClientService().getAll(pageNumber,pageSize,params, pHandler);

    this.refreshDeviceTypeHandler = (pageNumber: string, pageSize: string, params: string[], pHandler: (array: any[]) => void) => this.editRepairTabsService.getDeviceTypeService().getAll(pageNumber,pageSize,params, pHandler);
    
    this.selectClientHandler = (client) => { 
      this.editRepairTabsService.tabrepair.client = client;
      this.editRepairTabsService.tabrepair.clientId = client?.id ?? null;
      this.editRepairTabsService.form.controls.clientId.setValue(this.editRepairTabsService.tabrepair.clientId);
    };
    
    this.selectDeviceTypeHandler = (deviceType) => { 
      this.editRepairTabsService.tabrepair.deviceType = deviceType;
      this.editRepairTabsService.tabrepair.deviceTypeId = deviceType?.id ?? null;
      this.editRepairTabsService.form.controls.deviceType.setValue(this.editRepairTabsService.tabrepair?.deviceTypeId);
    };
  }

  getTotal(): number{
    this.total = 0;
    this.editRepairTabsService.tabrepair?.repairLines.forEach((repairLine) => {
      this.total = this.total + (repairLine.price*repairLine.quantity);
    });
    this.total += this.editRepairTabsService.tabrepair?.laborPrice;
    return this.total;
  }

  onLaborPriceChanged(value: number) {
    this.editRepairTabsService.tabrepair.laborPrice = NumberUtils.getInputValue(value,this.minLaborPrice,this.maxLaborPrice);
    (document.getElementById('laborPrice') as HTMLInputElement).value = this.editRepairTabsService.tabrepair.laborPrice.toString();
  }
}
