import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncludedObjectsPage } from './included-objects.page';

const routes: Routes = [
  {
    path: '',
    component: IncludedObjectsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IncludedObjectsPageRoutingModule {}
