import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { IonicModule } from '@ionic/angular';

import { IncludedObjectsPageRoutingModule } from './included-objects-routing.module';

import { IncludedObjectsPage } from './included-objects.page';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';



import { ComponentsModule } from '../../../modules/components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IncludedObjectsPageRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentsModule
  ],
  declarations: [IncludedObjectsPage]
})
export class IncludedObjectsPageModule {}
