import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IncludedItem } from 'src/app/entities/IncludedItem';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { EditRepairTabList } from '../../../abstract-classes/edit-repair-tab/edit-repair-tab-list';

@Component({
  selector: 'app-included-objects',
  templateUrl: './included-objects.page.html',
  styleUrls: ['./included-objects.page.scss'],
})
export class IncludedObjectsPage extends EditRepairTabList implements OnInit {

  items: IncludedItem[];
  newitem: IncludedItem = null;
  newitemId: string = null;
  selectItem = '';
  dropdown: boolean = false;

  form = new FormGroup({
    includedItemId: new FormControl(this.newitemId)
  });

  refreshItemHandler: (pageNumber: string, pageSize: string, params: string[],
    pHandler: (array: any[]) => void) => Subscription;
 
  selectItemHandler: (includedItem: IncludedItem) => void;

  constructor(router: Router,changeDetectorRef: ChangeDetectorRef) {
    super('IncludedItem',router,changeDetectorRef);
  }

  ngOnInit() {
    this.refreshItemHandler = (pageNumber: string, pageSize: string, params: string[], pHandler: (array: any[]) => void) => this.editRepairTabsService?.getIncludedItemService().getAll(pageNumber,pageSize,params, pHandler);
    
    this.selectItemHandler = (includedItem) => { 
      this.newitem = includedItem;
      this.newitemId = includedItem?.id ?? null;
      this.form.controls.includedItemId.setValue(this.newitemId);
    };

    this.deletionHandler = (item: any) => {
      const indexToBeDeleted = this.editRepairTabsService.tabrepair?.includedItems.findIndex(includedItem => includedItem === item);
      this.editRepairTabsService.tabrepair?.includedItems.splice(indexToBeDeleted,1);
    }
  }

  onClickedAdd(){
    this.editRepairTabsService.tabrepair?.includedItems.push(this.newitem);
    this.newitem = null;   
  }

  canAddSelectedItem(){
    return this.editRepairTabsService.tabrepair?.includedItems.findIndex(x => x?.id == this.newitem?.id) == -1;
  }
}
