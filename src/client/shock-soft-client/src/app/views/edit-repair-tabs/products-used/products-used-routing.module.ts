import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsUsedPage } from './products-used.page';

const routes: Routes = [
  {
    path: '',
    component: ProductsUsedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsUsedPageRoutingModule {}
