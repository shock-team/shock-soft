import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { IonicModule } from '@ionic/angular';

import { ProductsUsedPageRoutingModule } from './products-used-routing.module';

import { ProductsUsedPage } from './products-used.page';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ComponentsModule } from '../../../modules/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductsUsedPageRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentsModule
  ],
  declarations: [ProductsUsedPage]
})
export class ProductsUsedPageModule {}
