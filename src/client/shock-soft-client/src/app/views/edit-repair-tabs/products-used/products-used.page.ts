import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/entities/Product';
import { RepairLine } from 'src/app/entities/RepairLine';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NumberUtils } from '../../../utils/number-utils';
import { EditRepairTabList } from '../../../abstract-classes/edit-repair-tab/edit-repair-tab-list';

@Component({
  selector: 'app-products-used',
  templateUrl: './products-used.page.html',
  styleUrls: ['./products-used.page.scss'],
})
export class ProductsUsedPage extends EditRepairTabList implements OnInit {

  selectProduct: Product;
  selectProductId: string = null;

  newPrice: number = 0;
  minPrice: number = 0;

  newQuantity: number = 1;
  minQuantity: number = 1;

  inputMaxValue: number = 99999999;
  
  dropdown: boolean = false;
  inputText: string;

  form = new FormGroup({
    ProductId: new FormControl(this.selectProductId)
  });

  lines: RepairLine[] = [];

  refreshProductHandler: (pageNumber: string, pageSize: string, params: string[],
    pHandler: (array: any[]) => void) => Subscription;
 
  selectProductHandler: (includedItem: Product) => void;

  initialProductSubscription: Subscription;

  constructor(router: Router, changeDetectorRef: ChangeDetectorRef) {
    super('Product',router,changeDetectorRef);
  }
  
  ngOnInit() {
    if(this.editRepairTabsService.isLoading('Repair')){
      this.editRepairTabsService.getActiveSubscription('Repair').add(() => this.editRepairTabsService.initializeRepairLines())
    }

    else{
      if(this.editRepairTabsService.tabrepair?.repairLines.findIndex( x => !x.product ) != -1){
        this.editRepairTabsService.initializeRepairLines();
      }  
    }

    this.refreshProductHandler = (pageNumber: string, pageSize: string, params: string[], pHandler: (array: any[]) => void) => this.editRepairTabsService.getProductService().getAll(pageNumber,pageSize,params,pHandler);  
    
    this.selectProductHandler = (product) => { 
      this.selectProduct = product;
      this.selectProductId = product?.id ?? null;
      this.form.controls.ProductId.setValue(this.selectProductId);
    };
    
    this.deletionHandler = (prepairLine: any) => {
      const indexToBeDeleted = this.editRepairTabsService?.tabrepair.repairLines .findIndex(repairline =>
        {
          const searchedRepair = (repairline === prepairLine);
          return searchedRepair;
        });
        this.editRepairTabsService?.tabrepair.repairLines.splice(indexToBeDeleted,1);
    };
  }
  
  getTotal(selectedline: RepairLine){
    return (selectedline.price*selectedline.quantity);
  }

  increaseQuantity(quantity: number){
    quantity += 1;
  }

  decreaseQuantity(prepairLine: RepairLine){
    if (prepairLine.quantity > 1)
    {
      prepairLine.quantity -= 1;
    }
    else
    {
      const indexToBeDeleted = this.editRepairTabsService?.tabrepair.repairLines .findIndex(repairline =>
        {
          const searchedRepair = (repairline === prepairLine);
          return searchedRepair;
        });
      this.editRepairTabsService?.tabrepair.repairLines .splice(indexToBeDeleted,1);
    }
  }

  onClickedAdd(){
    this.editRepairTabsService.tabrepair?.repairLines.push({ quantity: this.newQuantity , price: this.newPrice, productId: this.selectProductId, product: this.selectProduct});
    this.selectProduct = null;
    this.selectProductId = null;
    this.onValueChanged(this.minPrice,'Price');
    this.onValueChanged(this.minQuantity,'Quantity');
    this.editRepairTabsService.tabrepair.repairLines.sort((line1: RepairLine, line2: RepairLine) => NumberUtils.getComparisonValue(line2.product?.name,line1.product?.name));
  }

  onValueChanged(value: number, field: string){
    this['new' + field] = NumberUtils.getInputValue(value,this['min' + field],this.inputMaxValue).toString();
    if ((document.getElementById(field) as HTMLInputElement).value > this.inputMaxValue.toString())
    (document.getElementById(field) as HTMLInputElement).value = this.inputMaxValue.toString();
  }

  canAddSelectedItem(){
    return this.editRepairTabsService.tabrepair?.repairLines.findIndex(x => x?.productId == this.selectProductId && x?.price == this.newPrice) == -1;
  }
}
