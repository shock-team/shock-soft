import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StateHistoryPage } from './state-history.page';

const routes: Routes = [
  {
    path: '',
    component: StateHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StateHistoryPageRoutingModule {}
