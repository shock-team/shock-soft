import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { IonicModule } from '@ionic/angular';

import { StateHistoryPageRoutingModule } from './state-history-routing.module';

import { StateHistoryPage } from './state-history.page';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ComponentsModule } from '../../../modules/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StateHistoryPageRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentsModule
  ],
  declarations: [StateHistoryPage]
})
export class StateHistoryPageModule {}
