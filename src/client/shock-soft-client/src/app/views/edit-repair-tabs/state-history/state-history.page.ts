import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StateChange } from 'src/app/entities/RepairStateLine';
import { RepairState } from 'src/app/entities/RepairState';
import { EditRepairTabList } from '../../../abstract-classes/edit-repair-tab/edit-repair-tab-list';

@Component({
  selector: 'app-state-history',
  templateUrl: './state-history.page.html',
  styleUrls: ['./state-history.page.scss'],
})
export class StateHistoryPage extends EditRepairTabList implements OnInit {

  availableStates: RepairState[];
  selectState : RepairState;

  clickHandler: (state: RepairState) => void = (state: RepairState) => this.selectState = state;
  
  
  constructor(router: Router, changeDetectorRef: ChangeDetectorRef) {
    super('StateChange',router,changeDetectorRef)
  }

  ngOnInit() {
    this.updateStates();
  }


  getRepairStateString(repairState: number): string{
    return RepairState[repairState]?.toString() ?? '';
  }

  updateStates(){
    const totalStates = [0, 1, 2, 3, 4, 5, 6];
    this.availableStates = totalStates;
    const index = totalStates.findIndex(state => state == this.editRepairTabsService.tabrepair?.state);
    this.availableStates.splice(index,1);
  }
 
}
