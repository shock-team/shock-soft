import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditRepairPage } from './edit-repair.page';

const routes: Routes = [
  {
    path: 'edit-repair',
    component: EditRepairPage,
    children: [
      {
        path:'general-information',
        loadChildren: () => import('../edit-repair-tabs/general-information/general-information.module').then( m => m.GeneralInformationPageModule)
      },
      {
        path:'products-used',
        loadChildren: () => import('../edit-repair-tabs/products-used/products-used.module').then( m => m.ProductsUsedPageModule)
      },
      {
        path:'state-history',
        loadChildren: () => import('../edit-repair-tabs/state-history/state-history.module').then( m => m.StateHistoryPageModule)
      },
      {
        path:'attached-files',
        loadChildren: () => import('../edit-repair-tabs/attached-files/attached-files.module').then( m => m.AttachedFilesPageModule)
      },
      {
        path:'included-objects',
        loadChildren: () => import('../edit-repair-tabs/included-objects/included-objects.module').then( m => m.IncludedObjectsPageModule)
      }
    ]
  },
  {
    path: '',
    component: EditRepairPage,
    children: [
      {
        path:'',
        redirectTo: 'general-information',
        pathMatch: 'full'
      },
      {
        path:'general-information',
        loadChildren: () => import('../edit-repair-tabs/general-information/general-information.module').then( m => m.GeneralInformationPageModule)
      },
      {
        path:'products-used',
        loadChildren: () => import('../edit-repair-tabs/products-used/products-used.module').then( m => m.ProductsUsedPageModule)
      },
      {
        path:'state-history',
        loadChildren: () => import('../edit-repair-tabs/state-history/state-history.module').then( m => m.StateHistoryPageModule)
      },
      {
        path:'attached-files',
        loadChildren: () => import('../edit-repair-tabs/attached-files/attached-files.module').then( m => m.AttachedFilesPageModule)
      },
      {
        path:'included-objects',
        loadChildren: () => import('../edit-repair-tabs/included-objects/included-objects.module').then( m => m.IncludedObjectsPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditRepairPageRoutingModule {}
