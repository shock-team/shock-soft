import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { IonicModule } from '@ionic/angular';

import { EditRepairPageRoutingModule } from './edit-repair-routing.module';

import { EditRepairPage } from './edit-repair.page';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ComponentsModule } from '../../modules/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditRepairPageRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentsModule
  ],
  declarations: [EditRepairPage]
})
export class EditRepairPageModule {}
