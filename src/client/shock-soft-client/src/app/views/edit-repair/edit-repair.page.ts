import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EditRepairTabsService } from 'src/app/services/edit-repair-tabs.service';
import { Toast } from 'src/app/utils/toast';
import pageNames from './page-names.json';
import { ViewWillLeave } from '@ionic/angular';

@Component({
  selector: 'app-edit-repair',
  templateUrl: './edit-repair.page.html',
  styleUrls: ['./edit-repair.page.scss'],
})
export class EditRepairPage implements OnInit, ViewWillLeave {

  pageName: string = '';
  editRepairTabsService: EditRepairTabsService

  constructor(private router: Router) { }

  ngOnInit() {
    this.initializePageName();
    this.editRepairTabsService = EditRepairTabsService.getInstance(EditRepairTabsService.getRepairId(this.router.url));
  }

  async updateRepair(){
    this.editRepairTabsService.updateRepair(() => this.successHandler('Reparación editada exitosamente.'));
  }

  isDetails(){
    return !EditRepairTabsService.getCanEdit(this.router.url);
  }

  successHandler(successMessage: string){
    Toast.createToast(successMessage);
    this.router.navigate(['/reparaciones']);
  }

  initializePageName(){
    const section: string = this.router.url.split(EditRepairTabsService.getRepairId(this.router.url)+'/')[1];
    this.pageName = pageNames[section];
  }

  ionViewWillLeave(){
    this.editRepairTabsService.selectState = this.editRepairTabsService.tabrepair?.state;
  }
}
