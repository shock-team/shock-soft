import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncludedItemListPage } from './included-item-list.page';

const routes: Routes = [
  {
    path: '',
    component: IncludedItemListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IncludedItemListPageRoutingModule {}
