import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IncludedItemListPageRoutingModule } from './included-item-list-routing.module';

import { IncludedItemListPage } from './included-item-list.page';

import { ComponentsModule } from '../../modules/components/components.module';
import { ModalsModule } from '../../modules/modals/modals.module';

@NgModule({ 
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IncludedItemListPageRoutingModule,
    ComponentsModule,
    ModalsModule
  ],
  declarations: [IncludedItemListPage]
})
export class IncludedItemListPageModule {}
