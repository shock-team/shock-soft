import { Component, OnInit } from '@angular/core';
import { IncludedItemService } from 'src/app/services/included-item.service';
import { SettingsService } from 'src/app/services/settings.service';
import { ModalService } from '../../services/modal.service';
import { EntityListPageControllerModal } from '../../abstract-classes/entity-controller/entity-list-page-controller-modal';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-included-item-list',
  templateUrl: './included-item-list.page.html',
  styleUrls: ['./included-item-list.page.scss'],
})
export class IncludedItemListPage extends EntityListPageControllerModal implements OnInit {

  constructor(includedItemService: IncludedItemService, settingsService: SettingsService, modalService: ModalService,private authService: AuthenticationService) {
    super(1,'IncludedItem',includedItemService,settingsService,modalService);
  }
 
  ngOnInit() {}

  onLogoutClicked() {
    this.authService.logOut();
  }
}
