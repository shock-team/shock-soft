import { Component, Optional } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-auth',
  templateUrl: './login.page.html',
  styles: [],
})
export class LoginPage {
  
  public userForm: FormGroup = new FormGroup({
    email: new FormControl('',[Validators.email, Validators.required]),
    password: new FormControl('',[Validators.required,Validators.minLength(6)]),
  });

  constructor(
    @Optional() private auth: Auth,
    private authService: AuthenticationService
  ) {}

  onLogin() {
    this.authService.login(this.userForm.value);
  }

  onLoginWithGoogle() {
    this.authService.loginWithGoogle();
  }
}
