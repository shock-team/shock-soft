import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MainMenuPageRoutingModule } from './main-menu-routing.module';

import { MainMenuPage } from './main-menu.page';
import { ClientListPage } from '../client-list/client-list.page';
import { ClientListPageModule } from '../client-list/client-list.module';

@NgModule({
  entryComponents: [ClientListPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainMenuPageRoutingModule,
    ClientListPageModule
  ],
  
  declarations: [MainMenuPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MainMenuPageModule {}
