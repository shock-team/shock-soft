import { Component, OnInit } from '@angular/core';
import { CityService } from 'src/app/services/city.service';
import { Toast } from 'src/app/utils/toast';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.page.html',
  styleUrls: ['./main-menu.page.scss'],
})
export class MainMenuPage {
  constructor(
    private cityService: CityService,
    private authService: AuthenticationService
  ) {}


  async onClickedCities() {
    const controller = new LoadingController();
    const loading = await controller.create({
      message: 'Inicializando localidades. Por favor espere...',
      spinner: 'crescent',
    });
    await loading.present();
    this.cityService.seedCities(
      () => {
        Toast.createToast('Localidades inicializadas exitosamente');
        loading.dismiss();
      },
      () => loading.dismiss()
    );
  }

  onLogoutClicked() {
    this.authService.logOut();
  }
}
