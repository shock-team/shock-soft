import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MediaFileDisplayPage } from './media-file-display.page';

const routes: Routes = [
  {
    path: '',
    component: MediaFileDisplayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MediaFileDisplayPageRoutingModule {}
