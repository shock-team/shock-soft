import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { MediaFileDisplayPageRoutingModule } from './media-file-display-routing.module';

import { MediaFileDisplayPage } from './media-file-display.page';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ComponentsModule } from '../../modules/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MediaFileDisplayPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [MediaFileDisplayPage]
})
export class MediaFileDisplayPageModule {}
