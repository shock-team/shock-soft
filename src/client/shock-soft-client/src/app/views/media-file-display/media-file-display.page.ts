import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { MediafileService } from 'src/app/services/mediafile.service';
import { MediaFile } from 'src/app/entities/MediaFile';


@Component({
  selector: 'app-media-file-display',
  templateUrl: './media-file-display.page.html',
  styleUrls: ['./media-file-display.page.scss'],
})
export class MediaFileDisplayPage implements OnInit {


  @Input() entity: MediaFile;

  pageName: string;
  successMessage: string;

  constructor(private modalController: ModalController, private mediafileService: MediafileService) { }

  ngOnInit() {
    this.pageName = this.entity?.name;
  }

  getFileHandler(fileId: string): (handler: (fileData: Blob) => void) => Subscription {
    return (handler: (fileData: Blob) => void) => this.mediafileService.downloadMediafile(fileId,handler);
  }

  onClickedReturn() {
    this.modalController.dismiss();
  }
}
