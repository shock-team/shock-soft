import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { SettingsService } from 'src/app/services/settings.service';
import { ModalService } from '../../services/modal.service';
import { EntityListPageControllerModal } from '../../abstract-classes/entity-controller/entity-list-page-controller-modal';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.page.html',
  styleUrls: ['./product-list.page.scss'],
})
export class ProductListPage extends EntityListPageControllerModal implements OnInit {

  constructor(productService: ProductService, settingService: SettingsService, modalService: ModalService,private authService: AuthenticationService) {
    super(1,'Product',productService,settingService,modalService);
  }

  ngOnInit() {}

  onLogoutClicked() {
    this.authService.logOut();
  }
}
