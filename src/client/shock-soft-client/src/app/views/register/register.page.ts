import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage {
  public userForm: FormGroup = new FormGroup({
    email: new FormControl('',[Validators.email, Validators.required]),
    password: new FormControl('',[Validators.required,Validators.minLength(6)]),
  });

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  onRegister() {
    this.authenticationService.register(this.userForm.value);
  }
}
