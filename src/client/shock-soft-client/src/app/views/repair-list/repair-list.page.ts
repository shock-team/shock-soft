import { Component, OnInit } from '@angular/core';
import { Repair } from 'src/app/entities/Repair';
import { RepairService } from 'src/app/services/repair.service';
import { DeviceTypeService } from 'src/app/services/device-type.service';
import { ClientService } from 'src/app/services/client.service';
import { SettingsService } from 'src/app/services/settings.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ListHeaders } from 'src/app/entities/ListHeaders';
import { DisplayField } from 'src/app/entities/DisplayField';
import { RepairState } from 'src/app/entities/RepairState';
import { EntityListPageController } from '../../abstract-classes/entity-controller/entity-list-page-controller';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-repair-list',
  templateUrl: './repair-list.page.html',
  styleUrls: ['./repair-list.page.scss'],
})
export class RepairListPage extends EntityListPageController implements OnInit {

  stateFilters: DisplayField[] = [
    {
      displayName: 'Todas',
      field: ''
    }
  ]

  editionHandler: (repair: Repair) => void = (repair: Repair) => this.router.navigate(['./reparaciones/editar/', repair.id]);
  detailsHandler: (repairId: string) => void = (repairId: string) => this.router.navigate(['./reparaciones/', repairId]);
  clickHandler: (state: string) => void = (state: string) => this.search(state,0);

  constructor(repairService: RepairService, private deviceTypeService: DeviceTypeService,
    private clientService: ClientService, settingsService: SettingsService,
    private router: Router, private authService: AuthenticationService) {
      super(3,'Repair',repairService,settingsService);
    }

  ngOnInit() {
    Object.keys(RepairState).slice(0,Object.keys(RepairState).length / 2).forEach( state => this.stateFilters.push({
      displayName: RepairState[state],
      field: state}))
  }

  onClickedNew() {
    this.router.navigate(['./nueva']);
  }

  setData(dataArray: Repair[], listHeaders: ListHeaders) {
    dataArray.forEach(repair => {
      this.subscriptions.push(this.clientService.getById(repair.clientId, (client) => repair.client = client));
      this.subscriptions.push(this.deviceTypeService.getById(repair.deviceTypeId, (deviceType) => repair.deviceType = deviceType));
    });
    this.data = dataArray;
    this.listHeaders = listHeaders;
  }

  onLogoutClicked() {
    this.authService.logOut();
  }
}
