import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingleFieldCreateEditPage } from './single-field-create-edit.page';

const routes: Routes = [
  {
    path: '',
    component: SingleFieldCreateEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SingleFieldCreateEditPageRoutingModule {}
