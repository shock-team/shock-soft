import { forwardRef, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { SingleFieldCreateEditPageRoutingModule } from './single-field-create-edit-routing.module';

import { SingleFieldCreateEditPage } from './single-field-create-edit.page';

import { ReactiveFormsModule, FormsModule, Form, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ComponentsModule } from '../../modules/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    SingleFieldCreateEditPageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SingleFieldCreateEditPageModule),
      multi: true
    },
  ],
  declarations: [SingleFieldCreateEditPage]
})
export class SingleFieldCreateEditPageModule {}
