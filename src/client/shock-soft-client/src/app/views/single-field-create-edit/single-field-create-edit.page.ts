import { Component, Input, OnDestroy, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators, ReactiveFormsModule, FormsModule, FormBuilder, NgControl } from '@angular/forms'
import { Subscription } from 'rxjs';
import { SingleFieldAddAndEditData } from '../../entities/SingleFieldAddAndEditData';
import { InstanceCreator } from '../../utils/instance-creator';
import singleFieldAddAndEditTemplates from './templates.json'
import { ModalController } from '@ionic/angular';
import { Toast } from '../../utils/toast';
import { ChangeDetectorRef, AfterContentChecked } from '@angular/core';
@Component({
  selector: 'app-single-field-create-edit',
  templateUrl: './single-field-create-edit.page.html',
  styleUrls: ['./single-field-create-edit.page.scss'],
})
export class SingleFieldCreateEditPage implements OnInit, OnDestroy, AfterContentChecked {

  @Input() template: string;
  @Input() submitHandler: (entity: any, pHandler: () => void) => Subscription;
  @Input() retrievalHandler: (pHandler: (entity: any) => void) => Subscription;

  pageName: string;
  successMessage: string;
  fieldData: SingleFieldAddAndEditData;

  entity: any;
  form: FormGroup;

  subscription: Subscription;

  constructor(private modalController: ModalController, private changeDetectorRef: ChangeDetectorRef) {}

  ngAfterContentChecked() { this.changeDetectorRef.detectChanges(); }

  ngOnInit() {
    this.fieldData = singleFieldAddAndEditTemplates[this.template];
    this.entity = InstanceCreator.CreateInstance(this.fieldData.className);

    this.form = new FormGroup({
      field: new FormControl(this.entity[this.fieldData.fieldName], Validators.required)
    });
    
    if(this.retrievalHandler){
      this.pageName = this.fieldData.editHeader;
      this.successMessage = this.fieldData.editSuccessMessage;
      this.subscription = this.retrievalHandler( (data) => this.entity = data );
    }
    else {
      this.pageName = this.fieldData.newHeader;
      this.successMessage = this.fieldData.newSuccessMessage;
    }

    
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  onClickedReturn() {
    this.modalController.dismiss();
  }

  onClickedAccept() {
    this.subscription = this.submitHandler(this.entity, () => {
      Toast.createToast(this.successMessage);
      this.modalController.dismiss();
    });
  }

  isLoading(): boolean {
    return this.subscription && (!this.subscription?.closed);
  }

  notFound(): boolean {
    return (this.retrievalHandler) && !this.entity['id'];
  }
}
