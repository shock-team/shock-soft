// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    projectId: 'shock-soft',
    apiKey: 'AIzaSyCKvuUY9yxM1rgyg9bwS_saIaILOdTC6t4',
    authDomain: 'shock-soft.firebaseapp.com',
    storageBucket: 'shock-soft.appspot.com',
    messagingSenderId: '428548358536',
    appId: '1:428548358536:web:f67ce8b2d58ea673463a3d',
    measurementId: 'G-WE0YG956QC',
  },
  useEmulators: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
