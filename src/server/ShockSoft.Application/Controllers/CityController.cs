﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ShockSoft.Application;

[ApiController]
[Route("api/[controller]")]
public class CityController : ControllerBase
{
    private readonly ICityService cityService;
    private readonly IMapper mapper;

    public CityController(
        IMapper mapper,
        ICityService cityService)
    {
        this.mapper = mapper;
        this.cityService = cityService;
    }

    [HttpGet("{cityId}")]
    public async Task<IActionResult> GetCityById(
        Guid cityId)
    {
        var city = await cityService.GetCityByIdAsync(cityId);

        if (city is null) return NotFound();

        var cityData = mapper.Map<CityData>(city);

        return Ok(cityData);
    }

    [HttpGet]
    public async Task<IActionResult> GetPaginatedCities(
        [FromQuery] CityFilterData filterData)
    {
        var cities = await cityService.GetPagedCitiesAsync(filterData);

        ControllerUtils.AddPaginationHeaders(cities, Response);

        var citiesData = mapper.Map<IEnumerable<CityData>>(cities);

        return Ok(citiesData);
    }

    [HttpPost]
    public async Task<IActionResult> AddCity(
        [FromBody] CityCreationData cityData)
    {
        var result = await cityService.AddCityAsync(cityData);

        return CreatedAtAction(
            nameof(GetCityById),
            mapper.Map<CityData>(result.Entity));
    }

    [HttpPut("{cityId}")]
    public async Task<IActionResult> UpdateCity(
        Guid cityId,
        [FromBody] CityUpdateData cityData)
    {
        var result = await cityService.UpdateCityAsync(cityId, cityData);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok(mapper.Map<CityData>(result.Entity));
    }

    [HttpDelete("{cityId}")]
    public async Task<IActionResult> DeleteCity(Guid cityId)
    {
        var result = await cityService.DeleteCityAsync(cityId);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok();
    }

    [HttpPost("Seed")]
    public async Task<IActionResult> SeedCities()
    {
        var result = await cityService.SeedCities();

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok();
    }
}