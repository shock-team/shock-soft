﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ShockSoft.Application;

[ApiController]
[Authorize]
[Route("api/[controller]")]
public class ClientController : ControllerBase
{
    private readonly IClientService clientService;
    private readonly IMapper mapper;

    public ClientController(
        IMapper mapper,
        IClientService clientService)
    {
        this.mapper = mapper;
        this.clientService = clientService;
    }

    [HttpGet("{clientId}")]
    public async Task<IActionResult> GetClientById(
        Guid clientId)
    {
        var client = await clientService.GetClientByIdAsync(clientId);

        if (client is null) return NotFound();

        var clientData = mapper.Map<ClientData>(client);

        return Ok(clientData);
    }

    [HttpGet]
    public async Task<IActionResult> GetPaginatedClients(
        [FromQuery] ClientFilterData filterData)
    {
        var clients = await clientService.GetPagedClientsAsync(filterData);

        ControllerUtils.AddPaginationHeaders(clients, Response);

        var clientsData = mapper.Map<IEnumerable<ClientData>>(clients);

        return Ok(clientsData);
    }

    [HttpPost]
    public async Task<IActionResult> AddClient(
        [FromBody] ClientCreationData clientData)
    {
        var result = await clientService.AddClientAsync(clientData);

        var errorResult = this.GetErrorResult(result);
        
        return errorResult ?? CreatedAtAction(nameof(GetClientById), mapper.Map<ClientData>(result.Entity));
    }

    [HttpPut("{clientId}")]
    public async Task<IActionResult> UpdateClient(
        Guid clientId,
        [FromBody] ClientUpdateData clientData)
    {
        var result = await clientService.UpdateClientAsync(clientId, clientData);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok(mapper.Map<ClientData>(result.Entity));
    }

    [HttpDelete("{clientId}")]
    public async Task<IActionResult> DeleteClient(Guid clientId)
    {
        var result = await clientService.DeleteClientAsync(clientId);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok();
    }
}