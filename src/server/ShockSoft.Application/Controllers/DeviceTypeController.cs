﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ShockSoft.Application;

[ApiController]
[Authorize]
[Route("api/[controller]")]
public class DeviceTypeController : ControllerBase
{
    private readonly IDeviceTypeService deviceTypeService;
    private readonly IMapper mapper;

    public DeviceTypeController(
        IMapper mapper,
        IDeviceTypeService deviceTypeService)
    {
        this.mapper = mapper;
        this.deviceTypeService = deviceTypeService;
    }

    [HttpGet("{deviceTypeId}")]
    public async Task<IActionResult> GetDeviceTypeById(
        Guid deviceTypeId)
    {
        var deviceType = await deviceTypeService.GetDeviceTypeByIdAsync(deviceTypeId);

        if (deviceType is null) return NotFound();

        var deviceTypeData = mapper.Map<DeviceTypeData>(deviceType);

        return Ok(deviceTypeData);
    }

    [HttpGet]
    public async Task<IActionResult> GetPaginatedDeviceTypes(
        [FromQuery] DeviceTypeFilterData filterData)
    {
        var deviceTypes = await deviceTypeService.GetPagedDeviceTypesAsync(filterData);

        ControllerUtils.AddPaginationHeaders(deviceTypes, Response);

        var deviceTypesData = mapper.Map<IEnumerable<DeviceTypeData>>(deviceTypes);

        return Ok(deviceTypesData);
    }

    [HttpPost]
    public async Task<IActionResult> AddDeviceType(
        [FromBody] DeviceTypeCreationData deviceTypeData)
    {
        var result = await deviceTypeService.AddDeviceTypeAsync(deviceTypeData);

        return CreatedAtAction(
            nameof(GetDeviceTypeById),
            mapper.Map<DeviceTypeData>(result.Entity));
    }

    [HttpPut("{deviceTypeId}")]
    public async Task<IActionResult> UpdateDeviceType(
        Guid deviceTypeId,
        [FromBody] DeviceTypeUpdateData deviceTypeData)
    {
        var result = await deviceTypeService.UpdateDeviceTypeAsync(deviceTypeId, deviceTypeData);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok(mapper.Map<DeviceTypeData>(result.Entity));
    }

    [HttpDelete("{deviceTypeId}")]
    public async Task<IActionResult> DeleteDeviceType(Guid deviceTypeId)
    {
        var result = await deviceTypeService.DeleteDeviceTypeAsync(deviceTypeId);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok();
    }
}