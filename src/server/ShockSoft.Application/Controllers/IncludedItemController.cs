﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ShockSoft.Application;

[ApiController]
[Authorize]
[Route("api/[controller]")]
public class IncludedItemController : ControllerBase
{
    private readonly IIncludedItemService includedItemService;
    private readonly IMapper mapper;

    public IncludedItemController(
        IMapper mapper,
        IIncludedItemService includedItemService)
    {
        this.mapper = mapper;
        this.includedItemService = includedItemService;
    }

    [HttpGet("{includedItemId}")]
    public async Task<IActionResult> GetIncludedItemById(
        Guid includedItemId)
    {
        var includedItem = await includedItemService.GetIncludedItemByIdAsync(includedItemId);

        if (includedItem is null) return NotFound();

        var includedItemData = mapper.Map<IncludedItemData>(includedItem);

        return Ok(includedItemData);
    }

    [HttpGet]
    public async Task<IActionResult> GetPaginatedIncludedItems(
        [FromQuery] IncludedItemFilterData filterData)
    {
        var includedItems = await includedItemService.GetPagedIncludedItemsAsync(filterData);

        ControllerUtils.AddPaginationHeaders(includedItems, Response);

        var includedItemsData = mapper.Map<IEnumerable<IncludedItemData>>(includedItems);

        return Ok(includedItemsData);
    }

    [HttpPost]
    public async Task<IActionResult> AddIncludedItem(
        [FromBody] IncludedItemCreationData includedItemData)
    {
        var result = await includedItemService.AddIncludedItemAsync(includedItemData);

        return CreatedAtAction(
            nameof(GetIncludedItemById),
            mapper.Map<IncludedItemData>(result.Entity));
    }

    [HttpPut("{includedItemId}")]
    public async Task<IActionResult> UpdateIncludedItem(
        Guid includedItemId,
        [FromBody] IncludedItemUpdateData includedItemData)
    {
        var result = await includedItemService.UpdateIncludedItemAsync(includedItemId, includedItemData);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok(mapper.Map<IncludedItemData>(result.Entity));
    }

    [HttpDelete("{includedItemId}")]
    public async Task<IActionResult> DeleteIncludedItem(Guid includedItemId)
    {
        var result = await includedItemService.DeleteIncludedItemAsync(includedItemId);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok();
    }
}