﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace ShockSoft.Application;

[ApiController]
[Authorize]
[Route("api/[controller]")]
public class MediaFileController : ControllerBase
{
    private readonly IMapper mapper;
    private readonly IMediaFileService mediaFileService;

    public MediaFileController(
        IMapper mapper,
        IMediaFileService mediaFileService)
    {
        this.mapper = mapper;
        this.mediaFileService = mediaFileService;
    }

    [HttpGet("{mediaFileId}")]
    public async Task<IActionResult> GetMediaFileById(
        Guid mediaFileId)
    {
        var mediaFile = await mediaFileService.GetMediaFileByIdAsync(mediaFileId);

        if (mediaFile is null) return NotFound();

        var mediaFileData = mapper.Map<MediaFileData>(mediaFile);

        return Ok(mediaFileData);
    }
    
    [HttpGet("{mediaFileId:guid}/download")]
    public async Task<IActionResult> DownloadFileById(
        Guid mediaFileId, [FromQuery] bool? inline)
    {
        var result = await mediaFileService.GetFileMetadata(mediaFileId);

        var errorResult = this.GetErrorResult(result);

        if (errorResult is not null)
        {
            return errorResult;
        }

        var fileMetadata = result.Entity;
        
        var fileStream = new FileStream(
            fileMetadata!.Path, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 4096, useAsync: true);
        
        var contentDisposition = inline ?? false
            ? new ContentDispositionHeaderValue("inline")
            : new ContentDispositionHeaderValue("attachment");

        contentDisposition.SetHttpFileName(fileMetadata.Name);
        this.Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();

        MimeTypes.TryGetMimeType(fileMetadata.Name, out var mimeType);

        return File(fileStream, mimeType ?? MimeTypes.FallbackMimeType);
    }

    [HttpGet]
    public async Task<IActionResult> GetMediaFilesByRepairId(
        [FromQuery] Guid repairId)
    {
        var mediaFiles = await mediaFileService.GetMediaFilesByRepairIdAsync(repairId);

        if (mediaFiles is null) return NotFound();

        var mediaFileData = mapper.Map<IEnumerable<MediaFileData>>(mediaFiles);

        return Ok(mediaFileData);
    }

    [HttpPost]
    public async Task<IActionResult> AddMediaFile(
        [FromBody] MediaFileCreationData mediaFileData)
    {
        var result = await mediaFileService.AddMediaFileAsync(mediaFileData);

        var errorResult = this.GetErrorResult(result);
        
        return errorResult ?? CreatedAtAction(
            nameof(GetMediaFileById),
            mapper.Map<MediaFileData>(result.Entity));
    }

    [HttpDelete("{mediaFileId}")]
    public async Task<IActionResult> DeleteMediaFile(Guid mediaFileId)
    {
        var result = await mediaFileService.DeleteMediaFileAsync(mediaFileId);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok();
    }
}