﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ShockSoft.Application.Controllers;

[ApiController]
[Authorize]
[Route("api/[controller]")]
public class ProductController : ControllerBase
{
    private readonly IMapper mapper;
    private readonly IProductService productService;

    public ProductController(
        IMapper mapper,
        IProductService productService)
    {
        this.mapper = mapper;
        this.productService = productService;
    }

    [HttpGet("{productId}")]
    public async Task<IActionResult> GetProductById(
        Guid productId)
    {
        var product = await productService.GetProductByIdAsync(productId);

        if (product is null) return NotFound();

        var productData = mapper.Map<ProductData>(product);

        return Ok(productData);
    }

    [HttpGet]
    public async Task<IActionResult> GetPaginatedProducts(
        [FromQuery] ProductFilterData filterData)
    {
        var products = await productService.GetPagedProductsAsync(filterData);

        ControllerUtils.AddPaginationHeaders(products, Response);

        var productsData = mapper.Map<IEnumerable<ProductData>>(products);

        return Ok(productsData);
    }

    [HttpPost]
    public async Task<IActionResult> AddProduct(
        [FromBody] ProductCreationData productData)
    {
        var result = await productService.AddProductAsync(productData);

        return CreatedAtAction(
            nameof(GetProductById),
            mapper.Map<ProductData>(result.Entity));
    }

    [HttpPut("{productId}")]
    public async Task<IActionResult> UpdateProduct(
        Guid productId,
        [FromBody] ProductUpdateData productData)
    {
        var result = await productService.UpdateProductAsync(productId, productData);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok(mapper.Map<ProductData>(result.Entity));
    }

    [HttpDelete("{productId}")]
    public async Task<IActionResult> DeleteProduct(Guid productId)
    {
        var result = await productService.DeleteProductAsync(productId);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok();
    }
}