﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ShockSoft.Application;

[ApiController]
[Authorize]
[Route("api/[controller]")]
public class RepairController : ControllerBase
{
    private readonly IMapper mapper;
    private readonly IRepairService repairService;

    public RepairController(
        IMapper mapper,
        IRepairService repairService)
    {
        this.mapper = mapper;
        this.repairService = repairService;
    }

    [HttpGet("{repairId}")]
    public async Task<IActionResult> GetRepairById(
        Guid repairId)
    {
        var repair = await repairService.GetRepairByIdAsync(repairId);

        if (repair is null) return NotFound();

        var repairData = mapper.Map<RepairData>(repair);

        return Ok(repairData);
    }

    [HttpGet]
    public async Task<IActionResult> GetPaginatedRepairs(
        [FromQuery] RepairFilterData filterData)
    {
        var repairs = await repairService.GetPagedRepairsAsync(filterData);

        ControllerUtils.AddPaginationHeaders(repairs, Response);

        var repairsData = mapper.Map<IEnumerable<RepairData>>(repairs);

        return Ok(repairsData);
    }

    [HttpPost]
    public async Task<IActionResult> AddRepair(
        [FromBody] RepairCreationData repairData)
    {
        var result = await repairService.AddRepairAsync(repairData);

        var errorResult = this.GetErrorResult(result);

        return errorResult ?? CreatedAtAction(
            nameof(GetRepairById),
            mapper.Map<RepairData>(result.Entity));
    }

    [HttpPut("{repairId}")]
    public async Task<IActionResult> UpdateRepair(
        Guid repairId,
        [FromBody] RepairUpdateData repairData)
    {
        var result = await repairService.UpdateRepairAsync(repairId, repairData);
        
        var errorResult = this.GetErrorResult(result);

        return errorResult ?? Ok(mapper.Map<RepairData>(result.Entity));
    }
}