﻿using System.Text.Json.Serialization;

namespace ShockSoft.Application;

public class CityApiDto
{
    [JsonPropertyName("localidad_censal_nombre")]
    public string Name { get; set; }
}