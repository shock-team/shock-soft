﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ShockSoft.Application;

public class ResponseApiDto
{
    [JsonPropertyName("localidades")] public IList<CityApiDto> Cities { get; set; }

    [JsonPropertyName("total")] public int TotalCount { get; set; }
}