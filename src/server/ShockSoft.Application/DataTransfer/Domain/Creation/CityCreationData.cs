﻿using System.ComponentModel.DataAnnotations;

namespace ShockSoft.Application;

public class CityCreationData
{
    [Required]
    public string Name { get; set; } = null!;
}