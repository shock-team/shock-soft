﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ShockSoft.Application;

public class ClientCreationData
{
    public string? Name { get; set; } 
    public string? Surname { get; set; }
    public string? PhoneNumber { get; set; }
    public string? Address { get; set; }
    
    [Required] 
    public Guid CityId { get; set; }
    
    public string? Email { get; set; }
}