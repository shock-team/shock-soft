﻿using System.ComponentModel.DataAnnotations;

namespace ShockSoft.Application;

public class DeviceTypeCreationData
{
    [Required]
    public string Name { get; set; } = null!;
}