﻿using System.ComponentModel.DataAnnotations;

namespace ShockSoft.Application;

public class IncludedItemCreationData
{
    [Required]
    public string Name { get; set; } = null!;
}