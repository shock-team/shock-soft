﻿using System;

namespace ShockSoft.Application;

public class MediaFileCreationData
{
    public string Name { get; set; } = null!;
    public string Base64Data { get; set; } = null!;
    public Guid RepairId { get; set; }
}