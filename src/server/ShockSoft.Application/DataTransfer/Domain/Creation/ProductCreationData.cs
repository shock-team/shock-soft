﻿using System.ComponentModel.DataAnnotations;

namespace ShockSoft.Application;

public class ProductCreationData
{
    [Required]
    public string Name { get; set; } = null!;
}