﻿using System;
using System.Collections.Generic;

namespace ShockSoft.Application;

public class RepairCreationData
{
    public string? Problem { get; set; }
    public string? Solution { get; set; } 
    public decimal? LaborPrice { get; set; }
    public string? DevicePassword { get; set; } 
    public DateTime? CreationDate { get; set; }
    public Guid ClientId { get; set; }
    public Guid DeviceTypeId { get; set; }
    public IList<IncludedItemData>? IncludedItems { get; set; } = new List<IncludedItemData>();
}