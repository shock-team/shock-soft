﻿using System;

namespace ShockSoft.Application;

public class CityData
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
}