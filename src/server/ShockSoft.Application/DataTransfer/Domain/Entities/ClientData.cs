﻿using System;

namespace ShockSoft.Application;

public class ClientData
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public string Surname { get; set; } = null!;
    public string PhoneNumber { get; set; } = null!;
    public string Address { get; set; } = null!;
    public Guid CityId { get; set; }
    public string Email { get; set; } = null!;
}