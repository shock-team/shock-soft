﻿using System;

namespace ShockSoft.Application;

public class DeviceTypeData
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
}