﻿using System;

namespace ShockSoft.Application;

public class IncludedItemData
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
}