﻿using System;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class MediaFileData
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public string Base64Data { get; set; } = null!;
    public string Extension { get; set; } = null!;
    public FileType Type { get; set; }
    public Guid RepairId { get; set; }
}