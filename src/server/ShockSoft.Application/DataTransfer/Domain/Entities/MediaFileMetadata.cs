﻿namespace ShockSoft.Application;

public record MediaFileMetadata(string Name, string Path);