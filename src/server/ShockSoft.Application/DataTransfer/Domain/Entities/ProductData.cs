﻿using System;

namespace ShockSoft.Application;

public class ProductData
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
}