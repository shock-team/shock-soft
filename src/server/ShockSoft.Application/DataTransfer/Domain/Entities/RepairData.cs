﻿using System;
using System.Collections.Generic;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class RepairData
{
    public Guid Id { get; set; }
    public string Problem { get; set; } = null!;
    public string Solution { get; set; } = null!;
    public decimal LaborPrice { get; set; }
    public string DevicePassword { get; set; } = null!;
    public DateTime? CreationDate { get; set; }
    public Guid ClientId { get; set; }
    public Guid DeviceTypeId { get; set; }
    public RepairState State { get; set; }
    public IList<StateChangeData> StateChanges { get; set; } = new List<StateChangeData>();
    public IList<IncludedItemData> IncludedItems { get; set; } = new List<IncludedItemData>();
    public IList<RepairLineData> RepairLines { get; set; } = new List<RepairLineData>();
}