﻿using System;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class StateChangeData
{
    public Guid Id { get; set; }
    public RepairState State { get; set; }
    public DateTime Date { get; set; }
    public Guid RepairId { get; set; }
}