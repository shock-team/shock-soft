﻿namespace ShockSoft.Application;

public class CityFilterData : BaseFilterData
{
    public string? Name { get; set; }
}