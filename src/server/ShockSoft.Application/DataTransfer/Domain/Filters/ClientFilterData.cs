﻿namespace ShockSoft.Application;

public class ClientFilterData : BaseFilterData
{
    public string? Name { get; set; }
    public string? Surname { get; set; }
}