﻿namespace ShockSoft.Application;

public class DeviceTypeFilterData : BaseFilterData
{
    public string? Name { get; set; }
}