﻿namespace ShockSoft.Application;

public class IncludedItemFilterData : BaseFilterData
{
    public string? Name { get; set; }
}