﻿namespace ShockSoft.Application;

public class ProductFilterData : BaseFilterData
{
    public string? Name { get; set; }
}