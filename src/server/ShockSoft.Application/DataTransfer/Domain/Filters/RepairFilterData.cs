﻿using ShockSoft.Domain;

namespace ShockSoft.Application;

public class RepairFilterData : BaseFilterData
{
    public RepairState? State { get; set; }
    public string? ClientName { get; set; }
    public string? ClientSurname { get; set; }
}