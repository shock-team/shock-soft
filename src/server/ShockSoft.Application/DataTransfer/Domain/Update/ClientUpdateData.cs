﻿using System;

namespace ShockSoft.Application;

public class ClientUpdateData
{
    public string Name { get; set; } = string.Empty;
    public string Surname { get; set; } =  string.Empty;
    public string PhoneNumber { get; set; } =  string.Empty;
    public string Address { get; set; } = string.Empty;
    public Guid CityId { get; set; }
    public string Email { get; set; } = string.Empty;
}