﻿using System;
using System.Collections.Generic;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class RepairUpdateData
{
    public string Problem { get; set; } = string.Empty;
    public string Solution { get; set; } = string.Empty;
    public decimal LaborPrice { get; set; }
    public string DevicePassword { get; set; } = string.Empty;
    public DateTime CreationDate { get; set; }
    public Guid ClientId { get; set; }
    public Guid DeviceTypeId { get; set; }
    public RepairState State { get; set; }
    public IList<IncludedItemData> IncludedItems { get; set; } = new List<IncludedItemData>();
    public IList<RepairLineData> RepairLines { get; set; } = new List<RepairLineData>();
}