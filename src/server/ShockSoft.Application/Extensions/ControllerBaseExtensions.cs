﻿using Microsoft.AspNetCore.Mvc;

namespace ShockSoft.Application;

public static class ControllerBaseExtensions
{
    public static IActionResult? GetErrorResult<TEntity>(
        this ControllerBase controller,
        ServiceResult<TEntity> serviceResult)
        where TEntity : class
    {
        if (!serviceResult.Errored) return null;
        
        if (serviceResult.Error is not null)
        {
            controller.ModelState.AddModelError(serviceResult.Error.Title, serviceResult.Error.Description);
        }

        return serviceResult.Error?.Type switch
        {
            ErrorType.NotFound => controller.NotFound(controller.ModelState),
            ErrorType.BadRequest => controller.BadRequest(controller.ModelState),
            _ => controller.BadRequest(controller.ModelState)
        };
    }
}