﻿using System;
using Microsoft.Extensions.Logging;

namespace ShockSoft.Application;

public static class LoggerExtensions
{
    public static void LogNotFoundError<T>(this ILogger logger, Guid? id)
    {
        logger.LogError($"The {typeof(T).Name} with Id {id} doesn't exist.");
    }
}