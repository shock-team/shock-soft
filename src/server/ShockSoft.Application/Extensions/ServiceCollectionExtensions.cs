﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public static class ServiceCollectionExtensions
{
    public static void RegisterRepositories(this IServiceCollection services)
    {
        services.AddTransient<IUnitOfWork, UnitOfWork>();
        services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
    }

    public static void RegisterServices(this IServiceCollection services)
    {
        services.AddTransient<IClientService, ClientService>();
        services.AddTransient<IMediaFileService, MediaFileService>();
        services.AddTransient<IRepairService, RepairService>();
        services.AddTransient<IDeviceTypeService, DeviceTypeService>();
        services.AddTransient<IProductService, ProductService>();
        services.AddTransient<IIncludedItemService, IncludedItemService>();
        services.AddTransient<ICityService, CityService>();
    }

    public static void RegisterOptions(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<MediaFileOptions>(configuration.GetSection(MediaFileOptions.MediaFile));
        services.Configure<TelegramIntegrationOptions>(configuration.GetSection(TelegramIntegrationOptions.TelegramIntegration));
    }

    public static void AddFirebaseAuthentication(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(opt =>
            {
                opt.Authority = configuration["Jwt:Firebase:ValidIssuer"];
                opt.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = configuration["Jwt:Firebase:ValidIssuer"],
                    ValidAudience = configuration["Jwt:Firebase:ValidAudience"]
                };
            });
    }
}