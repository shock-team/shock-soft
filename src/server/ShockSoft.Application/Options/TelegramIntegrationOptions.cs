﻿namespace ShockSoft.Application;

public class TelegramIntegrationOptions
{
    public const string TelegramIntegration = "TelegramIntegrationOptions";
    public bool Enabled { get; set; }
}