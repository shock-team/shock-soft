﻿using AutoMapper;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class CityProfile : Profile
{
    public CityProfile()
    {
        CreateMap<City, CityData>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name));
    }
}