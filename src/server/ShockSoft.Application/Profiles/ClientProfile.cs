﻿using AutoMapper;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class ClientProfile : Profile
{
    public ClientProfile()
    {
        CreateMap<Client, ClientData>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.Surname, o => o.MapFrom(src => src.Surname))
            .ForMember(dest => dest.PhoneNumber, o => o.MapFrom(src => src.PhoneNumber))
            .ForMember(dest => dest.Address, o => o.MapFrom(src => src.Address))
            .ForMember(dest => dest.CityId, o => o.MapFrom(src => src.City.Id))
            .ForMember(dest => dest.Email, o => o.MapFrom(src => src.Email));
    }
}