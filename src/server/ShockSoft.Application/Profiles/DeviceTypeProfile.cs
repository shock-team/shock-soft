﻿using AutoMapper;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class DeviceTypeProfile : Profile
{
    public DeviceTypeProfile()
    {
        CreateMap<DeviceType, DeviceTypeData>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name));
    }
}