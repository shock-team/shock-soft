﻿using AutoMapper;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class IncludedItemProfile : Profile
{
    public IncludedItemProfile()
    {
        CreateMap<IncludedItem, IncludedItemData>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name));
    }
}