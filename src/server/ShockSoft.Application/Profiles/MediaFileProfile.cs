﻿using AutoMapper;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class MediaFileProfile : Profile
{
    public MediaFileProfile()
    {
        CreateMap<MediaFile, MediaFileData>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.Extension, o => o.MapFrom(src => src.Extension))
            .ForMember(dest => dest.Base64Data, o => o.MapFrom(src => src.Base64Data))
            .ForMember(dest => dest.Type, o => o.MapFrom(src => src.Type))
            .ForMember(dest => dest.RepairId, o => o.MapFrom(src => src.Repair.Id));
    }
}