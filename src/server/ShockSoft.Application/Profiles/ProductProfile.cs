﻿using AutoMapper;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class ProductProfile : Profile
{
    public ProductProfile()
    {
        CreateMap<Product, ProductData>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name));
    }
}