﻿using AutoMapper;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class RepairLineProfile : Profile
{
    public RepairLineProfile()
    {
        CreateMap<RepairLine, RepairLineData>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Quantity, o => o.MapFrom(src => src.Quantity))
            .ForMember(dest => dest.Price, o => o.MapFrom(src => src.Price))
            .ForMember(dest => dest.ProductId, o => o.MapFrom(src => src.Product.Id));
    }
}