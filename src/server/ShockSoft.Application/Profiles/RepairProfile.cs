﻿using System.Linq;
using AutoMapper;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class RepairProfile : Profile
{
    public RepairProfile()
    {
        CreateMap<Repair, RepairData>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Problem, o => o.MapFrom(src => src.Problem))
            .ForMember(dest => dest.Solution, o => o.MapFrom(src => src.Solution))
            .ForMember(dest => dest.LaborPrice, o => o.MapFrom(src => src.LaborPrice))
            .ForMember(dest => dest.DevicePassword, o => o.MapFrom(src => src.DevicePassword))
            .ForMember(dest => dest.DeviceTypeId, o => o.MapFrom(src => src.DeviceType.Id))
            .ForMember(dest => dest.ClientId, o => o.MapFrom(src => src.Client.Id))
            .ForMember(dest => dest.State, o => o.MapFrom(src => src.State))
            .ForMember(dest => dest.StateChanges, o => o.MapFrom(src => src.StateChanges))
            .ForMember(dest => dest.CreationDate, o => o.MapFrom(
                src => src.StateChanges.First(sc => sc.State == RepairState.Queued).Date))
            .ForMember(dest => dest.IncludedItems, o => o.MapFrom(src => src.IncludedItems))
            .ForMember(dest => dest.RepairLines, o => o.MapFrom(src => src.RepairLines));
    }
}