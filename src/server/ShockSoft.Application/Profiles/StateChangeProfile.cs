﻿using AutoMapper;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public class StateChangeProfile : Profile
{
    public StateChangeProfile()
    {
        CreateMap<StateChange, StateChangeData>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.State, o => o.MapFrom(src => src.State))
            .ForMember(dest => dest.Date, o => o.MapFrom(src => src.Date))
            .ForMember(dest => dest.RepairId, o => o.MapFrom(src => src.Repair.Id));
    }
}