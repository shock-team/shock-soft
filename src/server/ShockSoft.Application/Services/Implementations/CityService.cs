﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public class CityService : ICityService
{
    private readonly IConfiguration configuration;
    private readonly ILogger<CityService> logger;
    private readonly IUnitOfWork unitOfWork;
    private readonly ServiceResult<City> result = new();

    public CityService(
        ILogger<CityService> logger,
        IUnitOfWork unitOfWork,
        IConfiguration configuration)
    {
        this.logger = logger;
        this.unitOfWork = unitOfWork;
        this.configuration = configuration;
    }

    public async Task<City> GetCityByIdAsync(Guid cityId)
    {
        return await unitOfWork.Cities.GetBy(x => x.Id == cityId);
    }

    public async Task<PagedList<City>> GetPagedCitiesAsync(
        CityFilterData cityFilterData)
    {
        Expression<Func<City, bool>> expression = x => x != null;

        if (!string.IsNullOrWhiteSpace(cityFilterData.Name))
        {
            Expression<Func<City, bool>> nameFilter = x => x.Name.ToUpper().Contains(cityFilterData.Name.ToUpper());
            expression = ExpressionUtils.ConcatLambdaExpression(expression, nameFilter);
        }

        return await unitOfWork.Cities.GetPaginated(
            x => x.Name,
            cityFilterData.PageNumber,
            cityFilterData.PageSize,
            expression);
    }

    public async Task<ServiceResult<City>> AddCityAsync(CityCreationData cityData)
    {
        var cityId = Guid.NewGuid();

        var city = new City
        {
            Id = cityId,
            Name = cityData.Name
        };

        unitOfWork.Cities.Add(city);
        await unitOfWork.Complete();

        result.SetEntity(city);

        return result;
    }

    public async Task<ServiceResult<City>> UpdateCityAsync(Guid cityId, CityUpdateData cityData)
    {
        var city = await unitOfWork.Cities.GetBy(x => x.Id == cityId);

        if (city is null)
        {
            logger.LogNotFoundError<City>(cityId);

            result.AddError(new ServiceError(
                nameof(City),
                $"The City {cityId} does not exist.",
                ErrorType.NotFound));

            return result;
        }

        city.Name = cityData.Name;

        await unitOfWork.Complete();
        
        result.SetEntity(city);
        
        return result;
    }

    public async Task<ServiceResult<City>> DeleteCityAsync(Guid cityId)
    {
        var city = await unitOfWork.Cities.GetBy(x => x.Id == cityId);

        if (city is null)
        {
            logger.LogNotFoundError<City>(cityId);

            result.AddError(new ServiceError(
                nameof(City),
                $"The City {cityId} does not exist.",
                ErrorType.NotFound));

            return result;
        }

        unitOfWork.Cities.Remove(city);
        await unitOfWork.Complete();

        return result;
    }

    public async Task<ServiceResult<City>> SeedCities()
    {
        logger.LogInformation("Seeding cities process starting...");

        var httpClient = new HttpClient();

        var totalUrl =
            await httpClient.GetStringAsync($"{configuration.GetValue<string>("CitiesApi")}?provincia=entre-rios");
        var totalResponse = JsonSerializer.Deserialize<ResponseApiDto>(totalUrl);
        
        if (totalResponse is null)
        {
            const string error = "The response from Cities API was empty.";
            logger.LogError(error);

            result.AddError(new ServiceError(
                nameof(City),
                error,
                ErrorType.BadRequest));

            return result;
        }

        var citiesUrl = await httpClient.GetStringAsync(
            $"{configuration.GetValue<string>("CitiesApi")}?provincia=entre-rios&aplanar=1&campos=localidad_censal.nombre&max={totalResponse.TotalCount}");
        var citiesResponse = JsonSerializer.Deserialize<ResponseApiDto>(citiesUrl);

        if (citiesResponse is null)
        {
            const string error = "The response from Cities API was empty.";
            logger.LogError(error);

            result.AddError(new ServiceError(
                nameof(City),
                error,
                ErrorType.BadRequest));

            return result;
        }

        var currentCities = await unitOfWork.Cities.GetAll();

        var cities = citiesResponse.Cities.GroupBy(x => x.Name).Select(x => x.First());

        var newCities = cities.Where(newCity => currentCities.All(city => newCity.Name != city.Name));

        var citiesToAdd = newCities.Select(city => new City { Id = Guid.NewGuid(), Name = city.Name }).ToList();

        unitOfWork.Cities.AddRange(citiesToAdd);

        var changes = await unitOfWork.Complete();

        logger.LogInformation(changes > 0 ? "The Cities were seeded successfully." : "No Cities were seeded.");

        return result;
    }
}