﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public class ClientService : IClientService
{
    private readonly ILogger<ClientService> logger;
    private readonly IUnitOfWork unitOfWork;
    private readonly ServiceResult<Client> result = new();

    public ClientService(
        ILogger<ClientService> logger,
        IUnitOfWork unitOfWork)
    {
        this.logger = logger;
        this.unitOfWork = unitOfWork;
    }

    public async Task<Client> GetClientByIdAsync(Guid clientId)
    {
        return await unitOfWork.Clients.GetBy(
            x => x.Id == clientId,
            x => x.City);
    }

    public async Task<PagedList<Client>> GetPagedClientsAsync(
        ClientFilterData clientFilterData)
    {
        Expression<Func<Client, bool>> expression = x => x != null;

        if (!string.IsNullOrWhiteSpace(clientFilterData.Name))
        {
            Expression<Func<Client, bool>> nameFilter = x => x.Name.ToUpper().Contains(clientFilterData.Name.ToUpper());
            expression = ExpressionUtils.ConcatLambdaExpression(expression, nameFilter);
        }

        if (!string.IsNullOrWhiteSpace(clientFilterData.Surname))
        {
            Expression<Func<Client, bool>> surnameFilter =
                x => x.Surname.ToUpper().Contains(clientFilterData.Surname.ToUpper());
            expression = ExpressionUtils.ConcatLambdaExpression(expression, surnameFilter);
        }

        return await unitOfWork.Clients.GetPaginated(
            x => x.Surname,
            clientFilterData.PageNumber,
            clientFilterData.PageSize,
            expression,
            x => x.City);
    }

    public async Task<ServiceResult<Client>> AddClientAsync(ClientCreationData clientData)
    {
        var city = await unitOfWork.Cities.GetBy(x => x.Id == clientData.CityId);

        if (city is null)
        {
            logger.LogNotFoundError<City>(clientData.CityId);
            
            result.AddError(new ServiceError(
                nameof(City),
                $"The City {clientData.CityId} does not exist.",
                ErrorType.BadRequest));
            
            return result;
        }
        
        var clientId = Guid.NewGuid();

        var client = new Client
        {
            Id = clientId,
            Name = clientData.Name,
            Surname = clientData.Surname,
            Address = clientData.Address,
            Email = clientData.Email,
            PhoneNumber = clientData.PhoneNumber,
            City = city
        };

        unitOfWork.Clients.Add(client);
        await unitOfWork.Complete();

        result.SetEntity(client);

        return result;
    }

    public async Task<ServiceResult<Client>> UpdateClientAsync(Guid clientId, ClientUpdateData clientData)
    {
        var client = await unitOfWork.Clients.GetBy(x => x.Id == clientId);

        if (client is null)
        {
            logger.LogNotFoundError<Client>(clientId);

            result.AddError(new ServiceError(
                nameof(Client),
                $"The Client {clientId} does not exist.",
                ErrorType.NotFound));

            return result;
        }
        
        var city = await unitOfWork.Cities.GetBy(x => x.Id == clientData.CityId);

        if (city is null)
        {
            logger.LogNotFoundError<City>(clientData.CityId);

            result.AddError(new ServiceError(nameof(City),
                $"The City {clientData.CityId} does not exist.",
                ErrorType.BadRequest));

            return result;
        }

        client.Name = clientData.Name;
        client.Surname = clientData.Surname;
        client.Address = clientData.Address;
        client.PhoneNumber = clientData.PhoneNumber;
        client.Email = clientData.Email;
        client.City = city;

        await unitOfWork.Complete();

        result.SetEntity(client);
        return result;
    }

    public async Task<ServiceResult<Client>> DeleteClientAsync(Guid clientId)
    {
        var client = await unitOfWork.Clients.GetBy(x => x.Id == clientId);

        if (client is null)
        {
            logger.LogNotFoundError<Client>(clientId);

            result.AddError(new ServiceError(
                nameof(Client),
                $"The Client {clientId} does not exist.",
                ErrorType.NotFound));

            return result;
        }

        unitOfWork.Clients.Remove(client);
        await unitOfWork.Complete();

        result.SetEntity(client);
        return result;
    }
}