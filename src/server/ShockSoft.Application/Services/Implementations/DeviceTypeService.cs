﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public class DeviceTypeService : IDeviceTypeService
{
    private readonly ILogger<DeviceTypeService> logger;
    private readonly IUnitOfWork unitOfWork;
    private readonly ServiceResult<DeviceType> result = new();

    public DeviceTypeService(
        ILogger<DeviceTypeService> logger,
        IUnitOfWork unitOfWork)
    {
        this.logger = logger;
        this.unitOfWork = unitOfWork;
    }

    public async Task<DeviceType> GetDeviceTypeByIdAsync(Guid deviceTypeId)
    {
        return await unitOfWork.DeviceTypes.GetBy(x => x.Id == deviceTypeId);
    }

    public async Task<PagedList<DeviceType>> GetPagedDeviceTypesAsync(
        DeviceTypeFilterData deviceTypeFilterData)
    {
        Expression<Func<DeviceType, bool>> expression = x => x != null;

        if (!string.IsNullOrWhiteSpace(deviceTypeFilterData.Name))
        {
            Expression<Func<DeviceType, bool>> nameFilter = x =>
                x.Name.ToUpper().Contains(deviceTypeFilterData.Name.ToUpper());
            expression = ExpressionUtils.ConcatLambdaExpression(expression, nameFilter);
        }

        return await unitOfWork.DeviceTypes.GetPaginated(
            x => x.Name,
            deviceTypeFilterData.PageNumber,
            deviceTypeFilterData.PageSize,
            expression);
    }

    public async Task<ServiceResult<DeviceType>> AddDeviceTypeAsync(DeviceTypeCreationData deviceTypeData)
    {
        var deviceTypeId = Guid.NewGuid();

        var deviceType = new DeviceType
        {
            Id = deviceTypeId,
            Name = deviceTypeData.Name
        };

        unitOfWork.DeviceTypes.Add(deviceType);
        await unitOfWork.Complete();
        
        result.SetEntity(deviceType);

        return result;
    }

    public async Task<ServiceResult<DeviceType>> UpdateDeviceTypeAsync(Guid deviceTypeId, DeviceTypeUpdateData deviceTypeData)
    {
        var deviceType = await unitOfWork.DeviceTypes.GetBy(x => x.Id == deviceTypeId);

        if (deviceType is null)
        {
            logger.LogNotFoundError<DeviceType>(deviceTypeId);

            result.AddError(new ServiceError(
                nameof(DeviceType),
                $"The DeviceType {deviceTypeId} does not exist.",
                ErrorType.NotFound));

            return result;
        };

        deviceType.Name = deviceTypeData.Name;

        await unitOfWork.Complete();
        
        result.SetEntity(deviceType);

        return result;
    }

    public async Task<ServiceResult<DeviceType>> DeleteDeviceTypeAsync(Guid deviceTypeId)
    {
        var deviceType = await unitOfWork.DeviceTypes.GetBy(x => x.Id == deviceTypeId);

        if (deviceType is null)
        {
            logger.LogNotFoundError<DeviceType>(deviceTypeId);

            result.AddError(new ServiceError(
                nameof(DeviceType),
                $"The DeviceType {deviceTypeId} does not exist.",
                ErrorType.NotFound));

            return result;
        };

        unitOfWork.DeviceTypes.Remove(deviceType);
        await unitOfWork.Complete();

        return result;
    }
}