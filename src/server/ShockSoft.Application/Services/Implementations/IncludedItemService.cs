﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public class IncludedItemService : IIncludedItemService
{
    private readonly ILogger<IncludedItemService> logger;
    private readonly IUnitOfWork unitOfWork;
    private readonly ServiceResult<IncludedItem> result = new();
    
    public IncludedItemService(
        ILogger<IncludedItemService> logger,
        IUnitOfWork unitOfWork)
    {
        this.logger = logger;
        this.unitOfWork = unitOfWork;
    }

    public async Task<IncludedItem> GetIncludedItemByIdAsync(Guid includedItemId)
    {
        return await unitOfWork.IncludedItems.GetBy(x => x.Id == includedItemId);
    }

    public async Task<PagedList<IncludedItem>> GetPagedIncludedItemsAsync(
        IncludedItemFilterData includedItemFilterData)
    {
        Expression<Func<IncludedItem, bool>> expression = x => x != null;

        if (!string.IsNullOrWhiteSpace(includedItemFilterData.Name))
        {
            Expression<Func<IncludedItem, bool>> nameFilter = x =>
                x.Name.ToUpper().Contains(includedItemFilterData.Name.ToUpper());
            expression = ExpressionUtils.ConcatLambdaExpression(expression, nameFilter);
        }

        return await unitOfWork.IncludedItems.GetPaginated(
            x => x.Name,
            includedItemFilterData.PageNumber,
            includedItemFilterData.PageSize,
            expression);
    }

    public async Task<ServiceResult<IncludedItem>> AddIncludedItemAsync(IncludedItemCreationData includedItemData)
    {
        var includedItemId = Guid.NewGuid();

        var includedItem = new IncludedItem
        {
            Id = includedItemId,
            Name = includedItemData.Name
        };

        unitOfWork.IncludedItems.Add(includedItem);
        await unitOfWork.Complete();
        
        result.SetEntity(includedItem);

        return result;
    }

    public async Task<ServiceResult<IncludedItem>> UpdateIncludedItemAsync(Guid includedItemId, IncludedItemUpdateData includedItemData)
    {
        var includedItem = await unitOfWork.IncludedItems.GetBy(x => x.Id == includedItemId);

        if (includedItem is null)
        {
            logger.LogNotFoundError<IncludedItem>(includedItemId);

            result.AddError(new ServiceError(
                nameof(IncludedItem),
                $"The IncludedItem {includedItemId} does not exist.",
                ErrorType.NotFound));

            return result;
        }

        includedItem.Name = includedItemData.Name;

        await unitOfWork.Complete();
        
        result.SetEntity(includedItem);
        
        return result;
    }

    public async Task<ServiceResult<IncludedItem>> DeleteIncludedItemAsync(Guid includedItemId)
    {
        var includedItem = await unitOfWork.IncludedItems.GetBy(x => x.Id == includedItemId);

        if (includedItem is null)
        {
            logger.LogNotFoundError<IncludedItem>(includedItemId);

            result.AddError(new ServiceError(
                nameof(IncludedItem),
                $"The IncludedItem {includedItemId} does not exist.",
                ErrorType.NotFound));

            return result;
        }

        unitOfWork.IncludedItems.Remove(includedItem);
        await unitOfWork.Complete();

        return result;
    }
}