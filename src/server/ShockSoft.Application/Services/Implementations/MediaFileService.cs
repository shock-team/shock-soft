﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public class MediaFileService : IMediaFileService
{
    private readonly ILogger<MediaFileService> logger;
    private readonly MediaFileOptions options;
    private readonly IUnitOfWork unitOfWork;
    private readonly ServiceResult<MediaFile> result = new();
    private readonly ServiceResult<MediaFileMetadata> metadataResult = new();
    
    public MediaFileService(
        IUnitOfWork unitOfWork,
        IOptions<MediaFileOptions> options,
        ILogger<MediaFileService> logger)
    {
        this.unitOfWork = unitOfWork;
        this.logger = logger;
        this.options = options.Value;
    }

    public async Task<ServiceResult<MediaFile>> AddMediaFileAsync(MediaFileCreationData mediaFileData)
    {
        var regex = new Regex(
            @"^data\:(?<mimeType>image|audio|video)\/(?<extension>png|tiff|jpg|jpeg|mp4|mpeg|webm|mp3|aac|ogg);base64,(?<data>[A-Z0-9\+\/\=]+)$",
            RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase);

        var match = regex.Match(mediaFileData.Base64Data);
        
        if (!match.Success)
        {
            const string error = 
                "This MediaFile is not supported. Supported extensions: png|tiff|jpg|jpeg|mp4|mpeg|webm|mp3|aac|ogg";
            
            logger.LogError(error);

            result.AddError(new ServiceError(
                nameof(MediaFile),
                error,
                ErrorType.BadRequest));
            
            return result;
        }

        var mimeType = match.Groups["mimeType"].Value;
        var extension = match.Groups["extension"].Value;
        var base64Data = match.Groups["data"].Value;

        FileType mediaFileType;

        switch (mimeType)
        {
            case "image":
                mediaFileType = FileType.Image;
                break;
            case "video":
                mediaFileType = FileType.Video;
                break;
            case "audio":
                mediaFileType = FileType.Audio;
                break;
            
            default:
            {
                const string error = 
                    "This MediaFile is not supported. Supported media types: image|video|audio";
            
                logger.LogError(error);

                result.AddError(new ServiceError(
                    nameof(MediaFile),
                    error,
                    ErrorType.BadRequest));
            
                return result;
            }
        }

        var mediaFileId = Guid.NewGuid();

        var repair = await unitOfWork.Repairs.GetBy(x => x.Id == mediaFileData.RepairId);

        if (repair is null)
        {
            logger.LogNotFoundError<Repair>(mediaFileData.RepairId);

            result.AddError(new ServiceError(
                nameof(Repair),
                $"The Repair {mediaFileData.RepairId} does not exist",
                ErrorType.NotFound));

            return result;
        }

        var mediaFile = new MediaFile
        {
            Id = mediaFileId,
            Name = mediaFileData.Name,
            Type = mediaFileType,
            Extension = extension,
            Repair = repair
        };

        var filePath = GetFilePath(options.SavePath, mediaFileId, mediaFileData.Name, extension);
        await File.WriteAllBytesAsync(filePath, Convert.FromBase64String(base64Data));

        unitOfWork.MediaFiles.Add(mediaFile);
        await unitOfWork.Complete();
        
        result.SetEntity(mediaFile);

        return result;
    }

    public async Task<MediaFile> GetMediaFileByIdAsync(Guid mediaFileId)
    {
        var mediaFile = await unitOfWork.MediaFiles.GetBy(
            x => x.Id == mediaFileId,
            x => x.Repair);

        return mediaFile;
    }

    public async Task<ServiceResult<MediaFileMetadata>> GetFileMetadata(Guid mediaFileId)
    {
        var mediaFile = await unitOfWork.MediaFiles.GetBy(
            x => x.Id == mediaFileId);

        if (mediaFile is null)
        {
            logger.LogNotFoundError<MediaFile>(mediaFileId);

            metadataResult.AddError(new ServiceError(
                nameof(MediaFile),
                $"The MediaFile {mediaFileId} does not exist",
                ErrorType.NotFound));

            return metadataResult;
        }
        
        metadataResult.SetEntity(new MediaFileMetadata(
            mediaFile.Name,
            GetFilePath(options.SavePath, mediaFile.Id, mediaFile.Name, mediaFile.Extension)));
        
        return metadataResult;
    }

    public async Task<IEnumerable<MediaFile>> GetMediaFilesByRepairIdAsync(Guid repairId)
    {
        return await unitOfWork.MediaFiles.Find(
            x => x.Repair.Id == repairId,
            x => x.Repair);
    }

    public async Task<ServiceResult<MediaFile>> DeleteMediaFileAsync(Guid mediaFileId)
    {
        var mediaFile = await unitOfWork.MediaFiles.GetBy(x => x.Id == mediaFileId);

        if (mediaFile is null)
        {
            logger.LogNotFoundError<MediaFile>(mediaFileId);

            result.AddError(new ServiceError(
                nameof(MediaFile),
                $"The MediaFile {mediaFileId} does not exist",
                ErrorType.NotFound));

            return result;
        }

        var filePath = GetFilePath(options.SavePath, mediaFileId, mediaFile.Name, mediaFile.Extension);

        if (!File.Exists(filePath))
        {
            logger.LogError($"The file {filePath} does not exist", filePath);

            result.AddError(new ServiceError(
                nameof(MediaFile),
                $"The file {filePath} does not exist.",
                ErrorType.BadRequest));

            return result;
        }

        File.Delete(filePath);

        var exists = File.Exists(filePath);

        if (exists)
        {
            logger.LogError($"The file {filePath} couldn't be deleted", filePath);

            result.AddError(new ServiceError(
                nameof(MediaFile),
                $"The file {filePath} couldn't be deleted.",
                ErrorType.BadRequest));

            return result;
        }

        unitOfWork.MediaFiles.Remove(mediaFile);
        await unitOfWork.Complete();

        return result;
    }

    private static string GetFilePath(string savePath, Guid mediaFileId, string mediaFileName, string mediaFileExtension)
    {
        return $@"{savePath}/{mediaFileId}-{mediaFileName}.{mediaFileExtension}";
    }
}