﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public class ProductService : IProductService
{
    private readonly ILogger<ProductService> logger;
    private readonly IUnitOfWork unitOfWork;
    private readonly ServiceResult<Product> result = new();

    public ProductService(
        ILogger<ProductService> logger,
        IUnitOfWork unitOfWork)
    {
        this.logger = logger;
        this.unitOfWork = unitOfWork;
    }

    public async Task<Product> GetProductByIdAsync(Guid productId)
    {
        return await unitOfWork.Products.GetBy(x => x.Id == productId);
    }

    public async Task<PagedList<Product>> GetPagedProductsAsync(
        ProductFilterData productFilterData)
    {
        Expression<Func<Product, bool>> expression = x => x != null;

        if (!string.IsNullOrWhiteSpace(productFilterData.Name))
        {
            Expression<Func<Product, bool>> nameFilter = x =>
                x.Name.ToUpper().Contains(productFilterData.Name.ToUpper());
            expression = ExpressionUtils.ConcatLambdaExpression(expression, nameFilter);
        }

        return await unitOfWork.Products.GetPaginated(
            x => x.Name,
            productFilterData.PageNumber,
            productFilterData.PageSize,
            expression);
    }

    public async Task<ServiceResult<Product>> AddProductAsync(ProductCreationData productData)
    {
        var productId = Guid.NewGuid();

        var product = new Product
        {
            Id = productId,
            Name = productData.Name
        };

        unitOfWork.Products.Add(product);
        await unitOfWork.Complete();
        
        result.SetEntity(product);
        
        return result;
    }

    public async Task<ServiceResult<Product>> UpdateProductAsync(Guid productId, ProductUpdateData productData)
    {
        var product = await unitOfWork.Products.GetBy(x => x.Id == productId);

        if (product is null)
        {
            logger.LogNotFoundError<Product>(productId);

            result.AddError(new ServiceError(
                nameof(Product),
                $"The Product {productId} does not exist.",
                ErrorType.NotFound));

            return result;
        };

        product.Name = productData.Name;

        await unitOfWork.Complete();
        
        result.SetEntity(product);
        
        return result;    
    }

    public async Task<ServiceResult<Product>> DeleteProductAsync(Guid productId)
    {
        var product = await unitOfWork.Products.GetBy(x => x.Id == productId);

        if (product is null)
        {
            logger.LogNotFoundError<Product>(productId);

            result.AddError(new ServiceError(
                nameof(Product),
                $"The Product {productId} does not exist.",
                ErrorType.NotFound));

            return result;
        };

        unitOfWork.Products.Remove(product);
        await unitOfWork.Complete();

        return result;
    }
}