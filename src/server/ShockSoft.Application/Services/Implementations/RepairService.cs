﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ShockSoft.Domain;
using ShockSoft.Persistence;
using ShockSoft.TelegramApi;
using Telegram.Bot;

namespace ShockSoft.Application;

public class RepairService : IRepairService
{
    private readonly ILogger<RepairService> logger;
    private readonly ITelegramApiClient telegramApiClient;
    private readonly TelegramIntegrationOptions telegramOptions;
    private readonly IUnitOfWork unitOfWork;
    private readonly ServiceResult<Repair> result = new();

    public RepairService(
        ILogger<RepairService> logger,
        IOptions<TelegramIntegrationOptions> options,
        IUnitOfWork unitOfWork,
        ITelegramApiClient telegramApiClient)
    {
        telegramOptions = options.Value;
        this.logger = logger;
        this.unitOfWork = unitOfWork;
        this.telegramApiClient = telegramApiClient;
    }

    public async Task<Repair> GetRepairByIdAsync(Guid repairId)
    {
        return await unitOfWork.Repairs.GetBy(
            x => x.Id == repairId,
            x => x.Include(r => r.Client),
            x => x.Include(r => r.DeviceType),
            x => x.Include(r => r.IncludedItems),
            x => x.Include(r => r.StateChanges),
            x => x.Include(r => r.RepairLines).ThenInclude(rl => rl.Product));
    }

    public async Task<IEnumerable<Repair>> GetAllRepairsAsync()
    {
        return await unitOfWork.Repairs.GetAll();
    }

    public async Task<PagedList<Repair>> GetPagedRepairsAsync(
        RepairFilterData repairFilterData)
    {
        Expression<Func<Repair, bool>> filterExpression = x => x != null;

        if (!string.IsNullOrWhiteSpace(repairFilterData.ClientName))
        {
            Expression<Func<Repair, bool>> nameFilter = x =>
                x.Client.Name.ToUpper().Contains(repairFilterData.ClientName.ToUpper());
            filterExpression = ExpressionUtils.ConcatLambdaExpression(filterExpression, nameFilter);
        }

        if (!string.IsNullOrWhiteSpace(repairFilterData.ClientSurname))
        {
            Expression<Func<Repair, bool>> surnameFilter = x =>
                x.Client.Surname.ToUpper().Contains(repairFilterData.ClientSurname.ToUpper());
            filterExpression = ExpressionUtils.ConcatLambdaExpression(filterExpression, surnameFilter);
        }

        if (repairFilterData.State is not null)
        {
            Expression<Func<Repair, bool>> stateFilter = x => x.State == repairFilterData.State;
            filterExpression = ExpressionUtils.ConcatLambdaExpression(filterExpression, stateFilter);
        }

        return await unitOfWork.Repairs.GetPaginated(
            r => r.StateChanges.OrderBy(sc => sc.Date).Last().Date,
            repairFilterData.PageNumber,
            repairFilterData.PageSize,
            filterExpression,
            x => x.Include(r => r.Client),
            x => x.Include(r => r.DeviceType),
            x => x.Include(r => r.IncludedItems),
            x => x.Include(r => r.StateChanges),
            x => x.Include(r => r.RepairLines).ThenInclude(rl => rl.Product));
    }
    
    public async Task<ServiceResult<Repair>> AddRepairAsync(RepairCreationData repairData)
    {
        var client = await unitOfWork.Clients.GetBy(x => x.Id == repairData.ClientId);
        if (client is null)
        {
            logger.LogNotFoundError<Client>(repairData.ClientId);
            
            result.AddError(new ServiceError(
                nameof(Client),
                $"The Client {repairData.ClientId} does not exist.",
                ErrorType.BadRequest));

            return result;
        }

        var deviceType = await unitOfWork.DeviceTypes.GetBy(x => x.Id == repairData.DeviceTypeId);
        
        if (deviceType is null)
        {
            logger.LogNotFoundError<DeviceType>(repairData.DeviceTypeId);
            
            result.AddError(new ServiceError(
                nameof(DeviceType),
                $"The DeviceType {repairData.DeviceTypeId} does not exist.",
                ErrorType.BadRequest));

            return result;
        }

        var repairId = Guid.NewGuid();

        var repair = new Repair
        {
            Id = repairId,
            Problem = repairData.Problem,
            Solution = repairData.Solution,
            LaborPrice = repairData.LaborPrice ?? 0,
            DevicePassword = repairData.DevicePassword,
            Client = client,
            State = RepairState.Queued,
            DeviceType = deviceType
        };

        unitOfWork.Repairs.Add(repair);

        var stateChange = new StateChange
        {
            Id = Guid.NewGuid(),
            Date = repairData.CreationDate ?? DateTime.UtcNow,
            State = RepairState.Queued,
            Repair = repair
        };

        unitOfWork.StateChanges.Add(stateChange);

        await HandleIncludedItems(repair, repairData.IncludedItems);

        await unitOfWork.Complete();
        
        result.SetEntity(repair);
        
        return result;
    }

    public async Task<ServiceResult<Repair>> UpdateRepairAsync(Guid repairId, RepairUpdateData repairData)
    {
        var repair = await unitOfWork.Repairs.GetBy(
            x => x.Id == repairId,
            x => x.Include(r=>r.Client).ThenInclude(c=> c.Chats),
            x => x.Include(r =>r.DeviceType),
            x => x.Include(r=>r.IncludedItems),
            x => x.Include(r=>r.StateChanges),
            x => x.Include(r => r.RepairLines).ThenInclude(rl => rl.Product));
        
        if (repair is null)
        {
            logger.LogNotFoundError<Repair>(repairId);
            
            result.AddError(new ServiceError(
                nameof(Repair),
                $"The Repair {repairId} does not exist.",
                ErrorType.NotFound));
            
            return result;
        }

        repair.Problem = repairData.Problem;
        repair.Solution = repairData.Solution;
        repair.LaborPrice = repairData.LaborPrice;
        repair.DevicePassword = repairData.DevicePassword;
        repair.StateChanges.First(s => s.State == RepairState.Queued).Date = repairData.CreationDate;

        if (repair.Client.Id != repairData.ClientId)
        {
            var newClient = await unitOfWork.Clients.GetBy(x => x.Id == repairData.ClientId);

            if (newClient is null)
            {
                logger.LogNotFoundError<Client>(repairData.ClientId);
                result.AddError(new ServiceError(
                    nameof(Client),
                    $"The Client {repairData.ClientId} does not exist.",
                    ErrorType.BadRequest));
                
                return result;
            }

            repair.Client = newClient;
        }

        if (repair.DeviceType.Id != repairData.DeviceTypeId)
        {
            var newDeviceType = await unitOfWork.DeviceTypes.GetBy(x => x.Id == repairData.DeviceTypeId);

            if (newDeviceType is null)
            {
                logger.LogNotFoundError<DeviceType>(repairData.DeviceTypeId);
                
                result.AddError(new ServiceError(
                    nameof(DeviceType),
                    $"The DeviceType {repairData.DeviceTypeId} does not exist.",
                    ErrorType.BadRequest));
                
                return result;
            }

            repair.DeviceType = newDeviceType;
        }

        var repairLinesResult = await HandleRepairLines(repair, repairData.RepairLines);

        if (repairLinesResult is not null)
        {
            return repairLinesResult;
        }
        
        await HandleIncludedItems(repair, repairData.IncludedItems);

        var originalRepairState = repair.State;

        if (repair.State != repairData.State)
        {
            repair.State = repairData.State;

            var stateChange = new StateChange
            {
                Id = Guid.NewGuid(),
                Date = DateTime.UtcNow,
                State = repairData.State,
                Repair = repair
            };

            unitOfWork.StateChanges.Add(stateChange);
        }

        var changes = await unitOfWork.Complete();

        if (originalRepairState != repairData.State
            && changes > 1
            && repair.Client.Chats.Any()
            && telegramOptions.Enabled)
        {
            foreach (var chat in repair.Client.Chats)
            {
                await telegramApiClient.BotClient.SendTextMessageAsync(
                    chat.ChatId,
                    $"The repair with the problem: \"{repair.Problem}\" was updated. The state now is {repair.State.ToString()}");
            }
        }
        
        result.SetEntity(repair);
            
        return result;
    }

    private async Task HandleIncludedItems(Repair repair, IList<IncludedItemData> includedItems)
    {
        var input = includedItems.ToList();
        var toCreate = input.Where(c => repair.IncludedItems.SingleOrDefault(x => x.Id == c.Id) == null)
            .ToList();

        var toDelete = repair.IncludedItems.Where(c => input.All(ce => ce.Id != c.Id)).ToList();

        foreach (var item in toCreate)
        {
            var includedItem = await unitOfWork.IncludedItems.GetBy(x => x.Id == item.Id);

            if (includedItem is null)
            {
                var newIncludedItem = new IncludedItem
                {
                    Id = Guid.NewGuid(),
                    Name = item.Name
                };

                repair.IncludedItems.Add(newIncludedItem);
            }
            else
            {
                repair.IncludedItems.Add(includedItem);
            }
        }

        foreach (var item in toDelete)
        {
            repair.IncludedItems.Remove(item);
        }
    }
    
    private async Task<ServiceResult<Repair>?> HandleRepairLines(Repair repair, IList<RepairLineData> lines)
    {
        var input = lines.ToList();
        var toCreate = input.Where(c => repair.RepairLines.SingleOrDefault(x => x.Id == c.Id) == null)
            .ToList();

        var toDelete = repair.RepairLines.Where(c => input.All(ce => ce.Id != c.Id)).ToList();

        foreach (var item in toCreate)
        {
            var product = await unitOfWork.Products.GetBy(x => x.Id == item.ProductId);

            if (product is null)
            {
                logger.LogNotFoundError<Product>(item.ProductId);
                        
                result.AddError(new ServiceError(
                    nameof(Product),
                    $"The Product {item.ProductId} does not exist.",
                    ErrorType.BadRequest));

                return result;
            }
            
            var newRepairLine = new RepairLine
            {
                Id = Guid.NewGuid(),
                Price = item.Price,
                Product = product,
                Quantity = item.Quantity,
                Repair = repair
            };

            unitOfWork.RepairLines.Add(newRepairLine);
        }

        foreach (var item in toDelete)
        {
            repair.RepairLines.Remove(item);
        }

        return default;
    }
}