﻿using System;
using System.Threading.Tasks;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public interface ICityService
{
    Task<ServiceResult<City>> AddCityAsync(CityCreationData cityData);
    Task<ServiceResult<City>> DeleteCityAsync(Guid cityId);
    Task<City> GetCityByIdAsync(Guid cityId);
    Task<PagedList<City>> GetPagedCitiesAsync(CityFilterData cityFilterData);
    Task<ServiceResult<City>> UpdateCityAsync(Guid cityId, CityUpdateData cityData);
    Task<ServiceResult<City>> SeedCities();
}