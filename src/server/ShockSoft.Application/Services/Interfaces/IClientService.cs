﻿using System;
using System.Threading.Tasks;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public interface IClientService
{
    Task<Client> GetClientByIdAsync(Guid clientId);
    Task<ServiceResult<Client>> UpdateClientAsync(Guid clientId, ClientUpdateData clientData);
    Task<ServiceResult<Client>> AddClientAsync(ClientCreationData clientData);
    Task<PagedList<Client>> GetPagedClientsAsync(ClientFilterData clientFilterData);
    Task<ServiceResult<Client>> DeleteClientAsync(Guid clientId);
}