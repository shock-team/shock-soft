﻿using System;
using System.Threading.Tasks;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public interface IDeviceTypeService
{
    Task<ServiceResult<DeviceType>> AddDeviceTypeAsync(DeviceTypeCreationData deviceTypeData);
    Task<ServiceResult<DeviceType>> DeleteDeviceTypeAsync(Guid deviceTypeId);
    Task<DeviceType> GetDeviceTypeByIdAsync(Guid deviceTypeId);
    Task<PagedList<DeviceType>> GetPagedDeviceTypesAsync(DeviceTypeFilterData deviceTypeFilterData);
    Task<ServiceResult<DeviceType>> UpdateDeviceTypeAsync(Guid deviceTypeId, DeviceTypeUpdateData deviceTypeData);
}