﻿using System;
using System.Threading.Tasks;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public interface IIncludedItemService
{
    Task<ServiceResult<IncludedItem>> AddIncludedItemAsync(IncludedItemCreationData includedItemData);
    Task<ServiceResult<IncludedItem>> DeleteIncludedItemAsync(Guid includedItemId);
    Task<IncludedItem> GetIncludedItemByIdAsync(Guid includedItemId);
    Task<PagedList<IncludedItem>> GetPagedIncludedItemsAsync(IncludedItemFilterData includedItemFilterData);
    Task<ServiceResult<IncludedItem>> UpdateIncludedItemAsync(Guid includedItemId, IncludedItemUpdateData includedItemData);
}