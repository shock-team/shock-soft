﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ShockSoft.Domain;

namespace ShockSoft.Application;

public interface IMediaFileService
{
    Task<ServiceResult<MediaFile>> AddMediaFileAsync(MediaFileCreationData mediaFileData);
    Task<MediaFile> GetMediaFileByIdAsync(Guid mediaFileId);
    Task<ServiceResult<MediaFile>> DeleteMediaFileAsync(Guid mediaFileId);
    Task<IEnumerable<MediaFile>> GetMediaFilesByRepairIdAsync(Guid repairId);
    Task<ServiceResult<MediaFileMetadata>> GetFileMetadata(Guid mediaFileId);
}