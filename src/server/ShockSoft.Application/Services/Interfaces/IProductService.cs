﻿using System;
using System.Threading.Tasks;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public interface IProductService
{
    Task<ServiceResult<Product>> AddProductAsync(ProductCreationData productData);
    Task<ServiceResult<Product>> DeleteProductAsync(Guid productId);
    Task<PagedList<Product>> GetPagedProductsAsync(ProductFilterData productFilterData);
    Task<Product> GetProductByIdAsync(Guid productId);
    Task<ServiceResult<Product>> UpdateProductAsync(Guid productId, ProductUpdateData productData);
}