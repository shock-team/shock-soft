﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ShockSoft.Domain;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public interface IRepairService
{
    Task<ServiceResult<Repair>> AddRepairAsync(RepairCreationData repairData);
    Task<IEnumerable<Repair>> GetAllRepairsAsync();
    Task<PagedList<Repair>> GetPagedRepairsAsync(RepairFilterData repairFilterData);
    Task<Repair> GetRepairByIdAsync(Guid repairId);
    Task<ServiceResult<Repair>> UpdateRepairAsync(Guid repairId, RepairUpdateData repairData);
}