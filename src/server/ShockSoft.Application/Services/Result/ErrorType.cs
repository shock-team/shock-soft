﻿namespace ShockSoft.Application;

public enum ErrorType
{
    NotFound = 0,
    BadRequest = 1
}