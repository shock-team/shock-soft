﻿namespace ShockSoft.Application;

public class ServiceError
{
    public ServiceError(string title, string description, ErrorType errorType)
    {
        Title = title;
        Description = description;
        Type = errorType;
    }
    
    public ErrorType Type { get; }
    public string Title { get; }
    public string Description { get; }
}