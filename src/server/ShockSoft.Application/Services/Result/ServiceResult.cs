﻿namespace ShockSoft.Application;

public class ServiceResult<TEntityResult>
    where TEntityResult : class
{
    public TEntityResult? Entity { get; private set; }
    public bool Errored { get; private set; }
    public ServiceError? Error { get; private set; }
    
    public void AddError(ServiceError error)
    {
        Errored = true;
        Error = error;
    }

    public void SetEntity(TEntityResult entity)
    {
        Entity = entity;
    }
}