using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ShockSoft.Persistence;
using ShockSoft.TelegramApi;

namespace ShockSoft.Application;

public class Startup
{
    public Startup()
    {
        var configBuilder = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .AddJsonFile("telegramsettings.json", false, true);

        Configuration = configBuilder.Build();
    }

    private IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddCors();
        services.AddControllersWithViews();
        services.AddAutoMapper(typeof(Startup));
        services.AddOptions();

        services.AddDbContext<ShockSoftContext>(options =>
            options.UseSqlite(Configuration.GetConnectionString("Default"))
        );

        services.AddSwaggerGen();

        FirebaseApp.Create(new AppOptions
        {
            Credential = GoogleCredential.FromFile("shock-soft-firebase-adminsdk-t5ch4-03376fdf6f.json")
        });

        services.AddFirebaseAuthentication(Configuration);

        services.RegisterRepositories();
        services.RegisterServices();
        services.RegisterOptions(Configuration);

        services.RegisterTelegramApiDependencies(Configuration);

        if (Configuration.GetSection(TelegramIntegrationOptions.TelegramIntegration)
            .Get<TelegramIntegrationOptions>().Enabled)
        {
            services.AddHostedService<TelegramApiClientHostedService>();
        }
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseCors(options =>
        {
            options.WithOrigins(Configuration.GetSection("ClientUrls").Get<string[]>());
            options.WithExposedHeaders("Pagination");
            options.AllowAnyMethod();
            options.AllowAnyHeader();
        });

        if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

        app.UseSwagger();
        app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "ShockSoft API V1"); });

        app.UseRouting();
        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute(
                "default",
                "{controller}/{action=Index}/{id?}");
        });
    }
}