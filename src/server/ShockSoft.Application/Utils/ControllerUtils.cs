﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using ShockSoft.Persistence;

namespace ShockSoft.Application;

public static class ControllerUtils
{
    public static void AddPaginationHeaders<T>(PagedList<T> pagedList, HttpResponse response)
    {
        var metadata = new
        {
            pagedList.TotalCount,
            pagedList.PageSize,
            pagedList.CurrentPage,
            pagedList.TotalPages,
            pagedList.HasPrevious,
            pagedList.HasNext
        };

        response.Headers.Add("Pagination", JsonConvert.SerializeObject(metadata));
    }
}