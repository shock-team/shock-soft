﻿using System;
using System.Linq.Expressions;

namespace ShockSoft.Application;

public static class ExpressionUtils
{
    public static Expression<Func<T, bool>> ConcatLambdaExpression<T>(Expression<Func<T, bool>> firstExpression,
        Expression<Func<T, bool>> secondExpression)
    {
        var invokedThird = Expression.Invoke(secondExpression, firstExpression.Parameters);
        var finalExpression = Expression.Lambda<Func<T, bool>>(Expression.AndAlso(firstExpression.Body, invokedThird),
            firstExpression.Parameters);
        return finalExpression;
    }
}