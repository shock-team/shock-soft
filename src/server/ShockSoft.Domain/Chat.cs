namespace ShockSoft.Domain;

public class Chat
{
    public Client Client { get; set; }
    public long ChatId { get; set; }
    public ChatState State { get; set; } = ChatState.New;
    public int ReplyToMessageId { get; set; }
}