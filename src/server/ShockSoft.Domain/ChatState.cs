namespace ShockSoft.Domain;

public enum ChatState
{
    New = 0,
    PendingSync = 1,
    Synced = 2
}