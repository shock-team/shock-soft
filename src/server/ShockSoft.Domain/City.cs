﻿using System;
using System.Collections.Generic;

namespace ShockSoft.Domain;

public class City
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public IEnumerable<Client> Clients { get; set; }
}