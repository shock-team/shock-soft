﻿using System;
using System.Collections.Generic;

namespace ShockSoft.Domain;

public class Client
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public string PhoneNumber { get; set; }
    public string Email { get; set; }
    public string Address { get; set; }
    public City City { get; set; }
    public IEnumerable<Repair> Repairs { get; set; }
    public IList<Chat> Chats { get; set; } = new List<Chat>();
}