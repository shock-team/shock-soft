﻿using System;

namespace ShockSoft.Domain;

public class DeviceType
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}