﻿namespace ShockSoft.Domain;

public enum FileType
{
    Image = 0,
    Audio = 1,
    Video = 2
}