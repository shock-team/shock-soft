﻿using System;
using System.Collections.Generic;

namespace ShockSoft.Domain;

public class IncludedItem
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public IList<Repair> Repairs { get; set; } = new List<Repair>();
}