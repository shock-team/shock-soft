﻿using System;

namespace ShockSoft.Domain;

public class MediaFile
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Base64Data { get; set; }
    public string Extension { get; set; }
    public FileType Type { get; set; }
    public Repair Repair { get; set; }
}