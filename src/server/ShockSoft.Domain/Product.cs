﻿using System;
using System.Collections.Generic;

namespace ShockSoft.Domain;

public class Product
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public IList<RepairLine> RepairLines { get; set; } = new List<RepairLine>();
}