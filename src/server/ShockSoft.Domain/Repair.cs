﻿using System;
using System.Collections.Generic;

namespace ShockSoft.Domain;

public class Repair
{
    public Guid Id { get; set; }
    public string Problem { get; set; }
    public string Solution { get; set; }
    public decimal LaborPrice { get; set; }
    public string DevicePassword { get; set; }
    public Client Client { get; set; }
    public DeviceType DeviceType { get; set; }
    public RepairState State { get; set; }
    public IList<StateChange> StateChanges { get; set; } = new List<StateChange>();
    public IList<IncludedItem> IncludedItems { get; set; } = new List<IncludedItem>();
    public IList<RepairLine> RepairLines { get; set; } = new List<RepairLine>();
    public IList<MediaFile> MediaFiles { get; set; } = new List<MediaFile>();
}