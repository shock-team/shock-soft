﻿using System;

namespace ShockSoft.Domain;

public class RepairLine
{
    public Guid Id { get; set; }
    public int Quantity { get; set; }
    public decimal Price { get; set; }
    public Product Product { get; set; }
    public Repair Repair { get; set; }
}