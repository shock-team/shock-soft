﻿namespace ShockSoft.Domain;

public enum RepairState
{
    Queued = 0,
    Active = 1,
    Pending = 2,
    Repaired = 3,
    Cancelled = 4,
    Paid = 5,
    Irreparable = 6
}