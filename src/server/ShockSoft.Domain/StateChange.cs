﻿using System;

namespace ShockSoft.Domain;

public class StateChange
{
    public Guid Id { get; set; }
    public RepairState State { get; set; }
    public DateTime Date { get; set; }
    public Repair Repair { get; set; }
}