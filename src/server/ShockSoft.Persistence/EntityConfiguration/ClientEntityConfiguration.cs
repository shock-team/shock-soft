﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ShockSoft.Domain;

namespace ShockSoft.Persistence;

internal class ClientEntityConfiguration : IEntityTypeConfiguration<Client>
{
    public void Configure(EntityTypeBuilder<Client> builder)
    {
        builder.ToTable("Clients");

        builder.HasKey(x => x.Id);
        builder.Property(x => x.Name);
        builder.Property(x => x.Surname);
        builder.Property(x => x.Email);
        builder.Property(x => x.Address);
        builder.Property(x => x.PhoneNumber);

        builder.HasOne(x => x.City);
        builder.HasMany(x => x.Repairs);
        builder.HasMany(x => x.Chats);
    }
}