﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ShockSoft.Domain;

namespace ShockSoft.Persistence;

internal class IncludedItemEntityConfiguration : IEntityTypeConfiguration<IncludedItem>
{
    public void Configure(EntityTypeBuilder<IncludedItem> builder)
    {
        builder.ToTable("IncludedItems");

        builder.HasKey(x => x.Id);
        builder.Property(x => x.Name);

        builder.HasMany(x => x.Repairs);
    }
}