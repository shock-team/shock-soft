﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ShockSoft.Domain;

namespace ShockSoft.Persistence;

internal class MediaFileEntityConfiguration : IEntityTypeConfiguration<MediaFile>
{
    public void Configure(EntityTypeBuilder<MediaFile> builder)
    {
        builder.ToTable("MediaFiles");

        builder.HasKey(x => x.Id);
        builder.Property(x => x.Name);
        builder.Property(x => x.Extension);
        builder.Property(x => x.Type);
        builder.Ignore(x => x.Base64Data);

        builder.HasOne(x => x.Repair);
    }
}