﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ShockSoft.Domain;

namespace ShockSoft.Persistence;

internal class RepairEntityConfiguration : IEntityTypeConfiguration<Repair>
{
    public void Configure(EntityTypeBuilder<Repair> builder)
    {
        builder.ToTable("Repairs");

        builder.HasKey(x => x.Id);
        builder.Property(x => x.Problem);
        builder.Property(x => x.Solution);
        builder.Property(x => x.DevicePassword);
        builder.Property(x => x.LaborPrice);
        builder.Property(x => x.State);

        builder.HasOne(x => x.Client);
        builder.HasOne(x => x.DeviceType);
        builder.HasMany(x => x.MediaFiles);
        builder.HasMany(x => x.StateChanges);
        builder.HasMany(x => x.RepairLines);
        builder.HasMany(x => x.IncludedItems);
    }
}