﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ShockSoft.Domain;

namespace ShockSoft.Persistence;

internal class RepairLineEntityConfiguration : IEntityTypeConfiguration<RepairLine>
{
    public void Configure(EntityTypeBuilder<RepairLine> builder)
    {
        builder.ToTable("RepairLines");

        builder.HasKey(x => x.Id);
        builder.Property(x => x.Quantity);
        builder.Property(x => x.Price);

        builder.HasOne(x => x.Repair);
        builder.HasOne(x => x.Product);
    }
}