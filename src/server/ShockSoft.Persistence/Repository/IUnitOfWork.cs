﻿using System;
using System.Threading.Tasks;

namespace ShockSoft.Persistence;

public interface IUnitOfWork : IDisposable
{
    IClientRepository Clients { get; }
    ICityRepository Cities { get; }
    IMediaFileRepository MediaFiles { get; }
    IRepairRepository Repairs { get; }
    IRepairLineRepository RepairLines { get; set; }
    IDeviceTypeRepository DeviceTypes { get; set; }
    IIncludedItemRepository IncludedItems { get; set; }
    IStateChangeRepository StateChanges { get; set; }
    IProductRepository Products { get; set; }
    IChatRepository Chats { get; set; }
    Task<int> Complete();
}