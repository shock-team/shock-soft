using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class ChatRepository : Repository<Chat>, IChatRepository
{
    public ChatRepository(ShockSoftContext context) : base(context)
    {
    }
}