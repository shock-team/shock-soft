﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class CityRepository : Repository<City>, ICityRepository
{
    public CityRepository(ShockSoftContext context) : base(context)
    {
    }
}