﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class ClientRepository : Repository<Client>, IClientRepository
{
    public ClientRepository(ShockSoftContext context) : base(context)
    {
    }
}