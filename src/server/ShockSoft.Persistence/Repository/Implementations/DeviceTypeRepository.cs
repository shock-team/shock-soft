﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class DeviceTypeRepository : Repository<DeviceType>, IDeviceTypeRepository
{
    public DeviceTypeRepository(ShockSoftContext context) : base(context)
    {
    }
}