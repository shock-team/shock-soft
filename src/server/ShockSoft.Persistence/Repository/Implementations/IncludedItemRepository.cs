﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class IncludedItemRepository : Repository<IncludedItem>, IIncludedItemRepository
{
    public IncludedItemRepository(ShockSoftContext context) : base(context)
    {
    }
}