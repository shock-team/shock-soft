﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class MediaFileRepository : Repository<MediaFile>, IMediaFileRepository
{
    public MediaFileRepository(ShockSoftContext context) : base(context)
    {
    }
}