﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class ProductRepository : Repository<Product>, IProductRepository
{
    public ProductRepository(ShockSoftContext context) : base(context)
    {
    }
}