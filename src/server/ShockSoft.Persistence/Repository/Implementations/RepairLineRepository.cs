﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class RepairLineRepository : Repository<RepairLine>, IRepairLineRepository
{
    public RepairLineRepository(ShockSoftContext context) : base(context)
    {
    }
}