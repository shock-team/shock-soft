﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class RepairRepository : Repository<Repair>, IRepairRepository
{
    public RepairRepository(ShockSoftContext context) : base(context)
    {
    }
}