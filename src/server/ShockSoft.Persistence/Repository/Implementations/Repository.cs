﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ShockSoft.Persistence;

public class Repository<T> : IRepository<T>
    where T : class
{
    private readonly ShockSoftContext context;

    public Repository(ShockSoftContext context)
    {
        this.context = context;
    }

    public void Add(T entity)
    {
        context.Set<T>()
            .Add(entity);
    }

    public void AddRange(IEnumerable<T> entities)
    {
        context.Set<T>()
            .AddRange(entities);
    }

    public async Task<IEnumerable<T>> Find(
        Expression<Func<T, bool>> expression,
        params Expression<Func<T, object>>[] includeExpressions)
    {
        var query = context.Set<T>()
            .AsQueryable();

        query = includeExpressions.Aggregate(query, (current, includeProp) => current.Include(includeProp));

        return await query.Where(expression)
            .ToListAsync()
            .ConfigureAwait(false);
    }

        public async Task<PagedList<T>> GetPaginated<TKey>(
            Expression<Func<T, TKey>> orderExpression,
            int pageNumber,
            int pageSize,
            Expression<Func<T, bool>> filterExpression = null)
        {
            var query = context.Set<T>()
                .AsQueryable();

            if (filterExpression is not null)
            {
                query = query.Where(filterExpression);
            }

            query = query.OrderBy(orderExpression);

            return await PagedList<T>.ToPagedList(
                query,
                pageNumber,
                pageSize);
        }

        public async Task<PagedList<T>> GetPaginated<TKey>(
            Expression<Func<T, TKey>> orderExpression,
            int pageNumber,
            int pageSize,
            Expression<Func<T, bool>> filterExpression = null,
            params Expression<Func<T, object>>[] includeExpressions)
        {
            var query = context.Set<T>()
                .AsQueryable();

            query = includeExpressions.Aggregate(query, (current, includeProp) => current.Include(includeProp));

            if (filterExpression is not null)
            {
                query = query.Where(filterExpression);
            }

            query = query.OrderBy(orderExpression);

            return await PagedList<T>.ToPagedList(
                query,
                pageNumber,
                pageSize);
        }
        
        public async Task<PagedList<T>> GetPaginated<TKey>(
            Expression<Func<T, TKey>> orderExpression,
            int pageNumber,
            int pageSize,
            Expression<Func<T, bool>> filterExpression = null,
            params Func<IIncludable<T>, IIncludable>[] includes)
        {
            var query = context.Set<T>()
                .AsQueryable();
            
            if (includes is not null)
            {
                query = includes.Aggregate(query, (current, includeProp) => current.IncludeMultiple(includeProp));
            }

            if (filterExpression is not null)
            {
                query = query.Where(filterExpression);
            }

            query = query.OrderBy(orderExpression);

            return await PagedList<T>.ToPagedList(
                query,
                pageNumber,
                pageSize);
        }

    public async Task<IEnumerable<T>> GetAll()
    {
        return await context.Set<T>()
            .ToListAsync()
            .ConfigureAwait(false);
    }
    
    public async Task<T> GetBy(
        Expression<Func<T, bool>> predicate)
    {
        var set = context.Set<T>()
            .AsQueryable();

        return await set.FirstOrDefaultAsync(predicate)
            .ConfigureAwait(false);
    }

    public async Task<T> GetBy(
        Expression<Func<T, bool>> predicate,
        params Expression<Func<T, object>>[] includeExpressions)
    {
        var set = context.Set<T>()
            .AsQueryable();

        set = includeExpressions.Aggregate(set, (current, includeProp) => current.Include(includeProp));

        return await set.FirstOrDefaultAsync(predicate)
            .ConfigureAwait(false);
    }
    
    public async Task<T> GetBy(
        Expression<Func<T, bool>> predicate,
        params Func<IIncludable<T>, IIncludable>[] includes)
    {
        var set = context.Set<T>()
            .AsQueryable();

        set = includes.Aggregate(set, (current, includeProp) => current.IncludeMultiple(includeProp));

        return await set.FirstOrDefaultAsync(predicate)
            .ConfigureAwait(false);
    }

    public void Remove(T entity)
    {
        context.Set<T>()
            .Remove(entity);
    }

    public void RemoveRange(IEnumerable<T> entities)
    {
        context.Set<T>()
            .RemoveRange(entities);
    }

    public async Task<int> Count()
    {
        return await context.Set<T>()
            .CountAsync();
    }
}