﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class StateChangeRepository : Repository<StateChange>, IStateChangeRepository
{
    public StateChangeRepository(ShockSoftContext context) : base(context)
    {
    }
}