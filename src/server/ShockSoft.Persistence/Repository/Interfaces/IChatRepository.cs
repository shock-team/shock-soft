using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface IChatRepository : IRepository<Chat>
{
}