﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface ICityRepository : IRepository<City>
{
}