﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface IClientRepository : IRepository<Client>
{
}