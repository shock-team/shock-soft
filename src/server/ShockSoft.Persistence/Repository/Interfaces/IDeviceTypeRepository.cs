﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface IDeviceTypeRepository : IRepository<DeviceType>
{
}