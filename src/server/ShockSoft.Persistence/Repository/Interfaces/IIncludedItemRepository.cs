﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface IIncludedItemRepository : IRepository<IncludedItem>
{
}