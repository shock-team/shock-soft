﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface IMediaFileRepository : IRepository<MediaFile>
{
}