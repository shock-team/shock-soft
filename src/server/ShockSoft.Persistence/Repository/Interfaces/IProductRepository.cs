﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface IProductRepository : IRepository<Product>
{
}