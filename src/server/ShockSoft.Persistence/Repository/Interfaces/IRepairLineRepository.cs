﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface IRepairLineRepository : IRepository<RepairLine>
{
}