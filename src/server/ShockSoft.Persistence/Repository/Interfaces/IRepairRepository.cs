﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface IRepairRepository : IRepository<Repair>
{
}