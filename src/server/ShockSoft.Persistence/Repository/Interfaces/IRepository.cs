﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ShockSoft.Persistence;

public interface IRepository<T>
    where T : class
{
    Task<T> GetBy(
        Expression<Func<T, bool>> predicate);
    
    Task<T> GetBy(
        Expression<Func<T, bool>> predicate,
        params Expression<Func<T, object>>[] includeExpressions);
    
    Task<T> GetBy(
        Expression<Func<T, bool>> predicate,
        params Func<IIncludable<T>, IIncludable>[] includes);

    Task<IEnumerable<T>> GetAll();

    Task<PagedList<T>> GetPaginated<TKey>(
        Expression<Func<T, TKey>> orderExpression,
        int pageNumber,
        int pageSize,
        Expression<Func<T, bool>> filterExpression = null);

    Task<PagedList<T>> GetPaginated<TKey>(
        Expression<Func<T, TKey>> orderExpression,
        int pageNumber,
        int pageSize,
        Expression<Func<T, bool>> filterExpression = null,
        params Expression<Func<T, object>>[] includeExpressions);

    Task<PagedList<T>> GetPaginated<TKey>(
        Expression<Func<T, TKey>> orderExpression,
        int pageNumber,
        int pageSize,
        Expression<Func<T, bool>> filterExpression = null,
        params Func<IIncludable<T>, IIncludable>[] includes);

    Task<IEnumerable<T>> Find(
        Expression<Func<T, bool>> expression,
        params Expression<Func<T, object>>[] includeExpressions);

    void Add(T entity);
    void AddRange(IEnumerable<T> entities);
    void Remove(T entity);
    void RemoveRange(IEnumerable<T> entities);
    Task<int> Count();
}