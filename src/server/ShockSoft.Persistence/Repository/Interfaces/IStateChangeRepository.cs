﻿using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public interface IStateChangeRepository : IRepository<StateChange>
{
}