﻿using System.Threading.Tasks;

namespace ShockSoft.Persistence;

public class UnitOfWork : IUnitOfWork
{
    private readonly ShockSoftContext context;

    public UnitOfWork(ShockSoftContext context)
    {
        this.context = context;

        Clients = new ClientRepository(this.context);
        Cities = new CityRepository(this.context);
        MediaFiles = new MediaFileRepository(this.context);
        Repairs = new RepairRepository(this.context);
        RepairLines = new RepairLineRepository(this.context);
        DeviceTypes = new DeviceTypeRepository(this.context);
        IncludedItems = new IncludedItemRepository(this.context);
        StateChanges = new StateChangeRepository(this.context);
        Products = new ProductRepository(this.context);
        Chats = new ChatRepository(this.context);
    }

    public IClientRepository Clients { get; set; }
    public ICityRepository Cities { get; set; }
    public IMediaFileRepository MediaFiles { get; set; }
    public IRepairRepository Repairs { get; set; }
    public IRepairLineRepository RepairLines { get; set; }
    public IDeviceTypeRepository DeviceTypes { get; set; }
    public IIncludedItemRepository IncludedItems { get; set; }
    public IStateChangeRepository StateChanges { get; set; }
    public IProductRepository Products { get; set; }
    public IChatRepository Chats { get; set; }

    public async Task<int> Complete()
    {
        return await context.SaveChangesAsync();
    }

    public async void Dispose()
    {
        await context.DisposeAsync();
    }
}