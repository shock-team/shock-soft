﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using ShockSoft.Domain;

namespace ShockSoft.Persistence;

public class ShockSoftContext : DbContext
{
    public ShockSoftContext(DbContextOptions<ShockSoftContext> options) : base(options)
    {
    }

    public DbSet<City> Cities { get; set; }
    public DbSet<Client> Clients { get; set; }
    public DbSet<IncludedItem> IncludedItems { get; set; }
    public DbSet<MediaFile> MediaFiles { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Repair> Repairs { get; set; }
    public DbSet<RepairLine> RepairLines { get; set; }
    public DbSet<Chat> Chats { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}