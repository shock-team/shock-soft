using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ShockSoft.Domain;
using Telegram.Bot;
using Telegram.Bot.Types;
using Chat = ShockSoft.Domain.Chat;

namespace ShockSoft.TelegramApi;

internal class ChatStateContext
{
    private readonly Dictionary<ChatState, IChatStateStrategy> states = new();
    
    public ChatStateContext(IEnumerable<IChatStateStrategy> chatStates)
    {
        foreach (var chatState in chatStates)
        {
            states.Add(chatState.State, chatState);
        }
    }

    public async Task Execute(TelegramBotClient botClient, Update update, Chat chat, CancellationToken cancellationToken)
    {
        await states[chat.State].Execute(botClient, update, chat, cancellationToken);
    }
}
