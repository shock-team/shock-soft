using System.Threading;
using System.Threading.Tasks;
using ShockSoft.Domain;
using Telegram.Bot;
using Telegram.Bot.Types;
using Chat = ShockSoft.Domain.Chat;

namespace ShockSoft.TelegramApi;

internal interface IChatStateStrategy
{
    ChatState State { get; }
    Task Execute(TelegramBotClient botClient, Update update, Chat chat, CancellationToken cancellationToken);
}