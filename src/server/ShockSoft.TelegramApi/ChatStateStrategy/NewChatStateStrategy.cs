using System.Threading;
using System.Threading.Tasks;
using ShockSoft.Domain;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Chat = ShockSoft.Domain.Chat;

namespace ShockSoft.TelegramApi;

internal class NewChatStateStrategy : IChatStateStrategy
{
    public ChatState State => ChatState.New;
    
    public async Task Execute(TelegramBotClient botClient, Update update, Chat chat, CancellationToken cancellationToken)
    {
        var replyMarkup = new ForceReplyMarkup
        {
            InputFieldPlaceholder = "Client id"
        };
            
        var message = await botClient.SendTextMessageAsync(
            chat.ChatId,
            "Hello! To start talking we need to verify who you are:\n" +
            "Please, provide your client id: ",
            replyMarkup: replyMarkup,
            cancellationToken: cancellationToken);

        chat.ReplyToMessageId = message.MessageId;
        chat.State = ChatState.PendingSync;
    }
}