using System;
using System.Threading;
using System.Threading.Tasks;
using ShockSoft.Domain;
using ShockSoft.Persistence;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Chat = ShockSoft.Domain.Chat;

namespace ShockSoft.TelegramApi;

internal class PendingSyncChatStateStrategy : IChatStateStrategy
{
    private readonly IUnitOfWork unitOfWork;
    public ChatState State => ChatState.PendingSync;

    public PendingSyncChatStateStrategy(IUnitOfWork unitOfWork)
    {
        this.unitOfWork = unitOfWork;
    }
    
    public async Task Execute(TelegramBotClient botClient, Update update, Chat chat, CancellationToken cancellationToken)
    {
        if (update.Message is null)
        {
            return;
        }
        
        if (update.Message.ReplyToMessage is null)
        {
            await botClient.SendTextMessageAsync(
                chat.ChatId,
                "Please respond with the Client Id to this message",
                replyToMessageId: chat.ReplyToMessageId,
                cancellationToken: cancellationToken);

            return;
        }
        
        if (update.Message.ReplyToMessage.MessageId == chat.ReplyToMessageId)
        {
            var parsed = Guid.TryParse(update.Message.Text, out var guid);

            if (!parsed)
            {
                var replyMarkup = new ForceReplyMarkup
                {
                    InputFieldPlaceholder = "Client id"
                };
                
                var message = await botClient.SendTextMessageAsync(
                    chat.ChatId,
                    "The provided Id does not exist, please try again:",
                    replyMarkup: replyMarkup,
                    cancellationToken: cancellationToken);

                chat.ReplyToMessageId = message.MessageId;

                return;
            }
            
            var client = await unitOfWork.Clients.GetBy(x => x.Id == guid);

            if (client is null)
            {
                var replyMarkup = new ForceReplyMarkup
                {
                    InputFieldPlaceholder = "Client id"
                };
                
                var message = await botClient.SendTextMessageAsync(
                    chat.ChatId,
                    "The provided Id does not exist, please try again:",
                    replyMarkup: replyMarkup,
                    cancellationToken: cancellationToken);

                chat.ReplyToMessageId = message.MessageId;

                return;
            }
            
            chat.State = ChatState.Synced;
            client.Chats.Add(chat);
            
            await botClient.SendTextMessageAsync(
                chat.ChatId,
                "You were synced successfully with the client name:\n" +
                $"{client.Name} {client.Surname}\n" +
                "Use /repairs to check your repairs.",
                cancellationToken: cancellationToken);
        }
    }
}