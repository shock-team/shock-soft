using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ShockSoft.Domain;
using ShockSoft.Persistence;
using Telegram.Bot;
using Telegram.Bot.Types;
using Chat = ShockSoft.Domain.Chat;

namespace ShockSoft.TelegramApi;

internal class SyncedChatStateStrategy : IChatStateStrategy
{
    private const string DefaultMessage = "default";
    private readonly IUnitOfWork unitOfWork;
    private readonly Dictionary<string, Func<Task>> messageActions = new();
    public ChatState State => ChatState.Synced;

    public SyncedChatStateStrategy(IUnitOfWork unitOfWork)
    {
        this.unitOfWork = unitOfWork;
    }
    
    public async Task Execute(TelegramBotClient botClient, Update update, Chat chat, CancellationToken cancellationToken)
    {
        if (update.Message is null)
        {
            return;
        }

        messageActions.Add(DefaultMessage, async () => await DefaultCommand(unitOfWork, botClient, chat, cancellationToken));
        messageActions.Add("/repairs", async () => await RepairsCommand(unitOfWork, botClient, chat, cancellationToken));

        if (messageActions.ContainsKey(update.Message.Text ??= DefaultMessage))
        {
            await messageActions[update.Message.Text].Invoke();
        }
        else
        {
            await messageActions[DefaultMessage].Invoke();
        }
    }
    
    private static async Task RepairsCommand(IUnitOfWork unitOfWork, ITelegramBotClient botClient, Chat chat, CancellationToken cancellationToken)
    {
        var repairs = await unitOfWork.Repairs.Find(x => x.Client.Id == chat.Client.Id);
        var repairsList = repairs.ToList();

        if (!repairsList.Any())
        {
            await botClient.SendTextMessageAsync(
                chat.ChatId,
                $"You have no repairs at your name!",
                cancellationToken: cancellationToken);

            return;
        }
                        
        var sb = new StringBuilder();
        
        foreach (var repair in repairsList)
        {
            sb.Append($"Repair problem: {repair.Problem}!\n");
            sb.Append($"Repair state: {repair.State.ToString()}\n");
            sb.Append('\n');
        }
        
        await botClient.SendTextMessageAsync(
            chat.ChatId,
            sb.ToString(),
            cancellationToken: cancellationToken);
    }
    
    private static async Task DefaultCommand(IUnitOfWork unitOfWork, ITelegramBotClient botClient, Chat chat, CancellationToken cancellationToken)
    {
        var repairs = await unitOfWork.Repairs.Find(x => x.Client.Id == chat.Client.Id);
        var repairsList = repairs.ToList();
        
        await botClient.SendTextMessageAsync(
            chat.ChatId,
            $"Hello {chat.Client.Name} {chat.Client.Surname}!\n" +
            $"You got {repairsList.Count} Repair(s) in your name\n" +
            "Use /repairs to check your repairs.",
            cancellationToken: cancellationToken);
    }
}

