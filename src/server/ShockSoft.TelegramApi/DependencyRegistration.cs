﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ShockSoft.TelegramApi;

public static class DependencyRegistration
{
    public static void RegisterTelegramApiDependencies(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<ITelegramApiClient, TelegramApiClient>();
        services.Configure<TelegramApiOptions>(configuration.GetSection(TelegramApiOptions.TelegramApi));

        services.AddTransient<ChatStateContext>();
        services.AddTransient<IChatStateStrategy, NewChatStateStrategy>();
        services.AddTransient<IChatStateStrategy, PendingSyncChatStateStrategy>();
        services.AddTransient<IChatStateStrategy, SyncedChatStateStrategy>();
    }
}
