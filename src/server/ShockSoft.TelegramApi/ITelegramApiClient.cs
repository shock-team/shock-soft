﻿using System.Threading;
using Telegram.Bot;

namespace ShockSoft.TelegramApi;

public interface ITelegramApiClient
{
    TelegramBotClient BotClient { get; }
    void StartReceiving(CancellationToken cancellationToken = default);
}