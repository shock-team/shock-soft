﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ShockSoft.Persistence;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace ShockSoft.TelegramApi;

internal class TelegramApiClient : ITelegramApiClient
{
    private readonly ILogger<TelegramApiClient> logger;
    private readonly IServiceProvider serviceProvider;
    private readonly TelegramApiOptions options;

    public TelegramApiClient(
        ILogger<TelegramApiClient> logger,
        IOptions<TelegramApiOptions> options,
        IServiceProvider serviceProvider)
    {
        this.logger = logger;
        this.serviceProvider = serviceProvider;
        this.options = options.Value;
    }

    public TelegramBotClient BotClient { get; private set; }

    public void StartReceiving(CancellationToken cancellationToken = default)
    {
        BotClient = new TelegramBotClient(options.AccessToken);

        var receiverOptions = new ReceiverOptions
        {
            AllowedUpdates = Array.Empty<UpdateType>() // receive all update types
        };

        BotClient.StartReceiving(
            HandleUpdateAsync,
            HandlePollingErrorAsync,
            receiverOptions,
            cancellationToken);
    }

    private Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception,
        CancellationToken cancellationToken)
    {
        var errorMessage = exception switch
        {
            ApiRequestException apiRequestException =>
                $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
            _ => exception.ToString()
        };

        logger.LogError(errorMessage);
        return Task.CompletedTask;
    }

    private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update,
        CancellationToken cancellationToken)
    {
        if (update.Type != UpdateType.Message)
            return;
        if (update.Message?.Type != MessageType.Text)
            return;

        var chatId = update.Message.Chat.Id;
        
        using var scope = serviceProvider.CreateScope();
        var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
        var chat = await unitOfWork.Chats.GetBy(c => c.ChatId == chatId, x => x.Client);

        if (chat is null)
        {
            var newChat = new Domain.Chat
            {
                ChatId = chatId,
            };

            chat = newChat;
            unitOfWork.Chats.Add(chat);
        }

        var stateContext = scope.ServiceProvider.GetRequiredService<ChatStateContext>();
        await stateContext.Execute(BotClient, update, chat, cancellationToken);
        
        await unitOfWork.Complete();
        
        logger.LogInformation($"Received a '{update.Message.Text}' message in chat {chatId} at {DateTime.Now}.");
    }
}