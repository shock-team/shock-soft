﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ShockSoft.TelegramApi;

public class TelegramApiClientHostedService : IHostedService
{
    private readonly ILogger<TelegramApiClientHostedService> logger;
    private readonly ITelegramApiClient telegramApiClient;

    public TelegramApiClientHostedService(
        ILogger<TelegramApiClientHostedService> logger,
        ITelegramApiClient telegramApiClient)
    {
        this.logger = logger;
        this.telegramApiClient = telegramApiClient;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        logger.LogInformation("Starting TelegramApi client hosted service...");

        telegramApiClient.StartReceiving(cancellationToken);
        
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        logger.LogInformation("Shutting down TelegramApi client hosted service...");

        return Task.CompletedTask;
    }
}