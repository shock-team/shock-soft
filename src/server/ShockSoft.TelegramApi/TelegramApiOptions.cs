﻿namespace ShockSoft.TelegramApi;

public class TelegramApiOptions
{
    public const string TelegramApi = "TelegramApi";
    public string AccessToken { get; set; }
    public string BaseUrl { get; set; }
    public TelegramApiEndpoints Endpoints { get; set; }
}

public class TelegramApiEndpoints
{
    public string SendMessage { get; set; }
}